<?php

include_once("common.php");

//echo "<pre>";print_r($_POST);exit;
if ($_POST) {
    /* if($_POST['vPassword'] != $_POST['vRPassword'])
      {
      $generalobj->getPostForm($_POST,"Password doesn't match","SignUp");
      exit;
      } */
    $msg = $generalobj->checkDuplicateFront('vEmail', "register_user", Array('vEmail'), $tconfig["tsite_url"] . "sign-up_rider.php?error=1&var_msg=Email already Exists", "Email already Exists", "", "");


    $eReftype = "Rider";
    $Data['vRefCode'] = $generalobj->ganaraterefercode($eReftype);
    $Data['iRefUserId'] = $_POST['iRefUserId'];
    $Data['eRefType'] = $_POST['eRefType'];
    $Data['vName'] = $_POST['vName'];
    $Data['vLang'] = $_POST['vLang'];
    $Data['vLastName'] = $_POST['vLastName'];

    $Data['vPassword'] = $generalobj->encrypt_bycrypt($_REQUEST['vPassword']);
    $Data['vEmail'] = $_POST['vEmail'];
    $Data['vPhone'] = $_POST['vPhone'];
    $Data['vCountry'] = $_POST['vCountry'];
    $Data['vPhoneCode'] = $_POST['vPhoneCode'];
    $Data['vZip'] = $_POST['vZip'];

    $Data['vInviteCode'] = $_POST['vInviteCode'];
    $Data['vCurrencyPassenger'] = $_POST['vCurrencyPassenger'];
    //$Data['eGender'] = $_POST['eGender'];
    $Data['dRefDate'] = Date('Y-m-d H:i:s');
    $Data['tRegistrationDate'] = Date('Y-m-d H:i:s');

    $csql = "SELECT eZeroAllowed,vCountryCode FROM `country` WHERE vPhoneCode = '" . $_POST['vPhoneCode'] . "'";
    $CountryData = $obj->MySQLSelect($csql);
    $eZeroAllowed = $CountryData[0]['eZeroAllowed'];

    if ($eZeroAllowed == 'Yes') {
        $Data['vPhone'] = $Data['vPhone'];
    } else {
        $first = substr($Data['vPhone'], 0, 1);

        if ($first == "0") {
            $Data['vPhone'] = substr($Data['vPhone'], 1);
        }
    }

    if (SITE_TYPE == 'Demo') {
        $Data['eStatus'] = 'Active';
    }

    $id = $obj->MySQLQueryPerform("register_user", $Data, 'insert');

//        $eFor = "Referrer";
//	$tDescription = "Referal amount credit ".$REFERRAL_AMOUNT." into your account";
//	$dDate = Date('Y-m-d H:i:s');
//	$ePaymentStatus = "Unsettelled";
//	$REFERRAL_AMOUNT; 
//	if($_POST['vRefCode'] != "" && !empty($_POST['vRefCode'])){
//		$generalobj->InsertIntoUserWallet($_POST['iRefUserId'],$_POST['eRefType'],$REFERRAL_AMOUNT,'Credit',0,$eFor,$tDescription,$ePaymentStatus,$dDate);
//	}

    if ($id != "") {
        $_SESSION['sess_iUserId'] = $id;
        $_SESSION["sess_vName"] = $Data['vName'] . ' ' . $Data['vLastName'];
        $_SESSION["sess_company"] = " ";
        $_SESSION["sess_vEmail"] = $Data['vEmail'];
        $_SESSION["sess_user"] = "rider";
        $_SESSION["sess_vCurrency"] = $Data['vCurrencyPassenger'];
        $maildata['EMAIL'] = $_SESSION["sess_vEmail"];
        $maildata['NAME'] = $_SESSION["sess_vName"];
        //$maildata['PASSWORD'] = $langage_lbl["LBL_PASSWORD"] . ": " . $_REQUEST['vPassword']; //Commented By HJ On 11-01-2019 For Hide Password As Per Discuss With QA BM
        $maildata['SOCIALNOTES'] = '';
        $generalobj->send_email_user("MEMBER_REGISTRATION_USER", $maildata);
		if(!empty($_SESSION['sess_currentpage_url_mr']) && isset($_SESSION['sess_currentpage_url_mr'])){
			$redirect = $_SESSION['sess_currentpage_url_mr'];
			unset($_SESSION['sess_currentpage_url_mr']);
			header("Location:".$redirect);
			
            exit;
		}	
        if ($_REQUEST['depart'] != "" && $_REQUEST['depart'] == 'mobi') {
            header("Location:mobi");
            exit;
        }
        header("Location:profile_rider.php");
        exit;
    }
}
?>
