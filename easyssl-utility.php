<?php // phpcs:disable PSR1.Files.SideEffects.FoundWithSymbols
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses

$version = '1.0.0';
$build = '101218';

@set_time_limit(0);
@ini_set('max_execution_time', 0);
date_default_timezone_set('UTC');

define('BACKUP_FOLDER', dirname(__FILE__) . DIRECTORY_SEPARATOR . '.easyssl_backup');
define('BACKUP_GROUP_DELIMITER', '_');
define('EASYSSL_KEY', 'kduhgop32lkj');

// Set Response Content Type
header('Content-Type: application/json');

/**
 * Prevents the non-supervised execution of the cleanup script.
 *
 * The main idea of this condition is to automatically delete this and other
 * scripts that were uploaded by one of our support agents to a customer's
 * website.
 *
 * If the agent forgets to delete these files and a web crawler finds them,
 * the direct execution of the script without the required parameters will
 * trigger the deletion of the cleanup suite.
 *
 * @option srun Stop the automatic deletion of the unsupervised scripts.
 */
if (!isset($_GET['erun'])) {
    if (file_exists('./easyssl_db_config.php')) {
        @unlink('./easyssl_db_config.php');
    }
    @unlink(__FILE__);
    exit;
}

// Test Request
if (isset($_POST['test'])) {
    if (version_compare(PHP_VERSION, '5.2.0') < 0) {
        // Not compatible
        echo 'false';
        die(0);
    }
    echo safe_json_encode(
        array(
            'php'     => PHP_VERSION,
            'version' => $version,
            'build'   => $build,
            'test'    => true
        )
    );
    die(0);
}

// http_response_code() requires PHP >= 5.4.0; define our own if it's missing
if (!function_exists('http_response_code')) {
    function http_response_code($code = null) {
        if ($code !== null) {
            $code = (int)$code;
            switch ($code) {
                case 200:
                    $text = 'OK';
                    break;
                case 400:
                    $text = 'Bad Request';
                    break;
                case 500:
                default:
                    $text = 'Internal Server Error';
                    break;
            }
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $text);
            $GLOBALS['http_response_code'] = $code;
        } else {
            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : false);
        }
        return $code;
    }
}

// Main Request Handling
try {
    // Check action is set
    if (!isset($_POST['action'])) {
        throw new Exception('Missing action');
    }
    // Validate action
    $valid_actions = array(
        'search',
        'replace',
        'view',
        'backups',
        'restore',
        'update',
        'db-configs',
        'test-db-config',
        'dump',
        'login-bypass'
    );
    if (!in_array($_POST['action'], $valid_actions)) {
        http_response_code(400);
        throw new Exception('Invalid action provided');
    }
    $action = $_POST['action'];

    if ($action === 'view') {
        // Check view
        if (empty($_POST['view'])) {
            http_response_code(400);
            throw new Exception('Missing view');
        }
        // Cleanup Slashes (Magic)
        if (get_magic_quotes_gpc()) {
            $_POST['view'] = stripslashes($_POST['view']);
        }
        // Validate Type
        $type = validateType();

        if ($type === 'files') {
            $path = $_POST['view'];

            if (!is_file($path) && !is_link($path)) {
                throw new Exception('Invalid path');
            }

            $file_utility = new FileUtility();
            $fileObj = new File($path);
            $file_utility->viewFile($fileObj);
            echo (safe_json_encode($fileObj));
            die(0);
        } elseif ($type === 'database') {
            // Check for config
            if (!isset($_POST['config'])) {
                http_response_code(400);
                throw new Exception('Missing config');
            }
            // Attempt to get requested config
            $configs = DatabaseConfig::getCMSConfigs();
            if (!isset($configs[$_POST['config']])) {
                http_response_code(400);
                throw new Exception('Invalid config provided');
            }
            // Check view type (Backup/DB Entry)
            if ($_POST['view'] === 'backup') {
                // Show content
                $db_utility = new DatabaseUtility($configs[$_POST['config']]);
                $restore_data = $db_utility->getBackup($_POST['filename']);
                echo (safe_json_encode($restore_data));
                die(0);
            } elseif ($_POST['view'] === 'entry') {
                // Validate Location
                if (!isset($_POST['t'])
                    && !isset($_POST['c'])
                    && !isset($_POST['id_name'])
                    && !isset($_POST['id'])
                ) {
                    throw new Exception('Invalid DB View Location');
                }
                // Show content
                $db_utility = new DatabaseUtility($configs[$_POST['config']]);
                $entry = $db_utility->viewContent($_POST['t'], $_POST['c'], $_POST['id_name'], $_POST['id']);
                echo (safe_json_encode($entry));
                die(0);
            }
        }
    } elseif ($action === 'restore') {
        // Check restore
        if (empty($_POST['restore'])) {
            http_response_code(400);
            throw new Exception('Missing restore');
        }
        // Cleanup Slashes (Magic)
        if (get_magic_quotes_gpc()) {
            $_POST['restore'] = stripslashes($_POST['restore']);
        }
        // Validate Type
        $type = validateType();

        if ($type === 'files') {
            // Check restore
            if (empty($_POST['version'])) {
                http_response_code(400);
                throw new Exception('Missing version');
            }
            // Cleanup Slashes (Magic)
            if (get_magic_quotes_gpc()) {
                $_POST['restore'] = stripslashes($_POST['restore']);
                $_POST['version'] = stripslashes($_POST['version']);
            }
            $file_utility = new FileUtility();
            $groups = $file_utility->restore($_POST['restore'], $_POST['version']);
            echo (safe_json_encode($groups));
            die(0);
        } elseif ($type === 'database') {
            // Check for config
            if (!isset($_POST['config'])) {
                http_response_code(400);
                throw new Exception('Missing config');
            }
            // Attempt to get requested config
            $configs = DatabaseConfig::getCMSConfigs();
            if (!isset($configs[$_POST['config']])) {
                http_response_code(400);
                throw new Exception('Invalid config provided');
            }
            // Use DatabaseUtility class
            $db_utility = new DatabaseUtility($configs[$_POST['config']]);
            $restore_data = $db_utility->getBackup($_POST['restore']);
            echo (safe_json_encode($db_utility->restoreBackup($restore_data['contents'])));
            die(0);
        }
    } elseif ($action === 'dump') {
        // Check for config
        if (!isset($_POST['config'])) {
            http_response_code(400);
            throw new Exception('Missing config');
        }
        // Attempt to get requested config
        $configs = DatabaseConfig::getCMSConfigs();
        if (!isset($configs[$_POST['config']])) {
            http_response_code(400);
            throw new Exception('Invalid config provided');
        }
        // Use DatabaseUtility class
        $db_utility = new DatabaseUtility($configs[$_POST['config']]);
        echo (safe_json_encode($db_utility->dump()));
    } elseif ($action === 'backups') {
        // Validate Type
        $type = validateType();

        if ($type === 'files') {
            $file_utility = new FileUtility();
            $bup_config = new BUPConfig(null);
            $groups = $file_utility->backups($bup_config);
            echo (safe_json_encode($groups));
            die(0);
        } elseif ($type === 'database') {
            // Check for config
            if (!isset($_POST['config'])) {
                http_response_code(400);
                throw new Exception('Missing config');
            }
            // Attempt to get requested config
            $configs = DatabaseConfig::getCMSConfigs();
            if (!isset($configs[$_POST['config']])) {
                http_response_code(400);
                throw new Exception('Invalid config provided');
            }
            // Use DatabaseUtility class
            $db_utility = new DatabaseUtility($configs[$_POST['config']]);
            echo (safe_json_encode($db_utility->backups()));
        }
    } elseif ($action === 'search' || $action === 'replace') {
        // Check search term
        if (empty($_POST['search'])) {
            http_response_code(400);
            throw new Exception('Missing search');
        }
        // Check search (is_regex)
        if (empty($_POST['is_regex'])) {
            http_response_code(400);
            throw new Exception('Missing is_regex');
        }

        // Validate Type
        $type = validateType();

        // Check for replace
        if ($action === 'replace') {
            // Check replace term
            if (empty($_POST['replace'])) {
                http_response_code(400);
                throw new Exception('Missing replace');
            }
        }

        // Cleanup Slashes (Magic)
        if (get_magic_quotes_gpc()) {
            $_POST['search'] = stripslashes($_POST['search']);
            $_POST['replace'] = stripslashes($_POST['replace']);
        }

        // Validate Search regex
        if ($_POST['is_regex'] === 'true' && @preg_match('/' . $_POST['search'] . '/', null) === false) {
            http_response_code(400);
            throw new Exception('Invalid search regex');
        }

        if ($type === 'files') {
            // Run search using FileUtility Class
            $file_utility = new FileUtility();
            $sr_config = new SRConfig(
                $action,
                $_POST['search'],
                $_POST['replace'],
                $_POST['is_regex'] === 'true',
                false
            );
            echo safe_json_encode($file_utility->search($sr_config));
            die(0);
        } elseif ($type === 'database') {
            // Check for config
            if (!isset($_POST['config'])) {
                http_response_code(400);
                throw new Exception('Missing config');
            }
            // Attempt to get requested config
            $configs = DatabaseConfig::getCMSConfigs();
            if (!isset($configs[$_POST['config']])) {
                http_response_code(400);
                throw new Exception('Invalid config provided');
            }
            // Use DatabaseUtility class
            $db_utility = new DatabaseUtility($configs[$_POST['config']]);
            // Search
            $sr_config = new SRConfig($action, $_POST['search'], $_POST['replace'], false, false);
            echo safe_json_encode($db_utility->search($sr_config));
            die(0);
        }
    } elseif ($action === 'db-configs') {
        // phpcs:ignore
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $configs = DatabaseConfig::getCMSConfigs();
        echo safe_json_encode(
            array(
            'configs' => $configs,
            'protocol' => $protocol
            )
        );
        die(0);
    } elseif ($action === 'test-db-config') {
        // Check Params
        if (!isset($_POST['dbname'])
            || !isset($_POST['dbuser'])
            || !isset($_POST['dbpass'])
            || !isset($_POST['dbhost'])
            || !isset($_POST['prefix'])
            || !isset($_POST['cms'])
        ) {
            throw new Exception('Missing DB params');
        }
        // Magic Strip
        if (get_magic_quotes_gpc()) {
            $_POST['dbname'] = stripslashes($_POST['dbname']);
            $_POST['dbuser'] = stripslashes($_POST['dbuser']);
            $_POST['dbpass'] = stripslashes($_POST['dbpass']);
            $_POST['dbhost'] = stripslashes($_POST['dbhost']);
            $_POST['prefix'] = stripslashes($_POST['prefix']);
            $_POST['cms'] = stripslashes($_POST['cms']);
        }
        // Create config
        $created_config = DatabaseConfig::createConfigFile($_POST);
        if (!$created_config) {
            throw new Exception('Failed to create custom config file');
        }
        // Test DB Connection
        $configs = DatabaseConfig::getCMSConfigs();
        if (!isset($configs['./easyssl_db_config.php'])) {
            http_response_code(400);
            throw new Exception('Unable to locate custom DB config');
        }
        // Use DatabaseUtility class
        $db_utility = new DatabaseUtility($configs['./easyssl_db_config.php']);

        // Success
        echo safe_json_encode(
            array(
                'config_test' => true,
                'config'      => $configs['./easyssl_db_config.php']
            )
        );
        die(0);
    } elseif ($action === 'login-bypass') {
        // Check for CMS
        if (empty($_POST['cms'])) {
            http_response_code(400);
            throw new Exception('Missing cms');
        }
        $cms = $_POST['cms'];
        $login = false;
        // Check for user
        if (!empty($_POST['user'])) {
            // Attempt login
            $login = true;
        }
        // Check CMS
        switch ($cms) {
            case 'wordpress':
                if ($login) {
                    LoginBypass::wordpressLogin($_POST['user']);
                    die(0);
                }
                // Get Users
                echo safe_json_encode(LoginBypass::wordpressUsers());
                die(0);
            case 'joomla':
                if ($login) {
                    LoginBypass::joomlaLogin($_POST['user']);
                    die(0);
                }
                // Get Users
                echo safe_json_encode(LoginBypass::joomlaListUsers());
                die(0);
            case 'drupal':
                // Drupal 8 Not Supported (Currently)
                if ($login) {
                    LoginBypass::drupalLogin($_POST['user']);
                    die(0);
                }
                // Get Users
                echo safe_json_encode(LoginBypass::drupalListUsers());
                die(0);
            case 'magento':
                if ($login) {
                    LoginBypass::magentoLogin($_POST['user']);
                    die(0);
                }
                // Get Users
                echo safe_json_encode(LoginBypass::magentoListUsers());
                die(0);
            default:
                throw new Exception('Unknown CMS');
        }
    }
} catch (Exception $e) {
    // Error Response
    if (http_response_code() === false) {
        http_response_code(500);
    }
    echo safe_json_encode(
        array(
            'status'  => 'error',
            'message' => $e->getMessage()
        )
    );
}

/**
 * Validates the $_POST['type'] param
 *
 * @return string If the POST parameter "type" is valid, throws Exception otherwise.
 */
function validateType()
{
    // Check for search type
    if (!isset($_POST['type'])) {
        http_response_code(400);
        throw new Exception('Missing type');
    }
    // Validate search type
    $valid_types = array('files', 'database');
    if (!in_array($_POST['type'], $valid_types)) {
        http_response_code(400);
        throw new Exception('Invalid type provided');
    }
    return $_POST['type'];
}

function safe_json_encode($array)
{
    $json = json_encode($array);

    if ($json === false) {
        // json_last_error() requires PHP >= 5.3
        if (!function_exists('json_last_error')) {
            throw new exception('JSON Error');
        }
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $json;
            case JSON_ERROR_DEPTH:
                throw new Exception('JSON Error: Maximum stack depth exceeded');
            case JSON_ERROR_STATE_MISMATCH:
                throw new Exception('JSON Error: Underflow or the modes mismatch');
            case JSON_ERROR_CTRL_CHAR:
                throw new Exception('JSON Error: Unexpected control character found');
            case JSON_ERROR_SYNTAX:
                throw new Exception('JSON Error: Syntax error, malformed JSON');
            case JSON_ERROR_UTF8:
                // Just use utf8_encode for now
                recursive_utf8_encode($array);
                // Now encode into JSON without issues
                $json = json_encode($array);
                // Check for error just to be safe
                if (json_last_error() !== JSON_ERROR_NONE) {
                    throw new Exception('JSON Error: Malformed UTF-8 characters, possibly incorrectly encoded');
                }
                return $json;
            default:
                throw new Exception('JSON Error: Unknown');
        }
    } else {
        return $json;
    }
}

function recursive_utf8_encode(&$item)
{
    if (is_object($item) || is_array($item)) {
        foreach ($item as $key => &$val) {
            recursive_utf8_encode($val);
        }
    } elseif (is_string($item)) {
        $item = utf8_encode($item);
    }
}

/**
 * Shared Methods for Utilites.
 */
abstract class MSSLUtility
{
    /**
     * Encodes the passed content with certain key.
     *
     * @param string $content Text to be encrypted.
     * @param string $key     Password for the encryption.
     * @param int    $shift   Number of encryption cycles.
     *
     * @return string          New text already encrypted.
     */
    protected function encrypt($content, $key, $shift)
    {
        $result = '';
        $contentLength = strlen($content);
        $keyLength = strlen($key);

        // XOR the first 'shift' bytes
        $shift %= $keyLength;
        $processedLength = min($keyLength - $shift, $contentLength);
        // substr() returns false for an empty $content string, so we do an additional check

        if ($processedLength > 0) {
            $result = substr($content, 0, $processedLength) ^ substr($key, $shift, $processedLength);
        }

        $restLength = ($contentLength - $processedLength) % $keyLength;
        $mainPartEnd = $contentLength - $restLength;

        for ($i = $processedLength; $i < $mainPartEnd; $i += $keyLength) {
            $result .= $key ^ substr($content, $i, $keyLength);
        }

        if ($restLength > 0) {
            $result .= substr($content, $mainPartEnd) ^ substr($key, 0, $restLength);
        }

        return $result;
    }
    /**
     * Creates a new directory to store the modified data.
     *
     * @return bool True if the directory was created, false otherwise.
     */
    protected function createBackupFolder()
    {
        // Create the directory (race condition here)
        if (!is_dir(BACKUP_FOLDER)) {
            if (!mkdir(BACKUP_FOLDER, 0755, true)) {
                return false;
            }
        }

        // Harden the directory
        if (!touch(BACKUP_FOLDER . DIRECTORY_SEPARATOR . 'index.html')
            || !touch(BACKUP_FOLDER . DIRECTORY_SEPARATOR . 'index.php')
        ) {
            return false;
        }

        $fh = fopen(BACKUP_FOLDER . DIRECTORY_SEPARATOR . '.htaccess', 'w');
        if ($fh === false || fwrite($fh, 'deny from all') === false) {
            fclose($fh);
            return false;
        }

        fclose($fh);
        return true;
    }
}

/**
 * Database Utility
 *
 * Relies on the MySQLIDriver and MySQLDriver classes for DB interaction.
 */
class DatabaseUtility extends MSSLUtility
{
    const SUPPORT_COMPOSITE_KEYS = true;
    const PCRE_DELIM             = '~';
    const POSIX_DELIM            = '/';
    const BACKUP_COMMENT         = '-- easyssl_backup';

    protected $updateErrMessages = array(
        'UPDATE_ERR_BACKUP_DIR'  => 'Create backup dir failed',
        'UPDATE_ERR_BACKUP_FILE' => 'Create backup file failed',
        'UPDATE_ERR_CHANGED'     => 'The database column was simultaneously changed by another program. Please try again',
        'UPDATE_ERR_NO_PRI'      => 'No primary key found'
    );

    protected $backup_appendix = '_easyssldbbackup.';
    protected $backup_files    = array();
    protected $config;
    protected $driver;
    protected $tablesColumns;
    protected $entries    = array();
    protected $scan_count = 0;
    protected $scan_time  = 0;

    /**
     * Handles multiple required checks.
     *
     * 1. Checks the Backup Directory
     * 2. Instantiates the DB Driver
     * 3. Tests the DB Config via Connection attempt
     * 4. Gets all table columns (array)
     *
     * @param array $config Holds DB Connection config.
     */
    public function __construct($config)
    {
        // Create Backup Directory
        if (!$this->createBackupFolder()) {
            throw new Exception('Unable to create backup folder.');
        }
        /**
         * The mysqli extension, or as it is sometimes known, the MySQL improved
         * extension, was developed to take advantage of new features found in MySQL
         * systems versions 4.1.3 and newer. The mysqli extension is included with PHP
         * versions 5 and later. The mysqli extension has a number of benefits, the key
         * enhancements over the mysql extension being:
         *
         * Object-oriented interface
         * Support for Prepared Statements
         * Support for Multiple Statements
         * Support for Transactions
         * Enhanced debugging capabilities
         * Embedded server support
         */
        if (function_exists('mysqli_connect')) {
            $this->driver = new MySQLIDriver();
        } else {
            $this->driver = new MySQLDriver();
        }
        $this->config = $config;

        $errMsg = $this->driver->connect($config);
        if ($errMsg !== true) {
            throw new Exception($errMsg);
        }
        $this->tablesColumns = $this->driver->getTablesColumns($config, self::SUPPORT_COMPOSITE_KEYS);
    }

    /**
     * Handles closing the DB connection during shutdown sequence.
     */
    public function __destruct()
    {
        if ($this->driver !== null) {
            $this->driver->disconnect();
        }
    }

    /**
     * Searches the database using the provided config.
     *
     * @param SRConfig $config The search/replace config.
     *
     * @return array Contains any matching entries and debug data.
     */
    public function search($config)
    {
        // Create Backup Directory
        if (!$this->createBackupFolder()) {
            throw new Exception('Unable to create backup folder.');
        }

        // Keep track of location in DB
        $location = array();

        // Build condition
        $condition = $this->prepareCondition($config);

        // Time Scan
        $start_time = microtime(true);

        foreach ($this->tablesColumns as $table => $columns) {
            $location['table'] = $table;
            $escapedId = '';
            if (isset($columns['idname'])) {
                $location['idname'] = $columns['idname'];
                $escapedId = ',' . $this->driver->escapeName($columns['idname']);
            } else {
                unset($location['idname']);
                unset($location['id']);
            }

            $escapedTable = $this->driver->escapeName($table);
            foreach ($columns as $i => $column) {
                if ($i === 'idname') {
                    continue;
                }

                if (is_array($column)) {
                    /* skip composite keys; not necessary */
                    /* idnames => [option_id, option_name] */
                    continue;
                }

                $this->scan_count += 1;

                $location['column'] = $column;
                $escapedColumn = $this->driver->escapeName($location['column']);

                // Search for the triggers using the previously prepared condition
                $sql =
                'SELECT ' .
                $escapedColumn .
                $escapedId .
                ' FROM ' .
                $escapedTable .
                implode($escapedColumn, $condition) . ';';

                // Execute Query
                $result = $this->driver->query($sql);
                while ($row = $this->driver->fetchRow($result)) {
                    if (isset($location['idname'])) {
                        $location['id'] = $row[1];
                    }
                    $text = $row[0];
                    $entry = new DBEntry($location);
                    $entry->contents = $text;
                    $entry->easy_id = utf8_encode("{$entry->location['table']}.{$entry->location['column']}, {$entry->location['idname']} = {$entry->location['id']}"); // Also UTF-8 encode the easy_id since it is also the array key

                    if ($config->action === 'replace') {
                        // Replace and update the entry contents
                        $entry->new_contents = $this->replace($config, $entry);
                        $entry->updated = $this->update($config, $entry);
                    }

                    $this->entries[$entry->easy_id] = $entry;
                }
            }
        }
        $this->scan_time += (microtime(true) - $start_time);
        return array(
            'entries' => $this->entries,
            'debug'   => array(
                'scan_time'  => $this->scan_time,
                'scan_count' => $this->scan_count
            )
        );
    }

    /**
     * Replaces contents based upon the provided config.
     *
     * @param SRConfig $config The current search/replace config.
     * @param DBEntry  $entry  The database entry with contents that needs replaced.
     *
     * @return string Is the string that has been modified via $config
     */
    public function replace($config, &$entry)
    {
        // Quick check: if it's a serialized object/array/string
        if (isset($entry->contents[1]) && $entry->contents[1] !== ':') {
            return $config->is_regex
            ? preg_replace($config->search, $config->replace, $entry->contents)
            : str_ireplace($config->search, $config->replace, $entry->contents);
        }

        list($pos, $replaced) = $this->replaceInSerialized(
            $config->search,
            $config->replace,
            $entry->contents,
            $config->is_regex
        );

        if ($replaced === false || $pos !== strlen($entry->contents)) {
            // Not a valid serialized data; do a usual textual search-and-replace
            return $config->is_regex
            ? preg_replace($config->search, $config->replace, $entry->contents)
            : str_ireplace($config->search, $config->replace, $entry->contents);
        }

        // Valid Serialized Data
        $entry->is_serialized = true;

        return $replaced;
    }
    /**
     * Replaces content in a serialized object.
     *
     * Avoid the use of the built-in PHP unserialize function as it is vulnerable on
     * old PHP versions. Do a custom parsing function, replace inside the serialized
     * strings, and correct the string lengths.
     *
     * @param string $search     Search term which may be valid regex.
     * @param string $replace    Replace term used if search is matching.
     * @param string $serialized Serialized object as a string.
     * @param bool   $isRegEx    True if pattern is a regexp, false for string.
     *
     * @return array              Two-index length array with new serialized data.
     */
    public function replaceInSerialized($search, $replace, $serialized, $isRegEx)
    {
        $valueToSkipRe = '/^(?:
            N; | b:[01];                     # null, Boolean
            | [rR] : [0-9]+;                 # references
            | [id] : [+-]? [0-9]+;           # an integer (as integer or double type)
            |   d  : [+-]? [0-9]* \\. [0-9]+ (?: [eE] [+-]? [0-9]+ )?;  # a floating point number (double)
            ) /x';
        $strRe = '/^ (?P<type> [sS]) : (?P<len> [0-9]+) : "/x';
        $compoundRe = '/^ (?:
            a : (?P<count> [0-9]+) : \\{
            | (?P<type> [OC]) : (?P<namelen> [0-9]+) : "(?P<name> [^"]+)" : (?P<clen> [0-9]+) : \\{
            ) /x';

        if (preg_match($valueToSkipRe, $serialized, $m)) {
            return array(strlen($m[0]), $m[0]);
        }

        if (preg_match($strRe, $serialized, $m)) {
            if ($m['type'] === 'S') {
                list($inputLen, $str) = $this->unescapeString(substr($serialized, strlen($m[0])), $m['len']);
                if ($str === false) {
                    return array(0, false);
                }
                $inputLen += strlen($m[0]);
            } else {
                $str = substr($serialized, strlen($m[0]), $m['len']);
                $inputLen = strlen($m[0]) + $m['len'];
            }

            if (substr($serialized, $inputLen, 2) !== '";') {
                return array(0, false);
            }

            $str = $isRegEx ? preg_replace($search, $replace, $str) : str_ireplace($search, $replace, $str);

            return array($inputLen + 2, 's:' . strlen($str) . ':"' . $str . '";');
        }

        if (preg_match($compoundRe, $serialized, $m)) {
            if (isset($m['namelen']) && intval($m['namelen']) !== strlen($m['name'])) {
                return array(0, false);
            }

            if (isset($m['type']) && $m['type'] === 'C') {
                // Custom serialization format, skip it (no search-and-replace)
                $inputLen = strlen($m[0]) + $m['clen'];
                if (substr($serialized, $inputLen, 1) !== '}') { // substr silently returns false if out of bounds
                    return array(0, false);
                }
                return array($inputLen + 1, substr($serialized, 0, $inputLen + 1));
            }

            $out = $m[0];
            $inputPos = strlen($m[0]);
            $count = $m['count'] === '' ? $m['clen'] : $m['count'];

            for ($i = 0; $i < $count; $i++) {
                if ($m['count'] === '') { // Object - don't replace in the variable name
                    list($len, $replaced) = $this->replaceInSerialized(
                        '',
                        $replace,
                        substr($serialized, $inputPos),
                        false
                    );
                } else { // Array - replace in the index
                    list($len, $replaced) = $this->replaceInSerialized(
                        $search,
                        $replace,
                        substr($serialized, $inputPos),
                        $isRegEx
                    );
                }
                if ($replaced === false) {
                    return array(0, false);
                }
                $inputPos += $len;
                $out .= $replaced;

                // Always replace in the value
                list($len, $replaced) = $this->replaceInSerialized(
                    $search,
                    $replace,
                    substr($serialized, $inputPos),
                    $isRegEx
                );
                if ($replaced === false) {
                    return array(0, false);
                }
                $inputPos += $len;
                $out .= $replaced;
            }

            if (substr($serialized, $inputPos, 1) !== '}') {
                return array(0, false);
            }

            return array($inputPos + 1, $out . '}');
        }

        return array(0, false);
    }
    /**
     * Reverts the escaping applied to a string.
     *
     * @param string $str    Text to remove the escaping.
     * @param int    $nChars Number of characters to revert.
     *
     * @return array          Array representation of the string.
     */
    public function unescapeString($str, $nChars)
    {
        $inputLen = 0;
        $out = '';
        for ($i = 0; $i < $nChars; $i++) {
            if (!isset($str[$inputLen])) {
                return array(0, false); // Invalid string length
            }

            if ($str[$inputLen] !== '\\') {
                $out .= $str[$inputLen];
                $inputLen++;
                continue;
            }

            $hex = substr($str, $inputLen + 1, 2);
            if (!ctype_xdigit($hex)) {
                return array(0, false); // Invalid hex char
            }
            $out .= pack('H*', $hex);
            $inputLen += 3;
        }
        return array($inputLen, $out);
    }

    /**
     * Changes the value in a table row in the database.
     *
     * This function also creates a backup of the original content.
     *
     * @param SRConfig $config The current search/replace config.
     * @param DBEntry  $entry  The database entry with contents that needs replaced.
     *
     * @return bool Whether the operation succeeded or not.
     */
    public function update($config, &$entry)
    {
        if (!isset($entry->location['idname'])) {
            return true;
        }

        $backupFilename = $this->getBackupFilename($entry->location['table']);

        if ($backupFilename === false) {
            array_push($entry->errors, $this->updateErrMessages['UPDATE_ERR_BACKUP_DIR']);
            return false;
        }

        // Prepare the SQL statements
        $update = 'UPDATE ' . $this->driver->escapeName($entry->location['table']) . ' SET ' .
        $this->driver->escapeName($entry->location['column']) . ' = ';

        $where = ' WHERE ' . $this->driver->escapeName($entry->location['idname']) . ' = ' .
        $this->driver->escapeStr($entry->location['id']) . ' AND ' .
        $this->driver->escapeName($entry->location['column']) . ' = ';

        // Encrypt the backup and save it in that folder
        $backup = $update . $this->driver->escapeStr($entry->contents) . $where .
        $this->driver->escapeStr($entry->new_contents) . ";\n\n";
        if (!$this->addToBackup($backupFilename, $backup)) {
            array_push($entry->errors, $this->updateErrMessages['UPDATE_ERR_BACKUP_FILE']);
            return false;
        }
        $entry->backedup = true;

        // Change the column value in the database only when we have the backup
        $this->driver->query(
            $update .
            $this->driver->escapeStr($entry->new_contents) .
            $where .
            $this->driver->escapeStr($entry->contents) .
            ';'
        );
        if ($this->driver->getAffectedRows() !== 1) {
            array_push($entry->errors, $this->updateErrMessages['UPDATE_ERR_CHANGED']);
            return false;
        }

        return true;
    }
    /**
     * Allows the viewing of DB contents.
     *
     * @param string $table  Name of the table.
     * @param string $column Name of the column.
     * @param string $idName Name of the primary key column.
     * @param array  $id     List of identifiers for the primary key column.
     *
     * @return DBEntry Database entry contents or throws exception.
     */
    public function viewContent($table = '', $column = '', $idName = '', $id = array())
    {
        $location = array('table' => $table, 'column' => $column, 'idname' => $idName, 'id' => $id);

        $compoundRe = '/^ (?:
        a : (?P<count> [0-9]+) : \\{
        | (?P<type> [OC]) : (?P<namelen> [0-9]+) : "(?P<name> [^"]+)" : (?P<clen> [0-9]+) : \\{
        ) /x';

        $sqlContent = 'SELECT ' . $this->driver->escapeName($column) . ' FROM ' . $this->driver->escapeName($table) .
        ' WHERE ' . $this->driver->escapeName($idName) . ' = ' . $this->driver->escapeStr($id) . ';';

        $row = $this->driver->fetchRow($this->driver->query($sqlContent));
        if (strlen($row[0]) === 0) {
            throw new Exception('Unable to locate requested entry in DB');
        }
        $entry = new DBEntry($location);
        $entry->contents = $row[0];
        // phpcs:ignore
        $entry->easy_id = "{$entry->location['table']}.{$entry->location['column']}," .
            " {$entry->location['idname']} = {$entry->location['id']}";

        if (preg_match($compoundRe, $entry->contents)) {
            $entry->is_serialized = true;
        }
        return $entry;
    }
    /**
     * Triggers the download of the entire database.
     *
     * @return void
     */
    public function dump()
    {
        if (!isset($_SERVER['SERVER_NAME'])) {
            $serverName = date('ymd-His');
        } else {
            $serverName = preg_replace('/[^a-zA-Z0-9.-]/', '-', $_SERVER['SERVER_NAME']);
        }

        date_default_timezone_set('GMT');
        header('Content-Type: application/sql; charset=utf-8');
        header('Content-Disposition: attachment; filename=dump_' . $serverName . '_' . $this->config['db'] . '.sql');

        echo "-- EasySSL DB dump\n\n";
        echo $this->driver->getDumpHeader() . "\n";
        echo $this->driver->getCreateDBSql($this->config['db']);

        foreach ($this->tablesColumns as $table => $columns) {
            echo $this->driver->getCreateTableSql($table);
            $this->dumpTableData($table);
        }

        echo "-- End of the dump\n";
    }
    /**
     * Prints the content of a specific table.
     *
     * @param string $table Name of the table.
     *
     * @return void
     */
    public function dumpTableData($table)
    {
        $res = $this->driver->query('SELECT * FROM ' . $this->driver->escapeName($table) . ';');
        list($columns, $types) = $this->driver->getColumnNamesTypes($table);

        // Prepare the header with column names
        $columnsEscaped = array();
        $total = count($columns);
        for ($i = 0; $i < $total; $i++) {
            $columnsEscaped[$i] = $this->driver->escapeName($columns[$i]);
        }
        $columnsEscaped = implode(', ', $columnsEscaped);
        $header = 'INSERT INTO ' . $this->driver->escapeName($table) . ' (' . $columnsEscaped . ') VALUES';

        // Print all rows
        $allValues = '';
        $count = 0;

        while ($row = $this->driver->fetchRow($res)) {
            $values = "\n(" . $this->driver->exportRow($row, $types) . ')';

            if (strlen($allValues) >= 500000) { // Split long SQL statements to avoid MySQL timeouts
                echo $header . $allValues . ";\n";
                $allValues = '';
            }

            if ($allValues) {
                $allValues .= ',';
            }
            $allValues .= $values;
            $count++;
        }

        if ($count === 0) {
            return;
        }

        echo $header . $allValues . ";\n\n";
    }
    /**
     * Checks the backup folder for any backups.
     *
     * @return array Contains debug data and array of backups split into two groups.
     */
    public function backups()
    {
        // Time Backups Scan
        $start_time = microtime(true);

        $backupFiles = array(
            'match'   => array(),
            'nomatch' => array()
        );

        $handle = @opendir(BACKUP_FOLDER);
        if (!$handle) {
            throw new Exception('Failed to open the backup folder');
        }

        while (false !== ($filename = @readdir($handle))) {
            $key = explode($this->backup_appendix, $filename, 2);

            $this->scan_count += 1;

            if (is_dir($filename) || count($key) < 2) {
                continue;
            }

            $fileDate = intval($key[1]);

            $subject = preg_replace('/[^a-zA-Z0-9_-]/', '-', $this->config['db']);
            $key = ((!is_null($subject) && strpos($filename, $subject) !== false) ? 'match' : 'nomatch');
            $backupFiles[$key][$filename] = $fileDate;
        }
        closedir($handle);

        arsort($backupFiles['match']);
        arsort($backupFiles['nomatch']);

        // Add to Scan Time
        $this->scan_time += (microtime(true) - $start_time);

        return array(
            'backups' => $backupFiles,
            'debug'   => array(
                'scan_time'  => $this->scan_time,
                'scan_count' => $this->scan_count
            )
        );
    }

    /**
     * Gets the database backup file.
     *
     * @param string $filename Name of the backup file without path.
     *
     * @return array|Exception Array with filename/contents if issues throws Exception.
     */
    public function getBackup($filename)
    {
        if (!preg_match('@^[A-Za-z0-9_-]+' . preg_quote($this->backup_appendix, '@') . '[0-9]+$@', $filename)) {
            throw new Exception('Invalid backup file name: ' . $filename);
        }

        $fh = fopen(BACKUP_FOLDER . DIRECTORY_SEPARATOR . $filename, 'rb');
        if ($fh === false || false === ($contents = fread($fh, 8 * 1048576))) {
            throw new Exception('Unable to read file: ' . $filename);
        }
        fclose($fh);

        // Decrypt the file
        $contents = $this->encrypt($contents, EASYSSL_KEY, 0);

        $valid = strncmp($contents, self::BACKUP_COMMENT, strlen(self::BACKUP_COMMENT)) === 0
        || strpos($contents, "\nWHERE ") !== false;
        if (!$valid) {
            throw new Exception('Invalid backup file');
        }

        return array(
            'filename' => $filename,
            'contents' => $contents
        );
    }

    /**
     * Restores database backup file contents.
     *
     * @param string $content Contents of DB Backup file.
     *
     * @return array Contains info on restore and any errors.
     */
    public function restoreBackup($content)
    {

        $response = array(
            'sql_statement_count' => 0,
            'sql_queries'         => array(),
            'restored'            => '',
            'error'               => '',
            'location'            => array()
        );

        $sqlLines = $this->getMultiSQLStatements($content);

        $response['sql_statement_count'] = count($sqlLines);

        foreach ($sqlLines as $sql) {
            if ($sql === '' || $sql === self::BACKUP_COMMENT) {
                continue;
            }

            $res = $this->driver->query($sql);

            array_push(
                $response['sql_queries'],
                array(
                    'query'  => $sql,
                    'result' => $res !== false
                )
            );

            if (strpos($sql, "CREATE\x20") === 0
                || strpos($sql, "DROP\x20") === 0
            ) {
                continue;
            }

            $response['location'] = $this->getLocationFromSql($sql);
            $naffected = $this->driver->getAffectedRows();

            if ($naffected === 0) {
                $response['error'] = 'The DB column was changed after backup. Please restore manually';
            } elseif ($naffected !== 1) {
                $response['error'] = 'Unexpected naffected: ' . $naffected;
            } else {
                $response['restored'] = isset($response['location']['column'])
                ? 'Restored DB column'
                : 'Restored DB row';
            }
        }

        return $response;
    }

    /**
     * Gets the Backup filename and adds to $this->backup_files
     *
     * @param [type] $table The current table.
     *
     * @return string|bool Backup filename if no issues, false otherwise.
     */
    public function getBackupFilename($table)
    {
        // Sanitize table name
        $table = preg_replace('/[^a-zA-Z0-9_-]/', '-', $table);

        // Get Database Name & Sanitize (Added during Config Update)
        $dbName = preg_replace('/[^a-zA-Z0-9_-]/', '-', $this->config['db']);

        // If the current backup file is too large or does not exist, generate a new one
        clearstatcache();
        if (isset($this->backup_files[$table]) && filesize($this->backup_files[$table]) <= 500000) {
            return $this->backup_files[$table];
        }

        if (!$this->createBackupFolder()) {
            return false;
        }

        // Generate the file name. There is a race condition here, but it's mostly okay
        // because scan usually takes more than 1 sec. Ideally, we would use fopen(..., "xb"),
        // but it's not supported until PHP 4.3.2.
        $suffix = $this->backup_appendix . time();
        $prefix = BACKUP_FOLDER . DIRECTORY_SEPARATOR . 'replace' . '-' . $dbName . '-' . $table;
        $rand = '';

        while (file_exists($prefix . $rand . $suffix)) {
            $rand = mt_rand(0, 1000000000);
        }

        $this->backup_files[$table] = $prefix . $rand . $suffix;

        return $this->backup_files[$table];
    }

    /**
     * Append a database backup into the same file.
     *
     * @param string $backupFilename Location of the backup file.
     * @param string $backup         Additional data to backup.
     *
     * @return bool                   Whether the operation succeeded or not.
     */
    public function addToBackup($backupFilename, $backup)
    {
        $fh = @fopen($backupFilename, 'ab');
        if ($fh === false) {
            return false;
        }

        if (fseek($fh, 0, SEEK_END) !== 0) {
            return false;
        }

        $size = ftell($fh);
        if ($size === 0) {
            $backup = self::BACKUP_COMMENT . "\n" . $backup;
        }

        $res = false !== fwrite($fh, $this->encrypt($backup, EASYSSL_KEY, $size));

        fclose($fh);
        return $res;
    }

    /**
     * Returns information about the table from an SQL statement.
     *
     * @param string $sql SQL statement that will be parsed.
     *
     * @return array|null  Information about the table.
     */
    public function getLocationFromSql($sql)
    {
        // Escaped names and values are not handled correctly by these regular expressions;
        // we would need a full SQL lexer to handle them.
        $location = array();
        if (preg_match('@^UPDATE "([^"]+)" SET "([^"]+)" = @', $sql, $match)) {
            $location['table'] = $match[1];
            $location['column'] = $match[2];

            if (preg_match('@ WHERE "([^"]+)" = \'([^\']+)\' AND @', $sql, $match)) {
                $location['idname'] = $match[1];
                $location['id'] = $match[2];
                return $location;
            }
        } elseif (preg_match('@^INSERT INTO "([^"]+)" \("([^"]+)"@', $sql, $match)) {
            // We cannot determine the primary key from the INSERT statement,
            // so just take the first column for the message
            $location['table'] = $match[1];
            $location['idname'] = $match[2];

            if (preg_match('@\) VALUES \(\'([^\']+)\'@', $sql, $match)) {
                $location['id'] = $match[1];
                return $location;
            }
        }
        return null;
    }

    /**
     * Find SQL statements associated to the same action.
     *
     * The default behavior of this function is to restore the backup file by
     * executing every SQL statement individually after splitting the file by new
     * lines, this clearly poses a problem with certain statements, for example,
     * with stored procedures where they could be composed of multiple lines and
     * separated by the DELIMITER keyword. This iteration is trying to fix this
     * problem by executing all the SQL statements previously collected with a
     * DELIMITER keyword.
     *
     * @param string $content All SQL statements that will be executed.
     *
     * @return array           SQL statements grouped by actions.
     */
    public function getMultiSQLStatements($content = '')
    {
        $sqlGroup = '';
        $sqlGroups = array();
        $appendToGroup = false;

        /* Clean and formalize SQL statements */
        // $content = str_replace("\x60", '', $content);
        $content = str_replace(';END', ";\nEND", $content);
        $content = str_replace("\x20END", "\nEND", $content);
        $content = str_replace("\x20BEGIN", "\nBEGIN", $content);

        $lines = explode("\n", $content); /* assume unique lines */

        foreach ($lines as $line) {
            $line = trim($line);

            /* Skip empty lines, comments and SQL delimiters */
            if (empty($line)
                || strpos($line, "--\x20") === 0
                || strpos($line, 'DELIMITER') === 0
            ) {
                continue;
            }

            if (strpos($line, 'CREATE DEFINER') === 0
                || strpos($line, 'CREATE FUNCTION') === 0
                || strpos($line, 'CREATE PROCEDURE') === 0
            ) {
                $appendToGroup = true;
                $sqlGroup = $line;

                /**
                 * Close group if the END was in the previous line.
                 *
                 * In some cases when the group of SQL statements is all merged in
                 * a single line, both the CREATE and END keywords will be at the
                 * beginning and end of the same instruction, making this condition
                 * more or less irrelevant. To prevent conflicts with other groups
                 * we will check if this line also contains the END keyword and
                 * close the group right here.
                 */
                if (strpos($line, ';END') !== false
                    || strpos($line, ";\x20END") !== false
                ) {
                    $appendToGroup = false;
                    $sqlGroups[] = $sqlGroup;
                }

                continue;
            }

            if (strpos($line, 'END') === 0) {
                $appendToGroup = false;
                $sqlGroups[] = $sqlGroup . "\n" . $line;
                continue;
            }

            if ($appendToGroup) {
                $sqlGroup .= "\n" . $line;
                continue;
            }

            $sqlGroups[] = $line;
        }

        return $sqlGroups;
    }
    /**
     * Prepares the DB Search condition based upon the SRConfig.
     *
     * @param SRConfig $config Search and replace config for current search.
     *
     * @return array Array containing the conditions.
     */
    public function prepareCondition(&$config)
    {
        $condition = array(' WHERE ');
        if ($config->search[0] === self::POSIX_DELIM && substr($config->search, -1) === self::POSIX_DELIM) {
            array_push($condition, ' REGEXP ' . $this->driver->escapeStr(substr($config->search, 1, -1)));
            // Update config
            $config->is_regex = true;
        } else {
            array_push($condition, ' LIKE ' . $this->driver->escapeStr('%' . $this->escapeLike($config->search) . '%') .
                " ESCAPE '$'");
        }
        return $condition;
    }
    /**
     * Escapes the LIKE keyword in an SQL statement.
     *
     * @param string $trigger SQL statement to be escaped.
     *
     * @return string Clean version of the SQL statement.
     */
    public function escapeLike($trigger)
    {
        return str_replace(array('$', '%', '_'), array('$$', '$%', '$_'), $trigger);
    }
}

/**
 * File Utility
 */
class FileUtility extends MSSLUtility
{
    protected $extensions = array(
        'normal'   => array(
            'html',
            'shtml',
            'php',
            'css',
            'js',
            'php',
            'htm',
            'shtml',
            'inc',
            'tmpl',
            'tpl',
            'css',
            'pl',
            'py',
            'sh',
            'cgi',
            'asp',
            'module',
            'cron',
            'config',
            'local',
            'bashrc',
            'aspx',
            'master',
            'xml'
        ),
        'extended' => array( //TODO: what is 'extended' for? I don't see it used anywhere
            'pht',
            'js',
            'sct',
            'cfm',
            'asa',
            'module',
            'cin',
            'ejl',
            'izo',
            'cron',
            'config',
            'local',
            'bashrc'
        )
    );
    protected $scan_count   = 0;
    protected $total_count  = 0;
    protected $resume_count = 0;
    protected $files        = array();
    protected $groups       = array();
    protected $errors       = array();
    protected $scan_time    = 0;
    protected $last_file    = null;
    protected $scanning     = false;
    protected $backups_group;
    protected $backup_appendix = '_easysslbackup.';
    protected $backup_pattern;

    /**
     * Handles multiple required checks.
     *
     * 1. Checks the Backup Directory
     * 2. Sets the Backups Group (Time)
     * 3. Sets the Backups Pattern
     * 4. Registers our shutdown function for resumes
     */
    public function __construct()
    {
        // Create Backup Directory
        if (!$this->createBackupFolder()) {
            throw new Exception('Unable to create backup folder.');
        }
        // Set our Backups Group
        $this->backups_group = time();
        // Set our Backup Pattern
        $this->backupPattern = '@' . BACKUP_FOLDER . DIRECTORY_SEPARATOR . '(.*?)' . $this->backup_appendix .
            '([0-9]*)(' . BACKUP_GROUP_DELIMITER . '[A-Za-z0-9_-]*)?@';
        // Register Shutdown Function (Saves current location of scan for resume)
        register_shutdown_function(array(&$this, 'shutdownHandler'));
    }

    /**
     * Writes resume data to BACKUP_FOLDER/.resume_dir
     *
     * This function is always called upon shutdown.
     *
     * If connection is terminated by the server no data will be sent to the client.
     *
     * In this case the data is still written to the file but
     * no response will be sent to the client.
     *
     * @return void
     */
    public function shutdownHandler()
    {
        if ($this->scanning === true) {
            // Save Last file scanned and count in resume file as JSON
            $saved = $this->filePutContents(
                BACKUP_FOLDER . DIRECTORY_SEPARATOR . '.resume_dir',
                safe_json_encode(
                    array(
                        'last_file'    => $this->last_file,
                        'scan_time'    => $this->scan_time,
                        'scan_count'   => $this->scan_count,
                        'total_count'  => $this->total_count,
                        'resume_count' => $this->resume_count,
                        'files'        => $this->files,
                        'errors'       => $this->errors
                    )
                )
            );
            // Get the last error
            $error = error_get_last();
            // Write Timeout JSON to Client
            echo safe_json_encode(
                array(
                    'files'  => $this->files,
                    'errors' => $this->errors,
                    'debug'  => array(
                        'saved'        => $saved,
                        'save_file'    => BACKUP_FOLDER . DIRECTORY_SEPARATOR . '.resume_dir',
                        'last_file'    => $this->last_file,
                        'scan_time'    => $this->scan_time,
                        'scan_count'   => $this->scan_count,
                        'total_count'  => $this->total_count,
                        'resume_count' => $this->resume_count,
                        'timeout'      => true,
                        'error'        => $error
                    )
                )
            );
        } else {
            // Clear Resume file
            $f = @fopen(BACKUP_FOLDER . DIRECTORY_SEPARATOR . '.resume_dir', 'r+');
            if ($f !== false) {
                ftruncate($f, 0);
                fclose($f);
            }
        }
    }

    /**
     * Reads JSON from BACKUP_FOLDER/.resume_dir
     *
     * @return array Contains resume data if successful otherwise null.
     */
    public function getResumeData()
    {
        // Read Resume File
        try {
            $resume_data = @file_get_contents(BACKUP_FOLDER . DIRECTORY_SEPARATOR . '.resume_dir');
            $resume_json = json_decode($resume_data, true);
            if ($resume_json === null) {
                throw new Exception('Unable to read resume data');
            }
            return array(
                'files'        => $resume_json['files'],
                'errors'       => $resume_json['errors'],
                'path'         => $resume_json['last_file'],
                'dir'          => dirname($resume_json['last_file']),
                'file'         => basename($resume_json['last_file']),
                'scan_time'    => $resume_json['scan_time'],
                'scan_count'   => $resume_json['scan_count'],
                'total_count'  => $resume_json['total_count'],
                'resume_count' => $resume_json['resume_count'],
                'status'       => true
            );
        } catch (Exception $e) {
            return array(
                'files'        => null,
                'errors'       => null,
                'dir'          => '.',
                'file'         => null,
                'scan_time'    => null,
                'scan_count'   => null,
                'total_count'  => null,
                'resume_count' => null,
                'status'       => false
            );
        }
    }

    /**
     * Searches the files based upon the provided config.
     *
     * @param SRConfig $sr_config Search/replace config.
     *
     * @return array|Exception Returns array containing files and debug data or exception on critial error.
     */
    public function search($sr_config)
    {
        // Create Backup Directory (Store Resume File)
        if (!$this->createBackupFolder()) {
            throw new Exception('Unable to create backup folder.');
        }
        // Get Resume Data
        $resume = $this->getResumeData();
        // Check Resume Data & Scan
        if ($resume['status'] === true) {
            // Set Scan Count, Total Count, Resume Count from Resume Data
            $this->scan_count = $resume['scan_count'];
            $this->total_count = $resume['total_count'];
            $this->resume_count = $resume['resume_count'] + 1;
            // Set Files & Errors from Resume Data
            $this->files = $resume['files'];
            $this->errors = $resume['errors'];
            // Set Scan Time
            $this->scan_time = $resume['scan_time'];
            // Get Starting Loc & Starting Scan Dir
            $start_loc = basename($resume['path']);
            $scan_dir = rtrim(
                substr($resume['path'], 0, strlen($resume['path']) - strlen($start_loc)),
                DIRECTORY_SEPARATOR
            );
            // Start Scanning
            $this->scanning = true;
            while (!empty($scan_dir)) {
                // Run Scan
                $this->recursiveLoop($sr_config, $scan_dir, $start_loc);
                // Trim off the directory/file
                $start_loc = basename($scan_dir);
                $scan_dir = rtrim(substr($scan_dir, 0, strlen($scan_dir) - strlen($start_loc)), DIRECTORY_SEPARATOR);
            }
            // Cleanup
            $this->scanning = false;
        } else {
            // No Resume Data
            $this->scanning = true;
            $this->recursiveLoop($sr_config, '.');
            $this->scanning = false;
        }
        // Return Results
        return array(
            'files'  => $this->files,
            'errors' => $this->errors,
            'debug'  => array(
                'scan_time'    => $this->scan_time,
                'scan_count'   => $this->scan_count,
                'total_count'  => $this->total_count,
                'resume_count' => $this->resume_count
            )
        );
    }

    /**
     * Allows viewing of files.
     *
     * @param File $fileObj Empty file obj to load with data.
     *
     * @return void
     */
    public function viewFile(&$fileObj)
    {
        if (strpos($fileObj->path, BACKUP_FOLDER . DIRECTORY_SEPARATOR) !== false &&
            strpos($fileObj->path, $this->backup_appendix) !== false) {
            $fileObj->path = preg_replace('@\.\./@', '$up$/', $fileObj->path);
            $fileObj->is_backup = true;
        }

        $fileObj->is_link = @is_link($filepath);

        if (!is_readable($fileObj->path)) {
            $fileObj->errors['unable_read_file'] = true;
            return false;
        }

        $fileObj->contents = file_get_contents($fileObj->path);

        if ($fileObj->is_backup) {
            $fileObj->contents = $this->encrypt($fileObj->contents, EASYSSL_KEY, 0);
        }

        if ($fileObj->is_link) {
            $fileObj->contents = readlink($fileObj->path);
        }
    }

    /**
     * Gets all backups from BACKUP_FOLDER
     *
     * @param BUPConfig $config Backup configuration.
     *
     * @return array|Exception Contains groups/errors/debug or exception.
     */
    public function backups($config)
    {
        // Create Backup Directory (Store Resume File)
        if (!$this->createBackupFolder()) {
            throw new Exception('Unable to create backup folder.');
        }
        // Loop
        $this->recursiveLoop($config, BACKUP_FOLDER . DIRECTORY_SEPARATOR);
        krsort($this->groups);
        return array(
            'groups' => $this->groups,
            'errors' => $this->errors,
            'debug'  => array(
                'scan_time'    => $this->scan_time,
                'scan_count'   => $this->scan_count,
                'total_count'  => $this->total_count,
                'resume_count' => $this->resume_count
            )
        );
    }

    /**
     * Restores file or group of files.
     *
     * @param string $restore_val Group or single file.
     * @param string $version     Version to use for single file restores.
     *
     * @return void
     */
    public function restore($restore_val, $version)
    {
        // Check if group restore
        if (0 === strpos($restore_val, 'group:')) {
            // Set Backup Type
            $backups_group_type = 'restoregroup';
            // Get Target Group
            $target_group = substr($restore_val, 6);
            // Get Backups
            $bup_config = new BUPConfig($target_group);
            $this->backups($bup_config);
        } else {
            // Set Backup Type
            $backups_group_type = 'restorefile';
            // Get Target Group
            $group = explode(BACKUP_GROUP_DELIMITER, $version);
            if (!isset($group[1])) {
                throw new Error('Invalid restore version');
            }
            $target_group = $group[1];
            $bup_config = new BUPConfig($target_group);
            $this->backups($bup_config);

            // Get full path to restore file
            $full_path = BACKUP_FOLDER . ltrim($restore_val, '.') . $this->backup_appendix . $version;
        }

        // Check to ensure we have some backups to restore
        if (!empty($this->groups)) {
            // Restore Backups
            foreach ($this->groups as $group_name => &$group_data) {
                if ($group_name === $target_group) {
                    // Restore files from this group
                    foreach ($group_data->files as $file_path => &$file_data) {
                        // Check full path
                        if ($full_path != null && $file_path != $full_path) {
                            unset($this->groups[$group_name]->files[$file_path]);
                            continue;
                        }
                        // Restore
                        $backupcopy = $this->restoreFile($file_data, $backups_group_type);
                        if (!$backupcopy) {
                            $file_data->errors['unable_restore_file'] = true;
                            return false;
                        }
                    }
                }
            }
        }
        return array(
            'groups' => $this->groups,
            'errors' => $this->errors,
            'debug'  => array(
                'scan_time'    => $this->scan_time,
                'scan_count'   => $this->scan_count,
                'total_count'  => $this->total_count,
                'resume_count' => $this->resume_count
            )
        );
    }

    /**
     * Main Loop for recursing through files/folders
     * Used by backups and search
     *
     * @param SRConfig|BUPSConfig $config    Config for the recursive loop.
     * @param string              $scan_dir  Starting scan directory.
     * @param string              $start_loc Starting item in the directory (resume)
     *
     * @return void
     */
    public function recursiveLoop($config, $scan_dir, $start_loc = null)
    {
        // Trailing Backslash for dir
        $dir = rtrim($scan_dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        // Get Directory Handle
        $handle = opendir($dir);
        if (!$handle) {
            $this->errors[$dir] = true;
            exit;
        }
        // Start Loop
        while (false !== ($file_name = @readdir($handle))) {
            if ($file_name === '.' || $file_name === '..' || $file_name === '.easyssl_backup') {
                continue;
            }
            // Time Scan
            $start_time = microtime(true);
            // Keep track of total files
            $this->total_count += 1;
            // Skip until after we find the start file
            if (!empty($start_loc)) {
                if ($start_loc == $file_name) {
                    $start_loc = null;
                }
                continue;
            }
            // Path
            $path = $dir . $file_name;
            if (is_link($path)) {
                continue;
            }
            // Update Count
            $this->scan_count += 1;
            // Check if directory and recursive loop
            if (is_dir($path)) {
                $this->recursiveLoop($config, $path);
                continue;
            }
            // File
            $fileObj = new File($path);
            // Search & Replace
            if (get_class($config) == 'SRConfig') {
                // Check Extension
                if (!in_array(pathinfo(strtolower($fileObj->path), PATHINFO_EXTENSION), $this->extensions['normal'])) {
                    continue;
                }
                // Check for matches
                $this->checkfile($fileObj, $config);
                // Replace on File
                if ($config->action === 'replace' && $fileObj->matched === true) {
                    $this->cleanfile($fileObj, $config);
                }
                // Add to files
                if ($config->noise === true || $fileObj->matched === true) {
                    $this->files[$fileObj->path] = $fileObj;
                }
                // Add to errors
                if (count($fileObj->errors) > 0) {
                    $this->errors[$fileObj->path] = $fileObj;
                }
            }
            // Backups
            if (get_class($config) == 'BUPConfig') {
                // Check file appendix
                if (false === strpos($fileObj->name, $this->backup_appendix)) {
                    continue;
                }
                // Backup File
                $fileObj->is_backup = true;

                // Explode to get parts
                $backup_appendix_split = explode($this->backup_appendix, $fileObj->path);
                $fileObj->version = $backup_appendix_split[1];
                $group = explode(BACKUP_GROUP_DELIMITER, $backup_appendix_split[1]);
                $restore_path = '.' . str_replace(BACKUP_FOLDER, '', $backup_appendix_split[0]);
                $fileObj->restore_path = $restore_path;

                // File Time
                if (isset($group[0])) {
                    $fileObj->backup_time = $group[0];
                }
                // Check for group (Should have a group)
                $group_name = 'nogroup';
                if (isset($group[1])) {
                    $group_name = $group[1];
                }
                // Check for target_group
                if ($config->target_group && $config->target_group != $group_name) {
                    continue;
                }

                // Create group
                if (!isset($this->groups[$group_name])) {
                    $this->groups[$group_name] = new Group($group_name);
                }

                // Get group name/timestamp
                if (preg_match('@([0-9]+)(.*)@', $group_name, $group_matches)) {
                    // Get type/name
                    $group_time = $group_matches[1];
                    $group_type = $group_matches[2];
                    // Set
                    $this->groups[$group_name]->group_time = $group_time;
                    $this->groups[$group_name]->group_type = $group_type;
                }

                // Add File to Group
                $this->groups[$group_name]->files[$fileObj->path] = $fileObj;
            }

            // Set last scanned file
            $this->last_file = $fileObj->path;
            // Add to Scan Time
            $this->scan_time += (microtime(true) - $start_time);
        }
        closedir($handle);
    }

    /**
     * Searches file for $sr_config->search
     *
     * @param File     $fileObj   Pointer to $fileObj from loop.
     * @param SRConfig $sr_config Search/replace config.
     *
     * @return bool Returns true if matched false if not.
     */
    public function checkfile(&$fileObj, $sr_config)
    {
        $fh = @fopen($fileObj->path, 'r');
        if (!$fh) {
            $fileObj->errors['unable_read_file'] = true;
            return $fileObj->matched;
        }
        while (($buffer = fgets($fh, 4096)) !== false) {
            // Check for matches
            if ($sr_config->is_regex === true) {
                if (@preg_match("/$sr_config->search/i", $buffer, $matches) > 0) {
                    $fileObj->matched = true;
                }
            } else {
                if (stripos($buffer, $sr_config->search) !== false) {
                    $fileObj->matched = true;
                }
            }
            if ($fileObj->matched == true) {
                break;
            }
        }
        fclose($fh);
        $fileObj->scanned = true;
        return $fileObj->matched;
    }

    /**
     * Backs up file and replaces content based on config
     *
     * @param File     $fileObj   Pointer to fileObj from loop.
     * @param SRConfig $sr_config Search/replace config.
     *
     * @return bool True if modified false if not.
     */
    public function cleanfile(&$fileObj, $sr_config)
    {
        $newfile = file($fileObj->path);
        if ($newfile === false) {
            $fileObj->errors['unable_read_file'] = true;
            return false;
        }
        if (empty($newfile)) {
            return false;
        }

        $backupcopy = $this->backupFile($fileObj->path, $newfile);
        if (!$backupcopy) {
            $fileObj->errors['unable_backup_file'] = true;
            return false;
        }
        $fileObj->backup_path = $backupcopy;

        $fp = @fopen($fileObj->path, 'w');
        if (!$fp) {
            @chmod($fileObj->path, 0644);
            $realdir = dirname($fileObj->path);
            @chmod($realdir, 0755);
            $fp = fopen($fileObj->path, 'w');
            if (!$fp) {
                @unlink($backupcopy);
                $fileObj->errors['unable_open_write_file'] = true;
                return false;
            }
            @chmod($realdir, 0755);
            @chmod($fileObj->path, 0644);
        }

        foreach ($newfile as $line) {
            // Replace
            if ($sr_config->is_regex === true) {
                $newline = preg_replace("/$sr_config->search/i", $sr_config->replace, $line);
            } else {
                $newline = str_ireplace($sr_config->search, $sr_config->replace, $line);
            }
            if ($newline != $line) {
                $line = $newline;
                $fileObj->modified = true;
            }
            fwrite($fp, $line);
        }
        fclose($fp);

        // Mark as scanned
        $fileObj->scanned = true;

        if ($fileObj->modified === false) {
            @unlink($backupcopy);
        }
        return ($fileObj->modified);
    }

    /**
     * Used to backup files
     *
     * @param string       $fileName     Relative path from script location to file.
     * @param string|array $fileContents Contents of the file being backed up.
     * @param string       $groupType    Group type should always be default 'files'
     *
     * @return bool|string File path on success and false on error
     */
    public function backupFile($fileName, $fileContents, $groupType = 'files')
    {
        $appendix = $this->backup_appendix . time() . BACKUP_GROUP_DELIMITER . $this->backups_group . $groupType;

        // Check backup folder
        if (!$this->createBackupFolder()) {
            return false;
        }

        //if exists, check for the files folder in the quaranteen folder
        if (!$this->createDirectory(BACKUP_FOLDER . DIRECTORY_SEPARATOR . dirname($fileName))) {
            return false;
        }

        //encrypt the file and save in that folder
        $backupFile = BACKUP_FOLDER . DIRECTORY_SEPARATOR . $fileName . $appendix;

        $fp = @fopen($backupFile, 'w');
        if (!$fp) { //if cannot create file, fail
            return false;
        }

        if (is_array($fileContents)) {
            $fileContents = implode($fileContents);
        }

        fwrite($fp, $this->encrypt($fileContents, EASYSSL_KEY, 0));
        fclose($fp);

        //putting last change time on backup file for research purposes
        $time = filemtime($fileName);
        touch($backupFile, $time);

        return ($backupFile);
    }

    /**
     * Restores the provided file
     *
     * @param File   $fileObj    Pointer to fileObj that is being restored
     * @param string $group_type Group type to pass over to _backupFile
     *
     * @return bool True on success, false on fail/err
     */
    public function restoreFile(&$fileObj, $group_type)
    {
        if (!strpos($fileObj->path, $this->backup_appendix)) {
            return false;
        }

        $fileContents = file_get_contents($fileObj->path);

        $realFilename = explode($this->backup_appendix, $fileObj->path);
        $realFilename = $realFilename[0];
        $realFilename = substr($realFilename, strlen(BACKUP_FOLDER . DIRECTORY_SEPARATOR));

        if (!file_exists(dirname($realFilename))) {
            mkdir(dirname($realFilename), 0755, true);
        } elseif (file_exists($realFilename)) {
            $backupcopy = $this->backupFile($realFilename, file_get_contents($realFilename), $group_type);

            if (!$backupcopy) {
                $fileObj->errors['unable_backup_realfile'] = true;
                return false;
            }
        }

        $fp = @fopen($realFilename, 'w');
        fwrite($fp, $this->encrypt($fileContents, EASYSSL_KEY, 0));
        fclose($fp);

        return true;
    }

    /**
     * Creates directory recursively
     *
     * @param string $directory Path to directory.
     *
     * @return bool
     */
    public function createDirectory($directory)
    {
        if (!is_dir($directory)) { //if it doesn't exist, create it
            if (!@mkdir($directory, 0755, true)) { //if cannot create, fail
                return false;
            }
        }

        return true;
    }

    /**
     * File_put_contents
     *
     * @param string $file    Path to file
     * @param string $content Contents of file
     *
     * @return void
     */
    public function filePutContents($file, $content)
    {
        $fh = fopen($file, 'w');
        if (!$fh) {
            return false;
        }
        if (flock($fh, LOCK_EX)) {
            fwrite($fh, $content);
            fflush($fh);
            flock($fh, LOCK_UN);
        } else {
            return false;
        }
        fclose($fh);
        return true;
    }
}

/**
 * Login Bypass
 */
class LoginBypass
{
    /**
     * Gets all the admin users for WordPress
     *
     * @throws Exception If wp-config.php isn't found.
     * @return array Returns array of users.
     */
    public static function wordpressUsers()
    {
        if (file_exists('./wp-config.php')) {
            include './wp-config.php';
        } elseif (file_exists('../wp-config.php')) {
            include '../wp-config.php';
        } else {
            throw new Error('wp-config.php file not available');
        }
        $wp_admin_users = get_users('blog_id=1&orderby=id&role=administrator');
        $users = array();
        foreach ($wp_admin_users as $user) {
            array_push(
                $users,
                array(
                    'id'   => $user->ID,
                    'user' => $user->user_login,
                    'url'  => get_site_url()
                )
            );
        }
        return $users;
    }

    /**
     * Attempts to login to WordPress as the provided user
     *
     * @param string $username Existing Wordpress user_login
     *
     * @return void
     */
    public static function wordpressLogin($username)
    {
        include './wp-config.php';
        // Get admin url
        if (function_exists('get_admin_url')) {
            $url = get_admin_url();
        } else {
            $url = admin_url();
        }
        if (!is_user_logged_in()) {
            if (function_exists('get_user_by')) {
                $user = get_user_by('login', $username);
            } else {
                $user = get_userdatabylogin($username);
            }

            if (!$user) {
                throw new Error("User doesn't exist");
            }

            $user_id = $user->ID;
            wp_set_current_user($user_id, $username);
            wp_set_auth_cookie($user_id);
            do_action('wp_login', $username);
        }
        wp_redirect($url);
    }

    /**
     * Hooks into Joomla
     *
     * @return void
     */
    public static function bootstrapJoomla()
    {
        define('_JEXEC', 1);
        define('JPATH_BASE', dirname(__FILE__));
        define('DS', DIRECTORY_SEPARATOR);

        if (file_exists(JPATH_BASE . '/includes/defines.php')) {
            include_once JPATH_BASE . '/includes/defines.php';
        }
        if (!defined('_JDEFINES')) {
            include_once JPATH_BASE . '/includes/defines.php';
        }
        include_once JPATH_BASE . '/includes/framework.php';

        $mainframe = &JFactory::getApplication('administrator');
        $mainframe->initialise();

        jimport('joomla.user.helper');

        return $mainframe;
    }

    /**
     * Gets all of the Joomla admin users
     *
     * @throws Exception If unable to get Joomla Users
     * @return array Returns array of users
     */
    public static function joomlaListUsers()
    {
        self::bootstrapJoomla();
        $db = &JFactory::getDBO();
        $query = "SELECT u.id, u.username
                    FROM `#__usergroups` us, `#__users` u, `#__user_usergroup_map` uum
                    WHERE us.id = uum.group_id
                    AND uum.user_id = u.id
                    AND `title` IN ('Super Users', 'Administrator', 'Super Administrator')";
        if (version_compare(JVERSION, '1.6.0', 'lt')) {
            $query = "SELECT u.id, u.username
                    FROM `#__users` u
                    WHERE u.usertype = 'Super Administrator'";
        }
        $db->setQuery($query);
        $joomla_users = $db->loadObjectList();
        if (!$joomla_users) {
            throw new Error('Failed to get Joomla Users');
        }
        $users = array();
        foreach ($joomla_users as $user) {
            array_push(
                $users,
                array(
                    'id'   => $row->id,
                    'user' => $row->username,
                    'url'  => JURI::root()
                )
            );
        }
        return $users;
    }

    /**
     * Attempts to login to Joomla as the provided user
     *
     * @param string $username Existing Joomla username
     *
     * @return void
     */
    public static function joomlaLogin($username)
    {
        $mainframe = self::bootstrapJoomla();
        JPluginHelper::importPlugin('user');
        if (version_compare(JVERSION, '1.6.0', 'ge')) {
            $result = $mainframe->triggerEvent(
                'onUserLogin',
                array(
                    array('username' => $username),
                    array('action' => 'core.login.admin')
                )
            );
        } else {
            $result = $mainframe->triggerEvent('onLoginUser', array(array('username' => $username), array()));
        }

        if ($result[0] != false) {
            // Redirect
            $app = JFactory::getApplication();
            $app->redirect(rtrim(JURI::root(), '/') . '/administrator/');
        } else {
            echo 'Could not log in!';
        }
    }

    /**
     * Hooks into Drupal
     *
     * @throws Exception If version isn't supported.
     * @return void
     */
    public static function bootstrapDrupal()
    {
        define('DRUPAL_ROOT', getcwd());
        if (is_file(DRUPAL_ROOT . '/includes/bootstrap.inc')) {
            include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
        } elseif (is_file(DRUPAL_ROOT . '/core/includes/bootstrap.inc')) {
            throw new Exception('Drupal version not supported');
        } else {
            throw new Exception('Unable to load bootstrap.inc');
        }
        drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    }

    /**
     * Gets all of the Drupal admin users
     *
     * @return array Returns array of users.
     */
    public static function drupalListUsers()
    {
        self::bootstrapDrupal();
        // phpcs:ignore
        $drupal_users = db_query('SELECT a.uid, a.name as "username", c.name as "role" FROM {users} a LEFT JOIN {users_roles} b ON a.uid = b.uid LEFT JOIN {role} c ON b.rid = c.rid WHERE status = 1')->fetchAll();
        $users = array();
        global $base_url;
        if (is_array($drupal_users)) {
            foreach ($drupal_users as $user) {
                if ($user->role == 'administrator') {
                    array_push(
                        $users,
                        array(
                            'id'   => $user->uid,
                            'user' => $user->username,
                            'url'  => $base_url
                        )
                    );
                }
            }
        }
        return $users;
    }

    /**
     * Attempts to login to Drupal as the provided user
     *
     * @param string $userID Existing drupal userID
     *
     * @return void
     */
    public static function drupalLogin($userID)
    {
        self::bootstrapDrupal();
        global $user;
        $user = user_load((int) $userID);
        user_login_finalize();
        if (user_is_logged_in()) {
            drupal_goto('admin/dashboard');
        } else {
            echo 'Unable to login to Drupal';
        }
    }

    /**
     * Hooks into Magento
     *
     * @throws Exception If magento version isn't supported.
     * @return void
     */
    public static function bootstrapMagento()
    {
        define('MAGENTO', realpath(dirname(__FILE__)));
        if (is_file(MAGENTO . '/app/Mage.php')) {
            include_once MAGENTO . '/app/Mage.php';
        } elseif (is_file(MAGENTO . '/app/autoload.php')) {
            throw new Exception('Magento version not supported');
        } else {
            throw new Exception('Unable to load /app/Mage.php');
        }

        Mage::app();
        $app = Mage::app();
        umask(0);
        Mage::app('default');
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));
    }

    /**
     * Gets all of the Magento admin users
     *
     * @return array Returns array of users.
     */
    public static function magentoListUsers()
    {
        self::bootstrapMagento();
        $roles_users = Mage::getResourceModel('admin/roles_user_collection');
        $users = array();
        foreach ($roles_users as $roleuser) {
            $user = Mage::getModel('admin/user')->load($roleuser->getUserId());
            array_push(
                $users,
                array(
                    'id'   => $roleuser->getUserId(),
                    'user' => $user->getUsername(),
                    'url'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
                )
            );
        }
        return $users;
    }

    /**
     * Attempts to login to Drupal as the provided user
     *
     * @param string $username Existing Magento username
     *
     * @return void
     */
    public static function magentoLogin($username)
    {
        self::bootstrapMagento();
        //If the username variable is set then we login as that user.
        $user = Mage::getModel('admin/user')->loadByUsername($username); // Here we set the username.

        //Auto login code - taken from stackoverflow
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }

        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));

        if ($session->isLoggedIn()) {
            // Redirect to the admin page.
            $redirectUrl = Mage::getSingleton('adminhtml/url')->getUrl(
                Mage::getModel('admin/user')->getStartupPageUrl(),
                array('_current' => false)
            );
            //Fix url to compensate for Magento hacking
            $redirectUrl = str_replace(basename($_SERVER['PHP_SELF']), 'index.php', $redirectUrl);
            header('Location: ' . $redirectUrl);
        } else {
            echo 'Magento login failed. Try logging in manually.';
        }
    }
}

/**
 * Database Config Finder
 *
 * All methods are static
 */
class DatabaseConfig
{

    const OPT_TYPE_CONST     = 1;
    const OPT_TYPE_VAR       = 2;
    const OPT_TYPE_ASSOC     = 4;
    const OPT_TYPE_ENV       = 8;
    const OPT_TYPE_XML       = 16;
    const OPT_TYPE_FIRST     = 32;
    const OPT_TYPE_CONST_NUM = 64;

    /**
     * Gets Option Match
     *
     * @param string $m    Match
     * @param int    $type Type
     *
     * @return string
     */
    public static function getOptMatch($m, $type)
    {
        return ($type & self::OPT_TYPE_FIRST) ? $m[0] : $m[count($m) - 1];
    }

    /**
     * Gets Option
     *
     * @param string $option Option to get
     * @param array  $config Config to search
     * @param int    $type   Type
     *
     * @return void
     */
    public static function getOption($option, $config, $type)
    {
        $option = preg_quote($option);

        // String constants (WordPress)
        if (($type & self::OPT_TYPE_CONST)
            && preg_match_all('@^\s*define\(\s*([\'"])' . $option . '\1\s*,\s*([\'"])(.*)\2\s*\)\s*;@m', $config, $m)
        ) {
            return stripslashes(self::getOptMatch($m[3], $type));
        }

        // Numeric constants
        if (($type & self::OPT_TYPE_CONST_NUM)
            && preg_match_all('@^\s*define\(\s*([\'"])' . $option . '\1\s*,\s*(\d*)\s*\)\s*;@m', $config, $m)
        ) {
            return stripslashes(self::getOptMatch($m[2], $type));
        }

        // wp-config.php trick, see
        // http://www.wpbeginner.com/wp-tutorials/useful-wordpress-configuration-tricks-that-you-may-not-know/
        if (($type & self::OPT_TYPE_ENV) && preg_match(
            '@^\s*define\(\s*([\'"])' . $option . '\1\s*,\s*\$_ENV\s*[[{]\s*' .
            '([\'"]?)DATABASE_SERVER\2\s*[}\]]\s*\)\s*;@m',
            $config,
            $m
        )
        ) {
            return $_ENV['DATABASE_SERVER'];
        }

        // Variables (Joomla, WordPress prefix)
        if (($type & self::OPT_TYPE_VAR)
            && preg_match_all(
                '@^\s*(?:public|var)?\s*' . $option . '\s*=\s*(?:([\'"])(.*)\1|([0-9]+))\s*;@m',
                $config,
                $m
            )
        ) {
            // Return the string m[2] or the number m[3] if the string was not found
            $str = self::getOptMatch($m[2], $type);
            return $str ? stripslashes($str) : self::getOptMatch($m[3], $type);
        }

        // Associative arrays (Drupal)
        if (($type & self::OPT_TYPE_ASSOC)
            && preg_match_all('@(?:^|,|\()\s*([\'"])' . $option . '\1\s*=>\s*([\'"])(.*?)\2\s*[,)]@m', $config, $m)
        ) {
            return stripslashes(self::getOptMatch($m[3], $type));
        }

        // Magento XML config
        if (($type & self::OPT_TYPE_XML) && preg_match_all(
            '@^\s*<' . $option . '>(?:<!\[CDATA\[|\{\{)(.*)(?:}}|\]\]>)</' .
            $option . '>\s*$@m',
            $config,
            $m
        )
        ) {
            return self::getOptMatch($m[1], $type);
        }

        return '';
    }

    /**
     * Returns the credentials to connect to a WordPress database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readWordPressConfig($lines)
    {
        return array(
            'host'   => self::getOption('DB_HOST', $lines, self::OPT_TYPE_CONST | self::OPT_TYPE_ENV),
            'user'   => self::getOption('DB_USER', $lines, self::OPT_TYPE_CONST),
            'pass'   => self::getOption('DB_PASSWORD', $lines, self::OPT_TYPE_CONST),
            'db'     => self::getOption('DB_NAME', $lines, self::OPT_TYPE_CONST),
            'prefix' => self::getOption('$table_prefix', $lines, self::OPT_TYPE_VAR)
        );
    }

    /**
     * Returns the credentials to connect to a Magento1 database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readMagento1Config($lines)
    {
        return array(
            'host'   => self::getOption('host', $lines, self::OPT_TYPE_XML),
            'user'   => self::getOption('username', $lines, self::OPT_TYPE_XML),
            'pass'   => self::getOption('password', $lines, self::OPT_TYPE_XML),
            'db'     => self::getOption('dbname', $lines, self::OPT_TYPE_XML),
            'prefix' => self::getOption('table_prefix', $lines, self::OPT_TYPE_XML)
        );
    }

    /**
     * Returns the credentials to connect to a Magento2 database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readMagento2Config($lines)
    {
        return array(
            'host'   => self::getOption('host', $lines, self::OPT_TYPE_ASSOC),
            'user'   => self::getOption('username', $lines, self::OPT_TYPE_ASSOC),
            'pass'   => self::getOption('password', $lines, self::OPT_TYPE_ASSOC),
            'db'     => self::getOption('dbname', $lines, self::OPT_TYPE_ASSOC),
            'prefix' => self::getOption('table_prefix', $lines, self::OPT_TYPE_ASSOC)
        );
    }

    /**
     * Returns the credentials to connect to a Drupal database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readDrupalConfig($lines)
    {
        return array(
            'host'   => self::getOption('host', $lines, self::OPT_TYPE_ASSOC | self::OPT_TYPE_FIRST),
            'user'   => self::getOption('username', $lines, self::OPT_TYPE_ASSOC | self::OPT_TYPE_FIRST),
            'pass'   => self::getOption('password', $lines, self::OPT_TYPE_ASSOC | self::OPT_TYPE_FIRST),
            'db'     => self::getOption('database', $lines, self::OPT_TYPE_ASSOC | self::OPT_TYPE_FIRST),
            'prefix' => self::getOption('prefix', $lines, self::OPT_TYPE_ASSOC | self::OPT_TYPE_FIRST)
        );
    }

    /**
     * Returns the credentials to connect to a Joomla database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readJoomlaConfig($lines)
    {
        return array(
            'host'   => self::getOption('$host', $lines, self::OPT_TYPE_VAR),
            'user'   => self::getOption('$user', $lines, self::OPT_TYPE_VAR),
            'pass'   => self::getOption('$password', $lines, self::OPT_TYPE_VAR),
            'db'     => self::getOption('$db', $lines, self::OPT_TYPE_VAR),
            'prefix' => self::getOption('$dbprefix', $lines, self::OPT_TYPE_VAR)
        );
    }

    /**
     * Returns the credentials to connect to a Vbulletin database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readVbulletinConfig($lines)
    {
        // https://www.vbulletin.com/docs/html/main/install_core_config_php
        return array(
            'host'   => self::getOption("\$config['MasterServer']['servername']", $lines, self::OPT_TYPE_VAR),
            'user'   => self::getOption("\$config['MasterServer']['username']", $lines, self::OPT_TYPE_VAR),
            'pass'   => self::getOption("\$config['MasterServer']['password']", $lines, self::OPT_TYPE_VAR),
            'db'     => self::getOption("\$config['Database']['dbname']", $lines, self::OPT_TYPE_VAR),
            'port'   => self::getOption("\$config['MasterServer']['port']", $lines, self::OPT_TYPE_VAR),
            'prefix' => self::getOption("\$config['Database']['tableprefix']", $lines, self::OPT_TYPE_VAR)
        );
    }

    /**
     * Returns the credentials to connect to a PrestaShop database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readPrestaShopConfig($lines)
    {
        $lines = explode("\n", $lines);
        $dbconfig = array();

        foreach ($lines as $line) {
            preg_match('/^[^#]\s+([\'"])(.*)\1\s+=>\s+([\'"])(.*)\3/', $line, $matches);
            $key = isset($matches[2]) ? $matches[2] : null;
            $value = isset($matches[4]) ? $matches[4] : null;

            if (isset($key) && isset($value)) {
                $dbconfig[$key] = $value;
            }
        }

        $admin_profile_config = '.' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'defines.inc.php';
        $apc_lines = file_get_contents($admin_profile_config);
        $admin_profile_num = self::getOption('_PS_ADMIN_PROFILE_', $apc_lines, self::OPT_TYPE_CONST_NUM);

        return array(
            'host'     => $dbconfig['database_host'],
            'user'     => $dbconfig['database_user'],
            'pass'     => $dbconfig['database_password'],
            'db'       => $dbconfig['database_name'],
            'prefix'   => $dbconfig['database_prefix'],
            'admin_id' => $admin_profile_num
        );
    }

    /**
     * Returns the credentials to connect to a PrestaShop database (versions 1.6 and lower).
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readPrestaShopConfigOld($lines)
    {
        if (is_array($lines)) {
            $lines = implode("\n", $lines);
        }

        $admin_profile_config = '.' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'defines.inc.php';
        $apc_lines = file_get_contents($admin_profile_config);
        $admin_profile_num = self::getOption('_PS_ADMIN_PROFILE_', $apc_lines, self::OPT_TYPE_CONST_NUM);

        return array(
            'host'     => self::getOption('_DB_SERVER_', $lines, self::OPT_TYPE_CONST | self::OPT_TYPE_ENV),
            'user'     => self::getOption('_DB_USER_', $lines, self::OPT_TYPE_CONST),
            'pass'     => self::getOption('_DB_PASSWD_', $lines, self::OPT_TYPE_CONST),
            'db'       => self::getOption('_DB_NAME_', $lines, self::OPT_TYPE_CONST),
            'prefix'   => self::getOption('_DB_PREFIX_', $lines, self::OPT_TYPE_CONST),
            'admin_id' => $admin_profile_num,
            'old'      => true
        );
    }

    /**
     * Returns the credentials to connect to a OsCommerce database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readOsCommerceConfig($lines)
    {
        return array(
            'host'   => self::getOption('DB_SERVER', $lines, self::OPT_TYPE_CONST | self::OPT_TYPE_ENV),
            'user'   => self::getOption('DB_SERVER_USERNAME', $lines, self::OPT_TYPE_CONST),
            'pass'   => self::getOption('DB_SERVER_PASSWORD', $lines, self::OPT_TYPE_CONST),
            'db'     => self::getOption('DB_DATABASE', $lines, self::OPT_TYPE_CONST),
            'prefix' => self::getOption('DB_PREFIX', $lines, self::OPT_TYPE_CONST)
        );
    }

    /**
     * Returns the credentials to connect to a MODX database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readMODXConfig($lines)
    {
        return array(
            'host'   => self::getOption('$database_server', $lines, self::OPT_TYPE_VAR),
            'user'   => self::getOption('$database_user', $lines, self::OPT_TYPE_VAR),
            'pass'   => self::getOption('$database_password', $lines, self::OPT_TYPE_VAR),
            'db'     => self::getOption('$dbase', $lines, self::OPT_TYPE_VAR),
            'prefix' => self::getOption('$table_prefix', $lines, self::OPT_TYPE_VAR)
        );
    }

    /**
     * Returns the credentials to connect to a OpenCart database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readOpenCartConfig($lines)
    {
        return array(
            'host'   => self::getOption('DB_HOSTNAME', $lines, self::OPT_TYPE_CONST | self::OPT_TYPE_ENV),
            'user'   => self::getOption('DB_USERNAME', $lines, self::OPT_TYPE_CONST),
            'pass'   => self::getOption('DB_PASSWORD', $lines, self::OPT_TYPE_CONST),
            'db'     => self::getOption('DB_DATABASE', $lines, self::OPT_TYPE_CONST),
            'prefix' => self::getOption('DB_PREFIX', $lines, self::OPT_TYPE_CONST)
        );
    }

    /**
     * Returns the credentials to connect to a PhpBB database.
     *
     * @param string $lines Content of the configuration file.
     *
     * @return array        Credentials to connect to the database.
     */
    public static function readPhpBBConfig($lines)
    {
        return array(
            'host'   => self::getOption('$dbhost', $lines, self::OPT_TYPE_VAR),
            'user'   => self::getOption('$dbuser', $lines, self::OPT_TYPE_VAR),
            'pass'   => self::getOption('$dbpasswd', $lines, self::OPT_TYPE_VAR),
            'db'     => self::getOption('$dbname', $lines, self::OPT_TYPE_VAR),
            'prefix' => self::getOption('$table_prefix', $lines, self::OPT_TYPE_VAR)
        );
    }

    /**
     * Returns the website configuration.
     *
     * @param string $dir Directory where the website is installed.
     *
     * @return array|bool Website configuration or false on failure.
     */
    public static function getCMSConfigInDir($dir)
    {
        global $config;
        $cmses = array(
            'easyssl_db_config.php'      => array('easyssl_db_config.php', 'readWordPressConfig'),
            'wp-config.php'              => array('WordPress', 'readWordPressConfig'),
            'configuration.php'          => array('Joomla', 'readJoomlaConfig'),
            'sites/default/settings.php' => array('Drupal', 'readDrupalConfig'),
            'app/etc/local.xml'          => array('Magento1', 'readMagento1Config'),
            'app/etc/env.php'            => array('Magento2', 'readMagento2Config'),
            'includes/config.php'        => array('vBulletin', 'readVbulletinConfig'), // vBulletin 4
            'core/includes/config.php'   => array('vBulletin', 'readVbulletinConfig'), // vBulletin 5 and higher
            'config/settings.inc.php'    => array('PrestaShop', 'readPrestaShopConfigOld'), // Older prestashop
            'app/config/parameters.php'  => array('PrestaShop', 'readPrestaShopConfig'),
            'includes/configure.php'     => array('osCommerce', 'readOsCommerceConfig'),
            'core/config/config.inc.php' => array('MODX', 'readMODXConfig'),
            'admin/config.php'           => array('OpenCart', 'readOpenCartConfig'),
            'config.php'                 => array('phpBB', 'readPhpBBConfig')
        );

        // Detect the CMS
        foreach ($cmses as $filename => $cmsArray) {
            if (!is_readable($dir . $filename)) {
                continue;
            }

            list($cmsName, $configFunc) = $cmsArray;

            $config = call_user_func(array('DatabaseConfig', $configFunc), file_get_contents($dir . $filename));

            // At least these parameters must be present in the config file;
            // the prefix, the port number, and the password can be empty
            if ($config['host'] && $config['db'] && $config['user']) {
                $config['cms'] = $cmsName;
                $config['file'] = $dir . $filename;

                if (preg_match('/:([0-9]+)$/', $config['host'], $matches)) {
                    $config['host'] = substr($config['host'], 0, -strlen($matches[0]));
                    $config['port'] = $matches[1];
                }
                return $config;
            }
        }

        return false;
    }

    /**
     * Returns the website configurations.
     *
     * The function searches for the configuration file in the current directory,
     * then the parent directory, then in every directory that is under the current
     * location. Once it finds it, it adds the information contained
     * in the file to an array.
     *
     * @return array|bool|null Website configuration or false on failure.
     */
    public static function getCMSConfigs()
    {
        // Store Configs
        $configs = array();

        // Check . and .. first because readdir() can return them after the other directories
        $config = self::getCMSConfigInDir('.' . DIRECTORY_SEPARATOR);
        if ($config) {
            $configs[$config['file']] = $config;
        }

        $config = self::getCMSConfigInDir('..' . DIRECTORY_SEPARATOR);
        if ($config) {
            $configs[$config['file']] = $config;
        }

        $dh = opendir('.');
        if (!$dh) {
            return null;
        }

        while (false !== ($fileName = @readdir($dh))) {
            if (!is_dir($fileName) || $fileName === '.' || $fileName === '..') {
                continue;
            }

            $config = self::getCMSConfigInDir($fileName . DIRECTORY_SEPARATOR);
            if ($config) {
                $configs[$config['file']] = $config;
            }
        }
        closedir($dh);
        return $configs;
    }

    /**
     * Creates a file with the website configuration.
     *
     * If the script is not able to find the file with the website configuration,
     * this function can be used to create a temporary file that will be used to
     * expose the necessary data in order to connect to the database and proceed
     * with the rest of the operations.
     *
     * @param array $config Website configuration.
     *
     * @return void
     */
    public static function createConfigFile($config)
    {
        $contents = "<?php

    define( 'DB_NAME', '" . addslashes($config['dbname']) . "' );
    define( 'DB_USER', '" . addslashes($config['dbuser']) . "' );
    define( 'DB_PASSWORD', '" . addslashes($config['dbpass']) . "' );
    define( 'DB_HOST', '" . addslashes($config['dbhost']) . "' );

    \$table_prefix = '" . addslashes($config['prefix']) . "';
    \$cms = '" . addslashes($config['cms']) . "';

    ";

        $fh = fopen('easyssl_db_config.php', 'wb');
        if ($fh === false || false === fwrite($fh, $contents)) {
            fclose($fh);
            throw new Exception('Unable to store file: easyssl_db_config.php');
        }
        fclose($fh);

        return true;
    }
}

/**
 * DB abstraction layer
 */
class MySQLCommon
{
    public function escapeName($name)
    {
        // See https://dev.mysql.com/doc/refman/5.7/en/identifiers.html
        return '"' . str_replace('"', '""', $name) . '"';
    }

    public function begin()
    {
        $this->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
        $this->query('BEGIN;');
    }

    public function commit()
    {
        $this->query('COMMIT;');
    }

    public function rollback()
    {
        $this->query('ROLLBACK;');
    }

    public function getTablesColumns($config, $withCompositeKeys = false)
    {
        $scannedTypes = array(
            'varchar'  => 1, 'char'       => 1,
            'tinytext' => 1, 'mediumtext' => 1, 'text'       => 1, 'longtext' => 1,
            'binary'   => 1, 'varbinary'  => 1,
            'tinyblob' => 1, 'blob'       => 1, 'mediumblob' => 1, 'longblob' => 1);

        // Get all tables
        $tablesResult = $this->query('SHOW TABLES;');

        $allTables = array();
        while ($tablesRow = $this->fetchRow($tablesResult)) {
            // Report tables that does not begin with the Wordpress prefix
            $table = $tablesRow[0];

            $allTables[$table] = array();

            // Get all columns
            $columnsResult = $this->query('SHOW COLUMNS FROM ' . $this->escapeName($table) . ';');
            $uniqueString = null;
            $compositePrimary = false;
            $compositeKeys = array();
            while ($columnsRow = $this->fetchAssoc($columnsResult)) {
                $type = $columnsRow['Type'];
                $pos = strpos($type, '(');
                if ($pos !== false) {
                    $type = substr($type, 0, $pos);
                }

                if (strtoupper($columnsRow['Key']) === 'PRI') {
                    if (isset($allTables[$table]['idname'])) {
                        $compositePrimary = true;
                    }

                    if ($withCompositeKeys) {
                        $compositeKeys[] = $columnsRow['Field'];
                    }

                    $allTables[$table]['idname'] = $columnsRow['Field'];
                }

                $type = strtolower(trim($type));
                if (!isset($scannedTypes[$type])) {
                    continue;
                }

                // Prefer unique string keys to display wp_options.option_name instead of the numeric ID
                if (strtoupper($columnsRow['Key']) === 'UNI') {
                    $uniqueString = $columnsRow['Field'];
                }

                $allTables[$table][] = $columnsRow['Field'];
            }

            if ($uniqueString !== null) {
                $allTables[$table]['idname'] = $uniqueString;
            } elseif ($withCompositeKeys && count($compositeKeys) > 1) {
                /* return idname as an array of primary keys */
                $allTables[$table]['idname'] = $compositeKeys[0];
            } elseif ($compositePrimary) { // Disable the cleanup for composite primary keys and no primary key
                unset($allTables[$table]['idname']);
            }
        }
        return $allTables;
    }

    public function getColumnNamesTypes($table)
    {
        $columnsResult = $this->query('SHOW COLUMNS FROM ' . $this->escapeName($table) . ';');

        $binaryTypes = array('binary' => 1, 'varbinary' => 1,
            'tinyblob'                    => 1, 'blob'      => 1, 'mediumblob' => 1, 'longblob' => 1);

        $numericTypes = array('tinyint' => 1, 'smallint' => 1, 'mediumint' => 1,
            'int'                           => 1, 'integer'  => 1, 'bigint'    => 1,
            'decimal'                       => 1, 'dec'      => 1, 'numeric'   => 1, 'fixed'            => 1,
            'float'                         => 1, 'real'     => 1, 'double'    => 1, 'double precision' => 1);

        $names = array();
        $types = array();

        while ($columnsRow = $this->fetchAssoc($columnsResult)) {
            $names[] = $columnsRow['Field'];

            $type = $columnsRow['Type'];
            $pos = strpos($type, '(');
            if ($pos !== false) {
                $type = substr($type, 0, $pos);
            }
            $type = strtolower(trim($type));

            if (isset($numericTypes[$type])) {
                $types[] = 'num';
            } elseif (isset($binaryTypes[$type])) {
                $types[] = 'bin';
            } elseif ($type === 'bit') {
                $types[] = 'bit';
            } else {
                $types[] = 'other';
            }
        }

        return array($names, $types);
    }

    public function exportRow($row, $types)
    {
        for ($i = 0; $i < count($row); $i++) {
            if ($row[$i] === null) {
                $row[$i] = 'NULL';
            } elseif ($types[$i] === 'bin') {
                // Story binary data as hex to avoid encoding problems
                $row[$i] = 'UNHEX(' . $this->escapeStr(bin2hex($row[$i])) . ')';
            } elseif ($types[$i] === 'bit') {
                // TODO: tests under PHP 4
                // See https://stackoverflow.com/questions/15106985/
                $row[$i] = "b'" . decbin($row[$i]) . "'";
            } elseif ($types[$i] === 'num') {
                $row[$i] = (string) $row[$i];
            } else {
                $row[$i] = $this->escapeStr($row[$i]);
            }
        }
        return implode(', ', $row);
    }

    public function getCreateTableSql($table)
    {
        $res = $this->query('SHOW CREATE TABLE ' . $this->escapeName($table) . ';');
        $row = $this->fetchRow($res);

        if (!$row) {
            exit('Unexpected empty result from SHOW CREATE TABLE');
        }

        return $row[1] . ";\n\n";
    }

    public function getCreateDBSql($db)
    {
        $res = $this->query('SHOW CREATE DATABASE ' . $this->escapeName($db) . ';');
        $row = $this->fetchRow($res);

        if (!$row) {
            exit('Unexpected empty result from SHOW CREATE DATABASE');
        }

        return $row[1] . ";\nUSE " . $this->escapeName($db) . ";\n\n";
    }

    public function getDumpHeader()
    {
        return
            "SET NAMES 'utf8';\n" .
            "SET sql_mode='NO_AUTO_VALUE_ON_ZERO,ANSI_QUOTES,STRICT_TRANS_TABLES';\n" .
            "SET foreign_key_checks=0;\n";
    }

    public function setModes()
    {
        $this->query("SET NAMES 'utf8';");
        $this->query(
            "SET sql_mode='PIPES_AS_CONCAT,IGNORE_SPACE," .
            "NO_AUTO_VALUE_ON_ZERO,ANSI_QUOTES,STRICT_TRANS_TABLES';"
        );
    }
}

class MySQLDriver extends MySQLCommon
{
    public $link;

    public function connect($config)
    {
        // Connect to DB
        $host = $config['host'];
        if (isset($config['port'])) {
            $host .= ':' . $config['port'];
        }
        $this->link = @mysql_connect($host, $config['user'], $config['pass']);
        if ($this->link === false) {
            return 'Could not connect to MySQL database: ' . mysql_error();
        }

        mysql_select_db($config['db'], $this->link);

        $this->setModes();
        return true;
    }

    public function disconnect()
    {
        mysql_close($this->link);
    }

    public function escapeStr($str)
    {
        return "'" . mysql_real_escape_string($str, $this->link) . "'";
    }

    public function query($sql, $forceResReturn = false)
    {
        global $isPlainText;
        $res = mysql_query($sql, $this->link);
        if (!$res) {
            if ($forceResReturn === true) {
                return $res;
            }
            $error = mysql_error($this->link);
            throw new Exception($error);
            exit(1);
        }
        return $res;
    }

    public function fetchAssoc($res)
    {
        $row = mysql_fetch_assoc($res);
        if (false === $row) {
            mysql_free_result($res);
        }
        return $row;
    }

    public function fetchRow($res)
    {
        $row = mysql_fetch_row($res);
        if (false === $row) {
            mysql_free_result($res);
        }
        return $row;
    }

    public function getAffectedRows()
    {
        return mysql_affected_rows($this->link);
    }

    public function getName()
    {
        return 'MySQL database';
    }
}

class MySQLIDriver extends MySQLCommon
{
    public $link;

    public function connect($config)
    {
        // Connect to DB
        $port = isset($config['port']) ? $config['port'] : null;
        $this->link = @mysqli_connect($config['host'], $config['user'], $config['pass'], $config['db'], $port);
        if ($this->link === false) {
            return 'Could not connect to MySQLi database: ' . mysqli_connect_error();
        }

        $this->setModes();
        return true;
    }

    public function disconnect()
    {
        mysqli_close($this->link);
    }

    public function escapeStr($str)
    {
        return "'" . mysqli_real_escape_string($this->link, $str) . "'";
    }

    public function query($sql, $forceResReturn = false)
    {
        global $isPlainText;
        $res = mysqli_query($this->link, $sql);
        if (!$res) {
            if ($forceResReturn === true) {
                return $res;
            }
            $error = mysqli_error($this->link);
            throw new Exception($error);
            exit(1);
        }
        return $res;
    }

    public function fetchAssoc($res)
    {
        $row = mysqli_fetch_assoc($res);
        if (false === $row) {
            mysqli_free_result($res);
        }
        return $row;
    }

    public function fetchRow($res)
    {
        $row = mysqli_fetch_row($res);
        if (false === $row) {
            mysqli_free_result($res);
        }
        return $row;
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    public function getName()
    {
        return 'MySQLi database';
    }
}

/**
 * Holds Data related to DB Entry
 */
class DBEntry
{
    public $easy_id;
    public $contents;
    public $new_contents;
    public $location;
    public $backedup;
    public $updated;
    public $is_serialized = false;
    public $errors        = array();

    /**
     * Sets location upon instantiation
     *
     * @param $array $location Location of entry in DB
     */
    public function __construct($location)
    {
        $this->location = $location;
    }
}

/**
 * Holds Search/Replace Config
 */
class SRConfig
{
    public $action;
    public $search;
    public $replace;
    public $is_regex;
    public $noise;

    /**
     * Sets Config Params upon instantiation
     *
     * @param string $action   Action is 'search' or 'replace'
     * @param string $search   Value to search for, can be regex
     * @param string $replace  Value to replace if search matches
     * @param string $is_regex True if search is regex (converted to bool)
     * @param string $noise    Doesn't do anything currently
     */
    public function __construct($action, $search, $replace, $is_regex, $noise)
    {
        $this->action = $action;
        $this->search = $search;
        $this->replace = $replace;
        $this->is_regex = $is_regex;
        $this->noise = $noise;
    }
}
/**
 * Config for Backups Search Loop
 */
class BUPConfig
{

    public $target_group;
    /**
     * Sets the target group upon instantiation
     *
     * @param string $target_group If set will only get this group
     */
    public function __construct($target_group)
    {
        $this->target_group = $target_group;
    }
}

/**
 * Store File Information
 *
 * This class could possibly be split into several classes.
 */
class File
{
    /* General */
    public $path;
    public $name;
    public $dir;
    public $content;
    public $errors = array();
    /* Backups */
    public $restore_path;
    public $backup_time;
    public $version = '';
    /* View */
    public $is_backup;
    public $is_link;
    /* Search/Replace */
    public $scanned     = false;
    public $matched     = false;
    public $modified    = false;
    public $backup_path = false;
    /**
     * Sets the path and then name/dir based on path
     *
     * @param [string $path Full path to file
     */
    public function __construct($path)
    {
        $this->path = $path;
        $this->name = basename($path);
        $this->dir = dirname($path);
    }
}

/**
 * Backup Group
 *
 * Used by FileUtility
 *
 * Holds Files in $files
 */
class Group
{
    public $group_name;
    public $group_type;
    public $group_time;
    public $files = array();
    /**
     * Sets the group name upon instantiation
     *
     * @param string $group_name Name of the group
     */
    public function __construct($group_name)
    {
        $this->group_name = $group_name;
    }
}
