<?php
include_once("common.php");
$meta_arr = $generalobj->getsettingSeo(2);
$sql = "SELECT * from language_master where eStatus = 'Active'";
$db_lang = $obj->MySQLSelect($sql);
$sql = "SELECT * from country where eStatus = 'Active'";
$db_code = $obj->MySQLSelect($sql);
$sql = "SELECT * FROM country WHERE eStatus = 'Active' ORDER BY  vCountry ASC";
$db_country = $obj->MySQLSelect($sql);
//echo "<pre>";print_r($db_lang);
$script = "Contact Us";
$add = "";
$vName = base64_decode($_REQUEST['1']);
$vName = explode(" ", $vName);
$vName0 = $vName['0'];
if ($vName[1] == "")
    $_vLastName = "";
else
    $_vLastName = $vName[1];

$vEmail = base64_decode($_REQUEST['2']);
$vPhone = base64_decode($_REQUEST['3']);
if (isset($_POST['action']) && $_POST['action'] == 'send_mail') {
    unset($_POST['action']);
    $maildata = array();
    $maildata['EMAIL'] = $_POST['vEmail'];
    $maildata['NAME'] = $_POST['vName'] . " " . $_POST['vLastName'];
    $maildata['PASSWORD'] = '123456';
    //$generalobj->send_email_user("DRIVER_REGISTRATION_ADMIN",$maildata);
    $generalobj->send_email_user("DRIVER_REGISTRATION_USER", $maildata);
}
if (isset($_POST['action']) && $_POST['action'] == 'add_dummy') {
    unset($_POST['action']);
    $vCountry = $_POST['vCountry'];
    $countryCodeQuery = "SELECT vPhoneCode,vTimeZone FROM country WHERE vCountryCode = '" . $vCountry . "'";
    $db_country_code = $obj->MySQLSelect($countryCodeQuery);
    $currencyCode = $countryTimezon = "";
    if (count($db_country_code) > 0) {
        $countryCode = $db_country_code[0]['vPhoneCode'];
        $countryTimezon = $db_country_code[0]['vTimeZone'];
    }
    $email = $_POST['vEmail'];
    $msg = $generalobj->checkDuplicateFront('vEmail', 'register_driver', Array('vEmail'), $tconfig["tsite_url"] . "dummy_data_insert_taxi.php?error=1&var_msg=Email already Exists", "Email already Exists", "", "");
    #echo "<pre>";print_r($_POST); die;
    //Insert Driver
    $eReftype1 = "Driver";
    $Data1['vRefCode'] = $generalobj->ganaraterefercode($eReftype1);
    $Data1['iRefUserId'] = $Data1['eRefType'] = '';
    $Data1['vName'] = $_POST['vName'];
    $Data1['vLastName'] = (isset($_POST['vLastName']) && $_POST['vLastName'] != '') ? $_POST['vLastName'] : '';
    $Data1['vLang'] = 'EN';
    $Data1['vPassword'] = $generalobj->encrypt_bycrypt('123456');
    $Data1['vEmail'] = $_POST['vEmail'];
    $Data1['dBirthDate'] = '1992-02-02';
    $Data1['vPhone'] = (isset($_POST['vPhone']) && $_POST['vPhone'] != '') ? $_POST['vPhone'] : '9876543210';
    $Data1['vCaddress'] = "test address";
    $Data1['vCadress2'] = "test address";
    $Data1['vCity'] = "test city";
    $Data1['vZip'] = "121212";
    $Data1['vCountry'] = "US";
    $Data1['vCode'] = "1";
    $Data1['vFathersName'] = 'test';
    $Data1['vCompany'] = 'test';
    $Data1['tRegistrationDate'] = Date('Y-m-d H:i:s');
    $Data1['eStatus'] = 'Active';
    $Data1['vCurrencyDriver'] = 'USD';
    $Data1['iCompanyId'] = 1;
    $Data1['eEmailVerified'] = $Data1['ePhoneVerified'] = 'Yes';
    //echo "<pre>";print_r($Data1); echo "</pre>";
    $id = $obj->MySQLQueryPerform('register_driver', $Data1, 'insert');
	
	/*ADDED BY AB*/
	$demo_details['iDriverId'] = $id;
	$demo_details['vDriverName'] = $Data1['vName'] . " " . $Data1['vLastName'];
	$demo_details['vDriverEmail'] = $Data1['vEmail'];
	$demo_details['vDriverPassword'] = $Data1['vPassword'];
	/*ADDED BY AB*/
	
    //Add Driver Vehicle
    if ($id != "") {
        if ($APP_TYPE == 'UberX' || $APP_TYPE == 'Ride-Delivery-UberX') {
            $Drive_vehicle['iDriverId'] = $id;
            $Drive_vehicle['iCompanyId'] = "1";
            $Drive_vehicle['iMakeId'] = "3";
            $Drive_vehicle['iModelId'] = "1";
            $Drive_vehicle['iYear'] = Date('Y');
            $Drive_vehicle['eStatus'] = "Active";
            $Drive_vehicle['eCarX'] = $Drive_vehicle['eCarGo'] = "Yes";
            $Drive_vehicle['vLicencePlate'] = "My Services";
            $Drive_vehicle['eType'] = "UberX";
            $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE eType = 'UberX'";
            $result = $obj->MySQLSelect($query);
            $Drive_vehicle['vCarType'] = $result[0]['countId'];
            $Drive_vehicle['vRentalCarType'] = $result[0]['countId'];
            $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle, 'insert');
            if ($APP_TYPE == 'UberX') {
                $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_VehicleId . "' WHERE iDriverId='" . $id . "'";
                $obj->sql_query($sql);
            }
            $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            foreach ($days as $value) {
                $data_avilability['iDriverId'] = $id;
                $data_avilability['vDay'] = $value;
                $data_avilability['vAvailableTimes'] = '08-09,09-10,10-11,11-12,12-13,13-14,14-15,15-16,16-17,17-18,18-19,19-20,20-21,21-22';
                $data_avilability['dAddedDate'] = @date('Y-m-d H:i:s');
                $data_avilability['eStatus'] = 'Active';
                $data_avilability_add = $obj->MySQLQueryPerform('driver_manage_timing', $data_avilability, 'insert');
            }
            if ($APP_TYPE == 'Ride-Delivery-UberX') {
                $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE eType = 'Ride' OR eType = 'Deliver'";
                $result_ride = $obj->MySQLSelect($query);
                $Drive_vehicle_ride['iDriverId'] = $id;
                $Drive_vehicle_ride['iCompanyId'] = "1";
                $Drive_vehicle_ride['iYear'] = "2014";
                $Drive_vehicle_ride['vLicencePlate'] = "CK201";
                $Drive_vehicle_ride['eStatus'] = "Active";
                $Drive_vehicle_ride['eCarX'] = "Yes";
                $Drive_vehicle_ride['eCarGo'] = "Yes";
                $Drive_vehicle_ride['eType'] = "Ride";
                $Drive_vehicle_delivery = $Drive_vehicle_ride;
                $Drive_vehicle_ride['iMakeId'] = "3";
                $Drive_vehicle_ride['iModelId'] = "1";
                $Drive_vehicle_ride['vCarType'] = $result_ride[0]['countId'];
                $Drive_vehicle_ride['vRentalCarType'] = $result_ride[0]['countId'];
                $iDriver_Ride_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle_ride, 'insert');

                $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_Ride_VehicleId . "' WHERE iDriverId='" . $id . "'";
                $obj->sql_query($sql);

                $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE eType = 'Ride' OR eType = 'Deliver'";
                $result_delivery = $obj->MySQLSelect($query);
                $Drive_vehicle_delivery['iMakeId'] = "5";
                $Drive_vehicle_delivery['iModelId'] = "18";
                $Drive_vehicle_delivery['eType'] = "Delivery";
                $Drive_vehicle_delivery['vCarType'] = $result_delivery[0]['countId'];
                $Drive_vehicle_delivery['vRentalCarType'] = $result_delivery[0]['countId'];
                $iDriver_Delivery_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle_delivery, 'insert');
            }
            /*
              $query ="SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`";
              $result = $obj->MySQLSelect($query);
              $Drive_vehicle['iDriverId'] = $id;
              $Drive_vehicle['iCompanyId'] = "1";
              $Drive_vehicle['iMakeId'] = "3";
              $Drive_vehicle['iModelId'] = "1";
              $Drive_vehicle['iYear'] = Date('Y');
              $Drive_vehicle['vLicencePlate'] = "My Services";
              $Drive_vehicle['eStatus'] = "Active";
              $Drive_vehicle['eCarX'] = "Yes";
              $Drive_vehicle['eCarGo'] = "Yes";
              $Drive_vehicle['vCarType'] = $result[0]['countId'];
              $iDriver_VehicleId=$obj->MySQLQueryPerform('driver_vehicle',$Drive_vehicle,'insert');
              $sql = "UPDATE register_driver set iDriverVehicleId='".$iDriver_VehicleId."' WHERE iDriverId='".$id."'";
              $obj->sql_query($sql);
              if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
              $sql="select iVehicleTypeId,iVehicleCategoryId,eFareType,fFixedFare,fPricePerHour from vehicle_type where 1=1";
              $data_vehicles = $obj->MySQLSelect($sql);
              //echo "<pre>";print_r($data_vehicles);exit;
              if($data_vehicles[$i]['eFareType'] != "Regular")
              {
              for($i=0 ; $i < count($data_vehicles); $i++){
              $Data_service['iVehicleTypeId'] = $data_vehicles[$i]['iVehicleTypeId'];
              $Data_service['iDriverVehicleId'] = $iDriver_VehicleId;
              if($data_vehicles[$i]['eFareType'] == "Fixed"){
              $Data_service['fAmount'] = $data_vehicles[$i]['fFixedFare'];
              }
              else if($data_vehicles[$i]['eFareType'] == "Hourly"){
              $Data_service['fAmount'] = $data_vehicles[$i]['fPricePerHour'];
              }
              $data_service_amount = $obj->MySQLQueryPerform('service_pro_amount',$Data_service,'insert');
              }
              }
              }
              $days =  array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
              foreach ($days as $value) {
              $data_avilability['iDriverId'] = $id;
              $data_avilability['vDay'] = $value;
              $data_avilability['vAvailableTimes'] = '08-09,09-10,10-11,11-12,12-13,13-14,14-15,15-16,16-17,17-18,18-19,19-20,20-21,21-22';
              $data_avilability['dAddedDate'] = Date('Y-m-d H:i:s');
              $data_avilability['eStatus'] = 'Active';
              $data_avilability_add = $obj->MySQLQueryPerform('driver_manage_timing',$data_avilability,'insert');
              }
             */
        } else {
            $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`";
            $result = $obj->MySQLSelect($query);
            $Drive_vehicle['iDriverId'] = $id;
            $Drive_vehicle['iCompanyId'] = "1";
            $Drive_vehicle['iMakeId'] = "5";
            $Drive_vehicle['iModelId'] = "18";
            $Drive_vehicle['iYear'] = "2014";
            $Drive_vehicle['vLicencePlate'] = "CK201";
            $Drive_vehicle['eStatus'] = "Active";
            $Drive_vehicle['eCarX'] = $Drive_vehicle['eCarGo'] = "Yes";
            $Drive_vehicle['vCarType'] = $result[0]['countId'];
            $Drive_vehicle['vRentalCarType'] = $result[0]['countId'];
            $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle, 'insert');
            $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_VehicleId . "' WHERE iDriverId='" . $id . "'";
            $obj->sql_query($sql);
        }
        //Added By HJ On 21-01-2019 For Auto Create Hotel Admin Start
        $hotelData = createHotelAdmin($_POST, $countryCode);
        //Added By HJ On 21-01-2019 For Auto Create Hotel Admin End
    }
    //Insert Company
    $Data2['vName'] = $_POST['vName'];
    $Data2['vLastName'] = $_POST['vLastName'];
    $Data2['vLang'] = 'EN';
    $Data2['vPassword'] = $generalobj->encrypt_bycrypt('123456');
    $Data2['vEmail'] = "company-" . $_POST['vEmail'];
    $Data2['dBirthDate'] = '1992-02-02';
    $Data2['vPhone'] = (isset($_POST['vPhone']) && $_POST['vPhone'] != '') ? $_POST['vPhone'] : '9876543210';
    $Data2['vCaddress'] = "test address";
    $Data2['vCadress2'] = "test address";
    $Data2['vCity'] = "test city";
    $Data2['vZip'] = "121212";
    $Data2['vCountry'] = "US";
    $Data2['vCompany'] = $_POST['vName'] . " " . $_POST['vLastName'];
    $Data2['vCode'] = "1";
    $Data2['vFathersName'] = 'test';
    $Data2['tRegistrationDate'] = Date('Y-m-d H:i:s');
    $Data2['eStatus'] = 'Active';
    //echo "<pre>";print_r($Data2); echo "</pre>";
    //$id = $obj->MySQLQueryPerform('company',$Data2,'insert');
    //Insert rider
    $eReftype = "Rider";
    $Data['vRefCode'] = $generalobj->ganaraterefercode($eReftype);
    $Data['iRefUserId'] = $Data['eRefType'] = '';
    $Data['vName'] = $_POST['vName'];
    $Data['vLang'] = 'EN';
    $Data['vLastName'] = $_POST['vLastName'];
    //$Data['vLoginId'] = "";
    $Data['vPassword'] = $generalobj->encrypt_bycrypt('123456');
    $Data['vEmail'] = "user-" . $_POST['vEmail'];
    $Data['vPhone'] = (isset($_POST['vPhone']) && $_POST['vPhone'] != '') ? $_POST['vPhone'] : '9876543210';
    $Data['vCountry'] = "US";
    $Data['vPhoneCode'] = "1";
    //$Data['vExpMonth'] = $_POST['vExpMonth'];
    //$Data['vExpYear'] = $_POST['vExpYear'];
    $Data['vZip'] = '121212';
    //$Data['iDriverVehicleId	'] = "";
    $Data['vInviteCode'] = $Data['vCreditCard'] = "";
    $Data['vCvv'] = "";
    $Data['vCurrencyPassenger'] = "USD";
    $Data['dRefDate'] = Date('Y-m-d H:i:s');
    $Data['eStatus'] = 'Active';
    $Data['eEmailVerified'] = $Data['ePhoneVerified'] = 'Yes';
    $id = $obj->MySQLQueryPerform("register_user", $Data, 'insert');
	
	/*ADDED BY AB*/
	$demo_details['iRiderId'] = $id;
	$demo_details['vRiderName'] = $Data['vName'] . " " . $Data['vLastName'];
	$demo_details['vRiderEmail'] = $Data['vEmail'];
	$demo_details['vRiderPassword'] = $Data['vPassword'];
	/*ADDED BY AB*/
	
    $add = "Yes";
}

function createHotelAdmin($postData, $countryCode) {
    global $generalobj, $obj;
    //echo "<pre>";print_R($postData);die;
    $contact = "9876543210";
    if (isset($postData['vPhone']) && $postData['vPhone'] != "") {
        $contact = $postData['vPhone'];
    }
    $rendon = mt_rand(1000, 9999);  // set the order's reference number
    $email = "hotel-" . $rendon . "@gmail.com";
    if (isset($postData['vEmail']) && $postData['vEmail'] != "") {
        $email = "hotel-" . $postData['vEmail'];
    }
    $hotel_admin = array();
    $hotel_admin['iGroupId'] = 4;
    $hotel_admin['vFirstName'] = $postData['vCompany'];
    $hotel_admin['vLastName'] = $postData['vLastName'];
    $hotel_admin['vEmail'] = $email;
    $hotel_admin['vContactNo'] = $contact;
    $hotel_admin['vCode'] = $countryCode;
    $hotel_admin['vPassword'] = $generalobj->encrypt_bycrypt('123456');
    $hotel_admin['vCountry'] = $postData['vCountry'];
    $hotel_admin['vState'] = $hotel_admin['vCity'] = "";
    $hotel_admin['vAddress'] = $postData['vRestuarantLocation'];
    $hotel_admin['vAddressLat'] = $postData['vRestuarantLocationLat'];
    $hotel_admin['vAddressLong'] = $postData['vRestuarantLocationLong'];
    $hotel_admin['fHotelServiceCharge'] = 10;
    $hotel_admin['vPaymentEmail'] = $hotel_admin['vBankAccountHolderName'] = $hotel_admin['vAccountNumber'] = $hotel_admin['vBankName'] = $hotel_admin['vBankLocation'] = $hotel_admin['vBIC_SWIFT_Code'] = "";
    $hotel_admin['eStatus'] = "Active";
    $hotel_admin['eDefault'] = "Yes";
    //echo "<pre>";print_R($hotel_admin);die;
    $id = $obj->MySQLQueryPerform("administrators", $hotel_admin, 'insert');
	
	/*ADDED BY AB*/
	$demo_details['iHotelId'] = $id;
	$demo_details['vHotelName'] = $hotel_admin['vFirstName'] . " " . $hotel_admin['vLastName'];
	$demo_details['vHotelEmail'] = $hotel_admin['vEmail'];
	$demo_details['vHotelPassword'] = $hotel_admin['vPassword'];
	
	$demo_details['dDate'] = date("Y-m-d H:i:s");
	/*ADDED BY AB*/
	
    $hotel_admin['iAdminId'] = $id;3
    return $hotel_admin;
}

$hotelName = $hotelEmail = "Auto Create Failed";
$hotelPassword = "123456";
if (isset($hotelData['iAdminId']) && $hotelData['iAdminId'] > 0) {
    $hotelName = $hotelData['vFirstName'];
    $hotelEmail = $hotelData['vEmail'];
    $add = "Yes";
}

/*ADDED BY AB*/
$dd_id = $obj->MySQLQueryPerform("demo_details", $demo_details, 'insert');

//INSERT INTO `demo_details` (`iDemoDetailsId`, `iDriverId`, `vDriverName`, `vDriverEmail`, `vDriverPassword`, `iRiderId`, `vRiderName`, `vRiderEmail`, `vRiderPassword`, `iHotelId`, `vHotelName`, `vHotelEmail`, `vHotelPassword`) VALUES (NULL, '1', 'fas', 'asdf', 'asdf', '1', 'fasfd', 'asdf', 'asdf', '1', 'sdf', 'asdf', 'asdf');
/*ADDED BY AB*/

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
                <!--<title><?= $COMPANY_NAME ?> | Contact Us</title>-->
        <title>Dummy</title>
        <!-- Default Top Script and css -->
        <?php include_once("top/top_script.php"); ?>
        <?php include_once("top/validation.php"); ?>
        <!-- End: Default Top Script and css-->
        <script src="https://maps.google.com/maps/api/js?sensor=true&key=<?= $GOOGLE_SEVER_API_KEY_WEB ?>&libraries=places" type="text/javascript"></script>
        <script type="text/javascript" src="assets/map/gmaps.js"></script>
    </head>
    <body>
        <!-- home page -->
        <div id="main-uber-page">
            <div class="page-contant">
                <div class="page-contant-inner">
                    <div class="footer-text-center">			
                        <? if ($add == "Yes") { ?>
                            <!-- <h3 style="padding-top:15px;"> Company Details </h3>
                                    <h5>
                                    <p>Name: <?php echo $_POST['vName'] . " " . $_POST['vLastName']; ?></p>
                                    <p>Email: company_<?php echo $_POST['vEmail']; ?></p>
                                    <p>Password: 123456 </p>
                            </h5> -->
                            <h3 style="padding-top:15px;"> Driver Details </h3>
                            <h5>
                                <p>Name: <?php echo $_POST['vName'] . " " . $_POST['vLastName']; ?></p>
                                <p>Email: <?php echo $_POST['vEmail']; ?></p>
                                <p>Password: 123456 </p>
                            </h5>
                            <h3 style="padding-top:15px;"> Rider Details </h3>
                            <h5>
                                <p>Name: <?php echo $_POST['vName'] . " " . $_POST['vLastName']; ?></p>
                                <p>Email: user-<?php echo $_POST['vEmail']; ?></p>
                                <p>Password: 123456 </p>
                            </h5>
                            <h3 style="padding-top:15px;"> Hotel Admin Details </h3>
                            <h5>
                                <p>Hotel Name: <?php echo $hotelName; ?></p>
                                <p>Email: <?php echo $hotelEmail; ?></p>
                                <p>Password: <?php echo $hotelPassword; ?> </p>
                            </h5>
							<h3 style="padding-top:15px;"> Demo Link </h3>
							<a href="http://192.168.1.227/v3cube/taxi-app-summary.php">Demo URL</a>
							
                            <form method="post" action="">
                                <input type="hidden" name="vName" id="vName" value="<?= $_POST['vName']; ?>">
                                <input type="hidden" name="vLastName" id="vLastName" value="<?= $_POST['vLastName']; ?>">
                                <input type="hidden" name="vEmail" id="vEmail" value="<?= $_POST['vEmail']; ?>">
                                <input type="hidden" name="vPhone" id="vPhone" value="<?= $_POST['vPhone']; ?>">
                                <input type="hidden" name="action" id="action" value="send_mail">
                                <div class="contact-form">
                                    <b>
                                        <input type="submit" class="submit-but" value="Send Email to Driver" name="send_email" />
                                    </b>
                                </div>
                            </form>
                        <? } ?>
                    </div>
                    <h2 class="header-page">Add Dummy Data
                        <p>It will automatically create dummy record for company , driver, driver vehicle , rider .</p>
                    </h2>
                    <!-- contact page -->
                    <div style="clear:both;"></div>
                    <?php
                    if ($_REQUEST['error']) {
                        ?>
                        <div class="row" id="showError">
                            <div class="col-sm-12 alert alert-danger">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button" onclick="hideError();" >×</button>
                                <?= $_REQUEST['var_msg']; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div style="clear:both;"></div>
                    <form name="frmsignup" id="frmsignup" method="post" action="">
                        <input type="hidden" name="action" value="add_dummy" >
                        <div class="contact-form">
                            <b>
                                <strong>
                                    <em>First Name:</em><br/>
                                    <input type="text" name="vName" placeholder="<?= $langage_lbl['LBL_CONTECT_US_FIRST_NAME_HEADER_TXT']; ?>" class="contact-input required" value="<?= $vName0 ?>" />
                                </strong>
                                <strong>
                                    <em>Last Name:</em><br/>
                                    <input type="text" name="vLastName" placeholder="<?= $langage_lbl['LBL_CONTECT_US_LAST_NAME_HEADER_TXT']; ?>" class="contact-input" value="<?= $_vLastName ?>" />
                                </strong>
                                <strong>
                                    <em>Email address:</em><br/>
                                    <input type="text" placeholder="<?= $langage_lbl['LBL_CONTECT_US_EMAIL_LBL_TXT']; ?>" name="vEmail" value="<?= $vEmail ?>" autocomplete="off" class="contact-input required"/>
                                </strong>
                                <strong>
                                    <em>Phone Number:</em><br/>
                                    <input type="text" placeholder="777-777-7777" value="<?= $vPhone ?>" name="vPhone" class="contact-input" />
                                </strong>
                                <strong>
                                    <em>Restaurant Name: *</em><br/>
                                    <input type="text" name="vCompany" class="contact-input required" id="vCompany" placeholder="Restaurant Name" value="" />
                                </strong>
                                <strong>
                                    <em>Restaurant Location: *</em><br/>
                                    <input type="hidden" name="vRestuarantLocationLat" id="vRestuarantLocationLat" value="">
                                    <input type="hidden" name="vRestuarantLocationLong" id="vRestuarantLocationLong" value="">
                                    <input type="text" name="vRestuarantLocation" class="contact-input required" id="vRestuarantLocation" placeholder="Restaurant Location" value="" />
                                </strong>
                                <strong>
                                    <em>Country: *</em><br/>
                                    <select class="contact-input required" required name='vCountry' id="vCountry" >
                                        <option value="">Select Country</option>
                                        <? for ($i = 0; $i < count($db_country); $i++) { ?>
                                            <option value = "<?= $db_country[$i]['vCountryCode'] ?>" <? if ($DEFAULT_COUNTRY_CODE_WEB == $db_country[$i]['vCountryCode']) { ?>selected<? } ?>><?= $db_country[$i]['vCountry'] ?></option>
                                        <? } ?>
                                    </select>     
                                </strong>
                            </b>
                            <b>
                                <input type="submit" onClick="return submit_form();"  class="submit-but floatLeft" value="ADD" name="SUBMIT" />
                            </b> 
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <script>
                function submit_form()
                {
                    if (validatrix()) {
                        //alert("Submit Form");
                        document.frmsignup.submit();
                    } else {
                        console.log("Some fields are required");
                        return false;
                    }
                    return false; //Prevent form submition
                }
            </script>
            <script type="text/javascript">
                function hideError() {
                    $('#showError').fadeOut();
                }
            </script>
            <script>
                var from = document.getElementById('vRestuarantLocation');
                autocomplete_from1 = new google.maps.places.Autocomplete(from);
                google.maps.event.addListener(autocomplete_from1, 'place_changed', function () {
                    var placeaddress = autocomplete_from1.getPlace();
                    $('#vRestuarantLocationLat').val(placeaddress.geometry.location.lat());
                    $('#vRestuarantLocationLong').val(placeaddress.geometry.location.lng());

                });
                /*var map;
                 function initialize() {
                 map = new google.maps.Map(document.getElementById('map'), {
                 center: {lat: -33.8688, lng: 151.2195},
                 zoom: 13
                 });
                 var input = document.getElementById('vRestuarantLocation');
                 // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                 
                 var autocomplete = new google.maps.places.Autocomplete(input);
                 autocomplete.bindTo('bounds', map);
                 
                 var marker = new google.maps.Marker({
                 map: map,
                 anchorPoint: new google.maps.Point(0, -29)
                 });
                 
                 autocomplete.addListener('place_changed', function() {
                 
                 marker.setVisible(false);
                 var place = autocomplete.getPlace();
                 if (!place.geometry) {
                 window.alert("Autocomplete's returned place contains no geometry");
                 return;
                 }
                 
                 // If the place has a geometry, then present it on a map.
                 if (place.geometry.viewport) {
                 map.fitBounds(place.geometry.viewport);
                 } else {
                 map.setCenter(place.geometry.location);
                 map.setZoom(17);
                 }
                 
                 marker.setPosition(place.geometry.location);
                 marker.setVisible(true);
                 
                 var address = '';
                 if (place.address_components) {
                 address = [
                 (place.address_components[0] && place.address_components[0].short_name || ''),
                 (place.address_components[1] && place.address_components[1].short_name || ''),
                 (place.address_components[2] && place.address_components[2].short_name || '')
                 ].join(' ');
                 }
                 $("#vRestuarantLocation").val(place.formatted_address);
                 $("#vRestuarantLocationLat").val(place.geometry.location.lat());
                 $("#vRestuarantLocationLong").val(place.geometry.location.lng());
                 });
                 
                 if($("#vRestuarantLocation").val() != ""){
                 var myLatLng = new google.maps.LatLng($("#vRestuarantLocationLat").val(), $("#vRestuarantLocationLong").val());
                 marker.setPosition(myLatLng);
                 map.setCenter(myLatLng);
                 map.setZoom(17);
                 marker.setVisible(true);
                 }
                 }
                 google.maps.event.addDomListener(window, 'load', initialize);*/
            </script>
            <!-- End: Footer Script -->
    </body>
</html>



