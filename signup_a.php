<?php

include_once("common.php");
require_once(TPATH_CLASS . "/Imagecrop.class.php");
$thumb = new thumbnail();
include_once ('app_common_functions.php');
$POST_CAPTCHA = $_POST['POST_CAPTCHA'];
$SESS_CAPTCHA = $_SESSION['SESS_CAPTCHA'];
// echo "<pre>";print_r($_POST);exit;
if ($POST_CAPTCHA == $SESS_CAPTCHA) {
    if ($_POST) {

        $user_type = $_POST['user_type'];

        if ($user_type == 'driver') {
            $table_name = "register_driver";
            $msg = $generalobj->checkDuplicateFront('vEmail', 'register_driver', Array('vEmail'), $tconfig["tsite_url"] . "sign-up.php?error=1&var_msg=Email already Exists", "Email already Exists", "", "");
        }

        /* Use For Organization Module */ else if ($user_type == 'organization') {
            $table_name = "company";
            $msg = $generalobj->checkDuplicateFront('vEmail', 'company', Array('vEmail'), $tconfig["tsite_url"] . "sign-up-organization.php?error=1&var_msg=Email already Exists", "Email already Exists", "", "");
        }

        /* Use For Organization Module */ else {
            $table_name = "company";
            $msg = $generalobj->checkDuplicateFront('vEmail', 'company', Array('vEmail'), $tconfig["tsite_url"] . "sign-up.php?error=1&var_msg=Email already Exists", "Email already Exists", "", "");
        }


        if ($user_type == 'driver') {
            $eReftype = "Driver";
            $Data['vRefCode'] = $generalobj->ganaraterefercode($eReftype);
            $Data['iRefUserId'] = $_POST['iRefUserId'];
            $Data['eRefType'] = $_POST['eRefType'];
            $Data['eRefType'] = $eReftype;
            $Data['dRefDate'] = Date('Y-m-d H:i:s');
        }
        // $Data['photo'] = $_FILES['photo'];

        $Data['vName'] = $_POST['vFirstName'];
        $Data['vLastName'] = $_POST['vLastName'];
        $Data['vLang'] = $_SESSION['sess_lang'];
        $Data['vPassword'] = $generalobj->encrypt_bycrypt($_REQUEST['vPassword']);
        $Data['vEmail'] = $_POST['vEmail'];
        //$Data['dBirthDate'] = $_POST['vYear'].'-'.$_POST['vMonth'].'-'.$_POST['vDay'];
        $Data['vPhone'] = $_POST['vPhone'];
        $Data['vCaddress'] = $_POST['vCaddress'];
        $Data['vCadress2'] = $_POST['vCadress2'];
        $Data['vCity'] = $_POST['vCity'];
        $Data['vCountry'] = $_POST['vCountry'];
        // $Data['vState'] = $_POST['vState'];
        $Data['vZip'] = $_POST['vZip'];
        $Data['vCode'] = $_POST['vCode'];
        $Data['vBackCheck'] = $_POST['vBackCheck'];
        $Data['vInviteCode'] = $_POST['vInviteCode'];
        $Data['vFathersName'] = $_POST['vFather'];
        $Data['vCompany'] = $_POST['vCompany'];
        $Data['vTSLLicense'] = $_POST['vTSLLicense'];
        $Data['vAccountNumber'] = $_POST['vAccountNumber'];
        $Data['isHaveGST'] = $_POST['isHaveGST'];
        $Data['eGSTNumber'] = $_POST['eGSTNumber'];
        
        $Data['tRegistrationDate'] = Date('Y-m-d H:i:s');

        $csql = "SELECT eZeroAllowed,vCountryCode FROM `country` WHERE vPhoneCode = '" . $_POST['vCode'] . "'";
        $CountryData = $obj->MySQLSelect($csql);
        $eZeroAllowed = $CountryData[0]['eZeroAllowed'];

        if ($eZeroAllowed == 'Yes') {
            $Data['vPhone'] = $Data['vPhone'];
        } else {
            $first = substr($Data['vPhone'], 0, 1);

            if ($first == "0") {
                $Data['vPhone'] = substr($Data['vPhone'], 1);
            }
        }

        if (SITE_TYPE == 'Demo') {
            $Data['eStatus'] = 'Active';
        }

        if ($user_type == 'driver') {
            if(checkDriverDestinationModule()){
                $Data['eDestinationMode'] = 'No';
                $Data['iDestinationCount'] = 0;
            }else{
                $Data['eDestinationMode'] = 'No';
                $Data['iDestinationCount'] = 0;
            } 
            $table = 'register_driver';
            $Data['vCurrencyDriver'] = $_POST['vCurrencyDriver'];
            $Data['eGender'] = $_POST['eGender'];
            $user_type = 'driver';
            $Data['iCompanyId'] = 1;
        } else {


            $table = 'company';
            $Data['eSystem'] = ($user_type == 'organization') ? 'Organization' : 'General';  /* Use For Company & Organization */
            //$user_type='company';
            $Data['vVat'] = $_POST['vVat'];
        }


        $id = $obj->MySQLQueryPerform($table, $Data, 'insert');

        
            if (isset($_POST['img_path'])) {
                $img_path = $_POST['img_path'];
            }
            $temp_gallery = $img_path . '/';
            $image_object = $_FILES['photo']['tmp_name'];
            $image_name = $_FILES['photo']['name'];

            if( empty($image_name)) {
                $image_name = $_POST['driver_doc_hidden']; 
            }

            if ($image_name == "" || $image_name == "NONE") {
                $var_msg = $langage_lbl['LBL_DOC_UPLOAD_ERROR_'];
                // header("location:profile.php?success=0&id=" . $_REQUEST['id'] . "&var_msg=" . $var_msg);
                exit;
            }

            if ($image_name != "" || $image_name != "NONE") 
            {

                /*if ($_SESSION['sess_user'] == 'driver') {
                    $check_file_query = "select iDriverId,vImage from register_driver where iDriverId=" . $id;
                } 
                else if ($_SESSION['sess_user'] == 'company') {
                    $check_file_query = "select iCompanyId,vImage from company where iCompanyId=" . $id;
                }*/
                $check_file_query = "select iDriverId,vImage from register_driver where iDriverId=" . $id;
                $check_file = $obj->sql_query($check_file_query);
                $check_file['vImage'] = $img_path . '/' . $id . '/' . $check_file[0]['vImage'];

                if ($check_file['vImage'] != '' && file_exists($check_file['vImage'])) {
                    unlink($img_path . '/' . $id . '/' . $check_file[0]['vImage']);
                    unlink($img_path . '/' . $id . '/1_' . $check_file[0]['vImage']);
                    unlink($img_path . '/' . $id . '/2_' . $check_file[0]['vImage']);
                    unlink($img_path . '/' . $id . '/3_' . $check_file[0]['vImage']);
                }
                $filecheck = basename($_FILES['photo']['name']);
                $fileextarr = explode(".", $filecheck);
                $ext = strtolower($fileextarr[count($fileextarr) - 1]);
                $flag_error = 0;
                if ($ext != "jpg" && $ext != "gif" && $ext != "png" && $ext != "jpeg" && $ext != "bmp") {
                    // if ($ext != "jpg" && $ext != "gif" && $ext != "png" && $ext != "jpeg" && $ext != "bmp"  && $ext != "doc"  && $ext != "docx" && $ext != "pdf") {
                    $flag_error = 1;
                    $var_msg = $langage_lbl['LBL_UPLOAD_IMG_ERROR'];
                }
            
                if ($flag_error == 1) {
                    // $generalobj->getPostForm($_POST, $var_msg, "profile.php?success=0&var_msg=" . $var_msg);
                    exit;
                } 
                else 
                {
                    // if ($user_type == 'driver') {
                        $Photo_Gallery_folder = $img_path . '/' . $id . '/';
                    // }
                    if (!is_dir($Photo_Gallery_folder)) {
                        mkdir($Photo_Gallery_folder, 0777);
                    }
                
                    $img1 = $generalobj->general_upload_image($image_object, $image_name, $Photo_Gallery_folder, '', '', '', '', '', '', 'Y', '', $Photo_Gallery_folder);
                    if ($img1 != '') {
                        if (is_file($Photo_Gallery_folder . $img1)) {
                            include_once(TPATH_CLASS . "/SimpleImage.class.php");
                            $img = new SimpleImage();
                            list($width, $height, $type, $attr) = getimagesize($Photo_Gallery_folder . $img1);
                            if ($width < $height) {
                                $final_width = $width;
                            } else {
                                $final_width = $height;
                            }
                            $img->load($Photo_Gallery_folder . $img1)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder . $img1);

                            $img1 = $generalobj->img_data_upload($Photo_Gallery_folder, $img1, $Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"], "");
                        }
                    }
                    $vImage = $img1;
                    $var_msg = "Profile image uploaded successfully";
                    /*if ($_SESSION['sess_user'] == 'driver') {
                        $tbl = 'register_driver';
                        $where = " WHERE `iDriverId` = '" . $id . "'";
                    }
                    if ($_SESSION['sess_user'] == 'company') {
                        $tbl = 'company';
                        $where = " WHERE `iCompanyId` = '" . $id . "'";
                    }*/
                    $tbl = 'register_driver';
                    $where = " WHERE `iDriverId` = '" . $id . "'";

                    $q = "UPDATE ";
                    $query = $q . " `" . $tbl . "` SET  
                    `vImage` = '" . $vImage . "'
                    " . $where;
                    $obj->sql_query($query);
                }
            }



        if ($SITE_VERSION == "v5" && $user_type == 'driver') {
            $set_driver_pref = $generalobj->Insert_Default_Preferences($id);
        }

        // user_wallet table insert data
//		$eFor = "Referrer";
//		$tDescription = "Referal amount credit ".$REFERRAL_AMOUNT." into your account";
//		$dDate = Date('Y-m-d H:i:s');
//		$ePaymentStatus = "Unsettelled";
//		$REFERRAL_AMOUNT; 
//
//		if($user_type=='driver'){
//
//			if($_POST['vRefCode'] != "" && !empty($_POST['vRefCode'])){
//				$generalobj->InsertIntoUserWallet($_POST['iRefUserId'],$_POST['eRefType'],$REFERRAL_AMOUNT,'Credit',0,$eFor,$tDescription,$ePaymentStatus,$dDate);
//			}	
//		}	

        if (($APP_TYPE == 'UberX' || $APP_TYPE == 'Ride-Delivery-UberX') && ONLYDELIVERALL == "No") {
            if ($user_type == 'driver') {
                $query = "SELECT GROUP_CONCAT(iVehicleTypeId) as countId FROM `vehicle_type` WHERE `eType` = 'UberX'";
                $result = $obj->MySQLSelect($query);

                $Drive_vehicle['iDriverId'] = $id;
                $Drive_vehicle['iCompanyId'] = "1";
                $Drive_vehicle['iMakeId'] = "3";
                $Drive_vehicle['iModelId'] = "1";
                $Drive_vehicle['iYear'] = Date('Y');
                $Drive_vehicle['vLicencePlate'] = "My Services";
                $Drive_vehicle['eStatus'] = "Active";
                $Drive_vehicle['eType'] = "UberX";
                $Drive_vehicle['eCarX'] = "Yes";
                $Drive_vehicle['eCarGo'] = "Yes";
                if (SITE_TYPE == 'Demo') {
                    $Drive_vehicle['vCarType'] = $result[0]['countId'];
                } else {
                    $Drive_vehicle['vCarType'] = "";
                }
                //$Drive_vehicle['vCarType'] = $result[0]['countId'];
                $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle, 'insert');

                if ($APP_TYPE == 'UberX') {
                    $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_VehicleId . "' WHERE iDriverId='" . $id . "'";
                    $obj->sql_query($sql);
                }

                /* if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
                  $sql="select iVehicleTypeId,iVehicleCategoryId,eFareType,fFixedFare,fPricePerHour from vehicle_type where 1=1";
                  $data_vehicles = $obj->MySQLSelect($sql);
                  //echo "<pre>";print_r($data_vehicles);exit;

                  if($data_vehicles[$i]['eFareType'] != "Regular")
                  {
                  for($i=0 ; $i < count($data_vehicles); $i++){
                  $Data_service['iVehicleTypeId'] = $data_vehicles[$i]['iVehicleTypeId'];
                  $Data_service['iDriverVehicleId'] = $iDriver_VehicleId;

                  if($data_vehicles[$i]['eFareType'] == "Fixed"){
                  $Data_service['fAmount'] = $data_vehicles[$i]['fFixedFare'];
                  }
                  else if($data_vehicles[$i]['eFareType'] == "Hourly"){
                  $Data_service['fAmount'] = $data_vehicles[$i]['fPricePerHour'];
                  }
                  $data_service_amount = $obj->MySQLQueryPerform('service_pro_amount',$Data_service,'insert');
                  }
                  }
                  } */
                if ($APP_TYPE == 'Ride-Delivery-UberX') {
                    if (SITE_TYPE == 'Demo') {
                        $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE (`eType` = 'Ride' OR `eType` = 'Deliver')";
                        $result = $obj->MySQLSelect($query);

                        $query1 = "SELECT GROUP_CONCAT(iVehicleTypeId)as countRentalId FROM `vehicle_type` WHERE `eType` = 'Ride'";
                        $resultrental = $obj->MySQLSelect($query1);

                        $Drive_vehicle_Ride['iDriverId'] = $id;
                        $Drive_vehicle_Ride['iCompanyId'] = "1";
                        $Drive_vehicle_Ride['iMakeId'] = "5";
                        $Drive_vehicle_Ride['iModelId'] = "18";
                        $Drive_vehicle_Ride['iYear'] = "2014";
                        $Drive_vehicle_Ride['vLicencePlate'] = "CK201";
                        $Drive_vehicle_Ride['eStatus'] = "Active";
                        $Drive_vehicle_Ride['eCarX'] = "Yes";
                        $Drive_vehicle_Ride['eCarGo'] = "Yes";
                        $Drive_vehicle_Ride['eType'] = "Ride";
                        $Drive_vehicle_Ride['vCarType'] = $result[0]['countId'];
                        if (ENABLE_RENTAL_OPTION == 'Yes') {
                            $Drive_vehicle_Ride['vRentalCarType'] = $resultrental[0]['countRentalId'];
                        }
                        $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle_Ride, 'insert');

                        $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_VehicleId . "' WHERE iDriverId='" . $id . "'";
                        $obj->sql_query($sql);
                        $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE (`eType` = 'Ride' OR `eType` = 'Deliver')";
                        $result = $obj->MySQLSelect($query);
                        $Drive_vehicle_Deliver['iDriverId'] = $id;
                        $Drive_vehicle_Deliver['iCompanyId'] = "1";
                        $Drive_vehicle_Deliver['iMakeId'] = "5";
                        $Drive_vehicle_Deliver['iModelId'] = "18";
                        $Drive_vehicle_Deliver['iYear'] = "2014";
                        $Drive_vehicle_Deliver['vLicencePlate'] = "CK201";
                        $Drive_vehicle_Deliver['eStatus'] = "Active";
                        $Drive_vehicle_Deliver['eCarX'] = "Yes";
                        $Drive_vehicle_Deliver['eCarGo'] = "Yes";
                        $Drive_vehicle_Deliver['eType'] = "Delivery";
                        $Drive_vehicle_Deliver['vCarType'] = $result[0]['countId'];
                        $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle_Deliver, 'insert');
                    }
                }
            }
        } else {
            if (SITE_TYPE == 'Demo') {
                if ($APP_TYPE == 'Delivery') {
                    $app_type = 'Deliver';
                } else {
                    $app_type = $APP_TYPE;
                }
                if ($app_type == 'Ride-Delivery') {
                    $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type` WHERE (`eType` = 'Ride' OR `eType` = 'Deliver')";

                    $query1 = "SELECT GROUP_CONCAT(iVehicleTypeId)as RentalcountId FROM `vehicle_type` WHERE (`eType` = 'Ride')";
                    $resultReantal = $obj->MySQLSelect($query1);
                } else {
                    $query = "SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`  WHERE `eType` = '" . $app_type . "'";
                }
                $result = $obj->MySQLSelect($query);
                /* 	$query ="SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`  WHERE `eType` = '".$app_type ."'";
                  $result = $obj->MySQLSelect($query); */
                $Drive_vehicle['iDriverId'] = $id;
                $Drive_vehicle['iCompanyId'] = "1";
                $Drive_vehicle['iMakeId'] = "5";
                $Drive_vehicle['iModelId'] = "18";
                $Drive_vehicle['iYear'] = "2014";
                $Drive_vehicle['vLicencePlate'] = "CK201";
                $Drive_vehicle['eStatus'] = "Active";
                $Drive_vehicle['eCarX'] = "Yes";
                $Drive_vehicle['eCarGo'] = "Yes";
                $Drive_vehicle['eType'] = $app_type;
                $Drive_vehicle['vCarType'] = $result[0]['countId'];

                if (($app_type == 'Ride' || $app_type == 'Ride-Delivery') && ENABLE_RENTAL_OPTION == 'Yes') {
                    $Drive_vehicle['vRentalCarType'] = $resultReantal[0]['RentalcountId'];
                }

                $iDriver_VehicleId = $obj->MySQLQueryPerform('driver_vehicle', $Drive_vehicle, 'insert');
                $sql = "UPDATE register_driver set iDriverVehicleId='" . $iDriver_VehicleId . "' WHERE iDriverId='" . $id . "'";
                $obj->sql_query($sql);
            }
        }

        if ($id != "") {
            $_SESSION['sess_iUserId'] = $id;
            if ($user_type == 'driver') {
                $_SESSION['sess_iCompanyId'] = 1;
                $_SESSION["sess_vName"] = $Data['vName'] . ' ' . $Data['vLastName'];
                $_SESSION["sess_vCurrency"] = $Data['vCurrencyDriver'];
            } else {
                $_SESSION['sess_iCompanyId'] = $id;
                $_SESSION["sess_vName"] = $Data['vCompany'];
                $_SESSION["eSystem"] = $Data['eSystem'];
            }

            $_SESSION["sess_company"] = $Data['vCompany'];
            $_SESSION["sess_vEmail"] = $Data['vEmail'];
            $_SESSION["sess_user"] = $user_type;
            $_SESSION["sess_new"] = 1;

            $maildata['EMAIL'] = $_SESSION["sess_vEmail"];
            $maildata['NAME'] = $_SESSION["sess_vName"];
            //$maildata['PASSWORD'] = $langage_lbl['LBL_PASSWORD'].": ". $_REQUEST['vPassword']; //Commented By HJ On 11-01-2019 For Hide Password As Per Discuss With QA BM
            $maildata['SOCIALNOTES'] = '';
            //$generalobj->send_email_user("MEMBER_REGISTRATION_USER",$maildata);
            if ($user_type == 'driver') {
                $generalobj->send_email_user("DRIVER_REGISTRATION_ADMIN", $maildata);
                $generalobj->send_email_user("DRIVER_REGISTRATION_USER", $maildata);
            } else {
                $generalobj->send_email_user("COMPANY_REGISTRATION_ADMIN", $maildata);
                $generalobj->send_email_user("COMPANY_REGISTRATION_USER", $maildata);
            }
            #header("Location:profile.php?first=yes");

            if ($APP_TYPE == 'UberX' && $user_type == 'driver') {
                header("Location:add_services.php?iDriverId=" . base64_encode(base64_encode($_SESSION['sess_iUserId'])));
                exit;
            } else {
                header("Location:profile.php?first=yes");
                exit;
            }
        }
    }
} else {
    $_SESSION['postDetail'] = $_REQUEST;
    header("Location:" . $tconfig["tsite_url"] . "sign-up.php?error=1&var_msg=Captcha did not match.");
    exit;
}
?>
