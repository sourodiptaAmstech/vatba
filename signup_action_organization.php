<?php
include_once("common.php");
$POST_CAPTCHA = $_POST['POST_CAPTCHA'];
$SESS_CAPTCHA = $_SESSION['SESS_CAPTCHA'];
if($POST_CAPTCHA == $SESS_CAPTCHA)
{
	if($_POST)
	{	

		$table_name="organization";
		$msg= $generalobj->checkDuplicateFront('vEmail','organization',Array('vEmail'),$tconfig["tsite_url"]."sign-up-organization.php?error=1&var_msg=Email already Exists", "Email already Exists","" ,"");

		$Data['vLang'] = $_SESSION['sess_lang'];
		$Data['vPassword'] = $generalobj->encrypt_bycrypt($_REQUEST['vPassword']);
		$Data['vEmail'] = $_POST['vEmail'];
		$Data['vPhone'] = $_POST['vPhone'];
		$Data['vCaddress'] = $_POST['vCaddress'];
		$Data['vCity'] = $_POST['vCity'];
		$Data['vCountry'] = $_POST['vCountry'];
		$Data['vState'] = $_POST['vState'];
		$Data['vZip'] = $_POST['vZip'];
		$Data['vCode'] = $_POST['vCode'];
		$Data['vBackCheck'] = $_POST['vBackCheck'];
		$Data['vInviteCode'] = $_POST['vInviteCode'];
		$Data['vCompany'] = $_POST['vCompany'];
		$Data['vCurrency'] = $_POST['vCurrency'];
		$Data['iUserProfileMasterId'] = $_POST['iUserProfileMasterId'];
		$Data['tRegistrationDate']=Date('Y-m-d H:i:s');
		$Data['eStatus']='Inactive';
		$Data['ePaymentBy']='Passenger';

		$csql = "SELECT eZeroAllowed,vCountryCode FROM `country` WHERE vPhoneCode = '".$Data['vCode']."'";
	    $CountryData = $obj->MySQLSelect($csql);
	    $eZeroAllowed = $CountryData[0]['eZeroAllowed'];

	    if($eZeroAllowed == 'Yes'){
	        $Data['vPhone'] = $Data['vPhone'];
	    } else {
	        $first = substr($Data['vPhone'], 0, 1);

	        if ($first == "0") {
	            $Data['vPhone'] = substr($Data['vPhone'], 1);
	        }
	    }


		$id = $obj->MySQLQueryPerform('organization',$Data,'insert');

		if($id != "")
		{
			$_SESSION['sess_iUserId'] = $id;

			$_SESSION['sess_iOrganizationId'] = $id;
			$_SESSION["sess_vName"] = $Data['vCompany'];

			$_SESSION["sess_company"] = $Data['vCompany'];
			$_SESSION["sess_vEmail"] = $Data['vEmail'];
			$_SESSION["sess_user"] ='organization';
			$_SESSION["sess_new"]=1;

			$maildata['EMAIL'] = $_SESSION["sess_vEmail"];
			$maildata['NAME'] = $_SESSION["sess_vName"];
			$maildata['PASSWORD'] = $langage_lbl['LBL_PASSWORD'].": ". $_REQUEST['vPassword'];
			$maildata['SOCIALNOTES'] ='';
		
			$generalobj->send_email_user("ORGANIZATION_REGISTRATION_ADMIN",$maildata);
			$generalobj->send_email_user("ORGANIZATION_REGISTRATION_USER",$maildata);

			header("Location:organization-profile.php");

		}
	}
}
else
{
	$_SESSION['postDetail'] = $_REQUEST;
	header("Location:".$tconfig["tsite_url"]."sign-up-organization.php?error=1&var_msg=Captcha did not match.");
	exit;
}
?>
