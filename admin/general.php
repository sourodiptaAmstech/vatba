<?
header('X-XSS-Protection:0');
include_once('../common.php');
$$msgType = "";
if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}
define("CONFIGURATIONS_PAYMENT", "configurations_payment");
define("CONFIGURATIONS", "configurations");
////$generalobjAdmin->check_member_login();
if (!$userObj->hasPermission('manage-general-settings')) {
    $userObj->redirect();
}
include_once('common.php');
//ini_set("display_errors", 1);
$script = $activeTab = 'General';
$msgType = isset($_REQUEST['msgType']) ? $_REQUEST['msgType'] : '';
$msg = isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';
if (isset($_POST['submitbutton']) && $_POST['submitbutton'] != "") {
    //echo "<pre>";
    //POOL_ENABLE
    if (SITE_TYPE == 'Demo') {
        $msgType = 0;
        $msg = $langage_lbl_admin['LBL_EDIT_DELETE_RECORD'];
        header("Location:general.php?msgType=" . $msgType . "&msg=" . $msg);
        exit;
    }
    $activeTab = str_replace(" ", "_", $_REQUEST['frm_type']);
    $configTable = CONFIGURATIONS;
    if ($activeTab == "Payment") {
        $configTable = CONFIGURATIONS_PAYMENT;
    }
    //print_r($_REQUEST);die;
    foreach ($_REQUEST['Data'] as $key => $value) {
        unset($updateData);
        //Added By HJ On 11-01-2019 For Solved Bug - 6178 As Per Discuss With CD Sir Start
        if ($key == "POOL_ENABLE" && $value == "No") {
            //echo "UPDATE vehicle_type SET eStatus='Inactive' WHERE ePoolStatus='Yes'";die;
            $obj->sql_query("UPDATE vehicle_type SET eStatus='Inactive' WHERE ePoolStatus='Yes'");
        } else if ($key == "APP_PAYMENT_MODE") {
            $value = str_replace("Wallet", "Card", $value);
        }
        //Added By HJ On 11-01-2019 For Solved Bug - 6178 As Per Discuss With CD Sir End
        $updateData['vValue'] = trim($value);
        $where = " vName = '" . $key . "' AND eType = '" . $_REQUEST['frm_type'] . "'";
        $res = $obj->MySQLQueryPerform($configTable, $updateData, 'update', $where);
    }
    if ($res) {
        $msgType = 1;
        $msg = $langage_lbl_admin['LBL_Record_Updated_successfully'];
    } else {
        $msgType = 0;
        $msg = "Error in update configuration";
    }
}

$sql = "SELECT * FROM " . CONFIGURATIONS . " WHERE eAdminDisplay = 'Yes' ORDER BY eType, vOrder";
$data_gen = $obj->MySQLSelect($sql);

//country
$sql1 = "SELECT * FROM country WHERE eStatus = 'Active' ";
$country_name = $obj->MySQLSelect($sql1);
foreach ($data_gen as $key => $value) {
    $db_gen[$value['eType']][$key]['iSettingId'] = $value['iSettingId'];
    $db_gen[$value['eType']][$key]['tDescription'] = $value['tDescription'];
    $db_gen[$value['eType']][$key]['vValue'] = $value['vValue'];
    $db_gen[$value['eType']][$key]['tHelp'] = $value['tHelp'];
    $db_gen[$value['eType']][$key]['vName'] = $value['vName'];
    $db_gen[$value['eType']][$key]['eInputType'] = $value['eInputType'];
    $db_gen[$value['eType']][$key]['tSelectVal'] = $value['tSelectVal'];
    $db_gen[$value['eType']][$key]['eZeroAllowed'] = $value['eZeroAllowed'];
    $db_gen[$value['eType']][$key]['eDoubleValueAllowed'] = $value['eDoubleValueAllowed'];
    $db_gen[$value['eType']][$key]['eSpaceAllowed'] = $value['eSpaceAllowed'];
    $db_gen[$value['eType']][$key]['eConfigRequired'] = $value['eConfigRequired'];
}
$sandboxArr = array("STRIPE_SECRET_KEY_SANDBOX", "STRIPE_PUBLISH_KEY_SANDBOX", "BRAINTREE_TOKEN_KEY_SANDBOX", "BRAINTREE_ENVIRONMENT_SANDBOX", "BRAINTREE_MERCHANT_ID_SANDBOX", "BRAINTREE_PUBLIC_KEY_SANDBOX", "BRAINTREE_PRIVATE_KEY_SANDBOX", "PAYMAYA_API_URL_SANDBOX", "PAYMAYA_SECRET_KEY_SANDBOX", "PAYMAYA_PUBLISH_KEY_SANDBOX", "PAYMAYA_ENVIRONMENT_MODE_SANDBOX", "OMISE_SECRET_KEY_SANDBOX", "OMISE_PUBLIC_KEY_SANDBOX", "ADYEN_MERCHANT_ACCOUNT_SANDBOX", "ADYEN_USER_NAME_SANDBOX", "ADYEN_PASSWORD_SANDBOX", "ADYEN_API_URL_SANDBOX", "XENDIT_SECRET_KEY_SANDBOX", "XENDIT_PUBLIC_KEY_SANDBOX");
//echo "<pre>";
for ($r = 0; $r < count($sandboxArr); $r++) {
    $sandboxArr[$r] = rtrim($sandboxArr[$r], "SANDBOX");
}
$getPayDataQuery = "SELECT * FROM " . CONFIGURATIONS_PAYMENT . " WHERE eAdminDisplay = 'Yes' ORDER BY eType, vOrder";

$fetchData = $obj->MySQLSelect($getPayDataQuery);

$getPayFlow = $obj->MySQLSelect("SELECT * FROM " . CONFIGURATIONS_PAYMENT . " WHERE vName = 'SYSTEM_PAYMENT_FLOW'");
$eSystemPayFlow = "Method-1";
if (count($getPayFlow) > 0) {
    $eSystemPayFlow = $getPayFlow[0]['vValue'];
}
//echo "<pre>";print_r($getPayFlow);die;
$cardTxt = $cardTxt1 = "Card";
foreach ($fetchData as $payKey => $payValue) {
    if (isset($payValue['vName']) && $payValue['vName'] == "APP_PAYMENT_MODE" && ($eSystemPayFlow == "Method-2" || $eSystemPayFlow == "Method-3")) {
        $walletTxt = "Wallet";
        $payValue['vValue'] = str_replace($cardTxt1, $walletTxt, $payValue['vValue']);
        $payValue['tSelectVal'] = str_replace($cardTxt1, $walletTxt, $payValue['tSelectVal']);
        $cardTxt = $walletTxt;
    }
    $db_gen[$payValue['eType']][$payKey] = $payValue;
}
//echo "<pre>";print_r($db_gen);exit();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD-->
    <head>
        <meta charset="UTF-8" />
        <title><?= $SITE_NAME; ?> | Configuration</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <link href="../assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />


        <? include_once('global_files.php'); ?>

    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <? include_once('header.php'); ?>
            <? include_once('left_menu.php'); ?>
            <!--PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <div id="add-hide-show-div">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2> General Settings </h2>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="table-list">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">General  Settings</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php if ($msgType == '1') { ?>	
                                                    <div class="alert alert-success alert-dismissable">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><?= $msg ?>
                                                    </div>
                                                <?php } elseif ($msgType == '0') {
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>	<?= $msg ?> 
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <?php
                                            foreach ($db_gen as $key => $value) {
                                                $newKey = str_replace(" ", "_", $key);
                                                ?>
                                                <li class="<?php echo $activeTab == $newKey ? 'active' : '' ?>">
                                                    <a data-toggle="tab" href="#<?= $newKey ?>">
                                                        <?php
                                                        if ($key == "Apperance")
                                                            echo "Appearance";
                                                        else
                                                            echo $key;
                                                        ?>
                                                    </a>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php
                                            $paymentEnvMode = "";
                                            foreach ($db_gen as $key => $value) {
                                                $cnt = count($value);
                                                $tab1 = ceil(count($value) / 2);
                                                $tab2 = $cnt - $tab1;
                                                $newKey = str_replace(" ", "_", $key);
                                                if ($key != "Payment") {
                                                    ?>
                                                    <div id="<?= $newKey ?>" class="tab-pane <?php echo $activeTab == $newKey ? 'active' : '' ?>">
                                                        <form method="POST" action="" name="frm_<?= $key ?>">
                                                            <input type="hidden" name="frm_type" value="<?= $key ?>">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <?php
                                                                    $i = 0;
                                                                    $temp = true;
                                                                    foreach ($value as $key1 => $value1) {
                                                                        $i++;
                                                                        if ($tab1 < $i && $temp) {
                                                                            $temp = false;
                                                                            ?>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                        <?php } ?>
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if ($value1['vName'] == 'RIDER_EMAIL_VERIFICATION') {
                                                                                if (ONLYDELIVERALL != "Yes") {
                                                                                    ?>
                                                                                    <label><?= $value1['tDescription'] ?><?php if ($value1['tHelp'] != "") { ?> <i class="icon-question-sign" data-placement="top" data-toggle="tooltip" data-original-title='<?= htmlspecialchars($value1['tHelp'], ENT_QUOTES, 'UTF-8') ?>'></i><?php } ?></label>
                                                                                    <?php
                                                                                }
                                                                            } else {
                                                                                ?>	
                                                                                <label><?= $value1['tDescription'] ?><?php if ($value1['tHelp'] != "") { ?> <i class="icon-question-sign" data-placement="top" data-toggle="tooltip" data-original-title='<?= htmlspecialchars($value1['tHelp'], ENT_QUOTES, 'UTF-8') ?>'></i><?php } ?></label>
                                                                            <?php } ?>
                                                                            <?php if ($value1['eInputType'] == 'Textarea') { ?>
                                                                                <textarea class="form-control" rows="5" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> ><?= $value1['vValue'] ?></textarea>
                                                                                <?php
                                                                            } elseif ($value1['eInputType'] == 'Select') {
                                                                                $optionArr = explode(',', $value1['tSelectVal']);
                                                                                if ($value1['vName'] == 'DEFAULT_COUNTRY_CODE_WEB') {
                                                                                    ?>
                                                                                    <select class="form-control" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                        <?php
                                                                                        foreach ($country_name as $Value) {
                                                                                            $selected = $value1['vValue'] == $Value['vCountryCode'] ? 'selected' : '';
                                                                                            ?>
                                                                                            <option value="<?= $Value['vCountryCode'] ?>" <?= $selected ?>><?= $Value['vCountry'] . ' (' . $Value['vCountryCode'] . ')'; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                <?php } else if ($value1['vName'] == 'ENABLE_HAIL_RIDES') { ?>
                                                                                    <select class="form-control" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                        <?php
                                                                                        foreach ($optionArr as $oKey => $oValue) {
                                                                                            $selected = $oValue == $value1['vValue'] ? 'selected' : '';
                                                                                            ?>
                                                                                            <option value="<?= $oValue ?>" <?= $selected ?>><?= $oValue ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                    <div> [Note: This option will not work if you have selected payment mode "<?= $cardTxt; ?>"] </div>
                                                                                <?php } else if ($value1['vName'] == 'DRIVER_REQUEST_METHOD') {
                                                                                    ?>
                                                                                    <select class="form-control" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                        <?php
                                                                                        foreach ($optionArr as $oKey => $oValue) {
                                                                                            $selected = $oValue == $value1['vValue'] ? 'selected' : '';
                                                                                            if ($oValue == 'All') {
                                                                                                $oValuenew = $oValue . " (COMPETITIVE ALGORITHM)";
                                                                                            } else if ($oValue == 'Distance') {
                                                                                                $oValuenew = $oValue . " (Nearest 1st Algorithm)";
                                                                                            } else if ($oValue == 'Time') {
                                                                                                $oValuenew = $oValue . " (FIFO Algorithm)";
                                                                                            } else {
                                                                                                $oValuenew = $oValue;
                                                                                            }
                                                                                            ?>
                                                                                            <option value="<?= $oValue ?>" <?= $selected ?>><?= $oValuenew ?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                    <?php
                                                                                } else if ($value1['vName'] == 'RIDER_EMAIL_VERIFICATION') {
                                                                                    if (ONLYDELIVERALL != "Yes") {
                                                                                        ?>
                                                                                        <select class="form-control" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                            <?php
                                                                                            foreach ($optionArr as $oKey => $oValue) {
                                                                                                $selected = $oValue == $value1['vValue'] ? 'selected' : '';
                                                                                                ?>
                                                                                                <option value="<?= $oValue ?>" <?= $selected ?>><?= $oValue ?></option>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </select>
                                                                                        <?php
                                                                                    }
                                                                                } else {
                                                                                    $onChangeEvent = "";
                                                                                    if ($value1['vName'] == 'TRIP_TRACKING_METHOD') {
                                                                                        $onChangeEvent = 'onchange="showConfimbox(this.value);"';
                                                                                    }
                                                                                    ?>
                                                                                    <select <?= $onChangeEvent; ?> class="form-control" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                        <?php
                                                                                        foreach ($optionArr as $oKey => $oValue) {
                                                                                            $selected = $oValue == $value1['vValue'] ? 'selected' : '';
                                                                                            ?>
                                                                                            <option value="<?= $oValue ?>" <?= $selected ?>><?= $oValue ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>

                                                                                <?php } ?>
                                                                                <?php
                                                                            } else {
                                                                                if ($value1['eInputType'] == 'Number') {
                                                                                    ?>
                                                                                    <input type="number" name="Data[<?= $value1['vName'] ?>]" id = "<?= $value1['vName'] ?>" class="form-control numberfield <?= $value1['vName'] ?>" value="<?= $value1['vValue'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> <? if ($value1['eZeroAllowed'] == 'Yes') { ?> min="0" <? } else { ?> min="1" <? } ?>  <? if ($value1['eDoubleValueAllowed'] == 'No') { ?> onkeypress="return event.charCode >= 48 && event.charCode <= 57" <? } else { ?> step = 0.01 <? } ?> >
																				<?php }elseif ($value1['eInputType'] == 'Time') {?>	
																				
 <input type="time" name="Data[<?= $value1['vName'] ?>]" class="form-control date" value="<?= $value1['vValue'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                <? } else { ?>

                                                                                    <input type="text" name="Data[<?= $value1['vName'] ?>]" class="form-control date" value="<?= $value1['vValue'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> <? if ($value1['eSpaceAllowed'] == 'No') { ?> onkeyup="nospaces(this)" <? } ?> >
                                                                                    <?
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group" style="text-align: center;">
                                                                        <input type="submit" name="submitbutton" class="btn btn-primary save-configuration" value="Save Changes">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                <?php } else {
                                                    ?>
                                                    <div id="<?= $newKey ?>" class="tab-pane <?php echo $activeTab == $newKey ? 'active' : '' ?>">
                                                        <form method="POST" action="" name="frm_<?= $key ?>" novalidate>
                                                            <input type="hidden" name="frm_type" value="<?= $key ?>">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <?php
                                                                    $i = 0;
                                                                    $temp = true;
                                                                    foreach ($value as $key1 => $value1) {
                                                                        $i++;
                                                                        if ($tab1 < $i && $temp) {
                                                                            $temp = false;
                                                                        }
                                                                        ?>
                                                                        <div class="form-group">
                                                                            <label class="<?= $value1['vName'] ?>"><?= $value1['tDescription'] ?><?php if ($value1['tHelp'] != "") { ?> <i class="icon-question-sign" data-placement="top" data-toggle="tooltip" data-original-title='<?= htmlspecialchars($value1['tHelp'], ENT_QUOTES, 'UTF-8') ?>'></i><?php } ?></label>
                                                                            <?php
                                                                            if ($value1['eInputType'] == 'Textarea') {
                                                                                ?>
                                                                                <textarea class="form-control" rows="5" name="Data[<?= $value1['vName'] ?>]" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required="required" <? } ?>><?= $value1['vValue'] ?></textarea>
                                                                                <?php
                                                                            } elseif ($value1['eInputType'] == 'Select') {
                                                                                $optionArr = explode(',', $value1['tSelectVal']);
                                                                                $onChangedEvent = "";
                                                                                
                                                                                if ($value1['vName'] == "SYSTEM_PAYMENT_ENVIRONMENT") {
                                                                                    $onChangedEvent = 'onchange="changePayEnv();"';
                                                                                    $paymentEnvMode = $value1['vValue'];
                                                                                    //echo "<pre>";print_r($value1['vValue']);die;
                                                                                }
                                                                                ?>
                                                                                <select class="form-control <?= $value1['vName'] ?>" name="Data[<?= $value1['vName'] ?>]" id="<?= $value1['vName'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?>>
                                                                                    <?php
                                                                                    foreach ($optionArr as $oKey => $oValue) {
                                                                                        $selected = $oValue == $value1['vValue'] ? 'selected' : '';
                                                                                        ?>
                                                                                        <option value="<?= $oValue ?>" <?= $selected ?>><?= $oValue ?></option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            <?php } elseif ($value1['eInputType'] == 'Number') {
                                                                                ?>
                                                                                <input type="number" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> name="Data[<?= $value1['vName'] ?>]"<? if ($value1['eZeroAllowed'] == 'Yes') { ?> min="0" <? } else { ?> min="1" <? } ?>  id = "<?= $value1['vName'] ?>" class="form-control numberfield <?= $value1['vName'] ?>" value="<?= $value1['vValue'] ?>" <? if ($value1['eDoubleValueAllowed'] == 'No') { ?> onkeypress="return event.charCode >= 48 && event.charCode <= 57" <? } ?> >
																			<?php } elseif ($value1['eInputType'] == 'Time') {
                                                                                ?>
																				<input type="time" name="Data[<?= $value1['vName'] ?>]" id = "<?= $value1['vName'] ?>" class="form-control <?= $value1['vName'] ?>" value="<?= $value1['vValue'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> >
                                                                            <? } else {
                                                                                ?>
                                                                                <input type="text" name="Data[<?= $value1['vName'] ?>]" id = "<?= $value1['vName'] ?>" class="form-control <?= $value1['vName'] ?>" value="<?= $value1['vValue'] ?>" <?php if ($value1['eConfigRequired'] == 'Yes') { ?> required <? } ?> <? if ($value1['eSpaceAllowed'] == 'No') { ?>onkeyup="nospaces(this)" <? } ?> >
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group" style="text-align: center;">
                                                                        <input type="submit" name="submitbutton" class="btn btn-primary save-configuration" value="Save Changes">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <?
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div> <!--TABLE-END-->
                        </div>
                    </div>
                    <div class="clear"></div>
                    <?php if (SITE_TYPE != 'Demo') { ?>
                        <div class="admin-notes">
                            <h4>Notes:</h4>
                            <ul>
                                <li>
                                    Please close the application and open it again to see the settings reflected after saving the new setting values above.
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!--END PAGE CONTENT -->
        </div>
        <!--END MAIN WRAPPER -->
        <?
        include_once('footer.php');
        ?>
        <script src="../assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="../assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
                                                                    var boxesArr = stripearray = xenditarray = braintreearray = paymayarray = omisarray = adyenarray = someKeys = fakeArr = [];
                                                                    var previous = '';
                                                                    var prevEnvMode = '<?= $paymentEnvMode; ?>';
                                                                    function checkDec(el) {
                                                                        var ex = /^\d+(\.\d{0,2})?$/g;
                                                                        if (ex.test(el.value) == false) {
                                                                            el.value = el.value.substring(0, el.value.length - 1);
                                                                        }
                                                                    }
                                                                    $('[data-toggle="tooltip"]').tooltip();
                                                                    $(document).ready(function () {
                                                                        $('#dataTables-example').dataTable();
<?php foreach ($sandboxArr as $key => $val) { ?>
                                                                            boxesArr.push('<?php echo $val; ?>');
<?php } ?>
                                                                        changePayEnv();
                                                                    });
                                                                    function changePayEnv() {
                                                                        var payEnv = $("#SYSTEM_PAYMENT_ENVIRONMENT").val();
                                                                        var apppaymentmethod = $("#APP_PAYMENT_METHOD").val();
                                                                        someKeys = ['APP_PAYMENT_METHOD', 'STRIPE_SECRET_KEY_SANDBOX', 'STRIPE_PUBLISH_KEY_SANDBOX', 'STRIPE_SECRET_KEY_LIVE', 'STRIPE_PUBLISH_KEY_LIVE', 'XENDIT_SECRET_KEY_SANDBOX', 'XENDIT_PUBLIC_KEY_SANDBOX', 'XENDIT_SECRET_KEY_LIVE', 'XENDIT_PUBLIC_KEY_LIVE', 'BRAINTREE_TOKEN_KEY_SANDBOX', 'BRAINTREE_ENVIRONMENT_SANDBOX', 'BRAINTREE_MERCHANT_ID_SANDBOX', 'BRAINTREE_PUBLIC_KEY_SANDBOX', 'BRAINTREE_PRIVATE_KEY_SANDBOX', 'BRAINTREE_ENVIRONMENT_LIVE', 'BRAINTREE_MERCHANT_ID_LIVE', 'BRAINTREE_PUBLIC_KEY_LIVE', 'BRAINTREE_PRIVATE_KEY_LIVE', 'BRAINTREE_CHARGE_AMOUNT', 'PAYMAYA_API_URL_SANDBOX', 'PAYMAYA_SECRET_KEY_SANDBOX', 'PAYMAYA_PUBLISH_KEY_SANDBOX', 'PAYMAYA_ENVIRONMENT_MODE_SANDBOX', 'PAYMAYA_API_URL_LIVE', 'PAYMAYA_SECRET_KEY_LIVE', 'PAYMAYA_PUBLISH_KEY_LIVE', 'PAYMAYA_ENVIRONMENT_MODE_LIVE', 'OMISE_SECRET_KEY_SANDBOX', 'OMISE_PUBLIC_KEY_SANDBOX', 'OMISE_SECRET_KEY_LIVE', 'OMISE_PUBLIC_KEY_LIVE', 'ADYEN_MERCHANT_ACCOUNT_SANDBOX', 'ADYEN_USER_NAME_SANDBOX', 'ADYEN_PASSWORD_SANDBOX', 'ADYEN_API_URL_SANDBOX', 'ADYEN_MERCHANT_ACCOUNT_LIVE', 'ADYEN_USER_NAME_LIVE', 'ADYEN_PASSWORD_LIVE', 'ADYEN_API_URL_LIVE', 'ADYEN_CHARGE_AMOUNT', 'PAYMAYA_CHECKOUT_PUBLISH_KEY_SANDBOX', 'PAYMAYA_CHECKOUT_PUBLISH_KEY_LIVE'];
                                                                        stripearray = ['STRIPE_SECRET_KEY_SANDBOX', 'STRIPE_PUBLISH_KEY_SANDBOX', 'STRIPE_SECRET_KEY_LIVE', 'STRIPE_PUBLISH_KEY_LIVE'];
                                                                        xenditarray = ['XENDIT_SECRET_KEY_SANDBOX', 'XENDIT_PUBLIC_KEY_SANDBOX', 'XENDIT_SECRET_KEY_LIVE', 'XENDIT_PUBLIC_KEY_LIVE'];
                                                                        braintreearray = ['BRAINTREE_TOKEN_KEY_SANDBOX', 'BRAINTREE_ENVIRONMENT_SANDBOX', 'BRAINTREE_MERCHANT_ID_SANDBOX', 'BRAINTREE_PUBLIC_KEY_SANDBOX', 'BRAINTREE_PRIVATE_KEY_SANDBOX', 'BRAINTREE_ENVIRONMENT_LIVE', 'BRAINTREE_MERCHANT_ID_LIVE', 'BRAINTREE_PUBLIC_KEY_LIVE', 'BRAINTREE_PRIVATE_KEY_LIVE', 'BRAINTREE_CHARGE_AMOUNT'];
                                                                        paymayarray = ['PAYMAYA_API_URL_SANDBOX', 'PAYMAYA_SECRET_KEY_SANDBOX', 'PAYMAYA_PUBLISH_KEY_SANDBOX', 'PAYMAYA_ENVIRONMENT_MODE_SANDBOX', 'PAYMAYA_API_URL_LIVE', 'PAYMAYA_SECRET_KEY_LIVE', 'PAYMAYA_PUBLISH_KEY_LIVE', 'PAYMAYA_ENVIRONMENT_MODE_LIVE', 'PAYMAYA_CHECKOUT_PUBLISH_KEY_SANDBOX', 'PAYMAYA_CHECKOUT_PUBLISH_KEY_LIVE'];
                                                                        omisarray = ['OMISE_SECRET_KEY_SANDBOX', 'OMISE_PUBLIC_KEY_SANDBOX', 'OMISE_SECRET_KEY_LIVE', 'OMISE_PUBLIC_KEY_LIVE'];
                                                                        adyenarray = ['ADYEN_MERCHANT_ACCOUNT_SANDBOX', 'ADYEN_USER_NAME_SANDBOX', 'ADYEN_PASSWORD_SANDBOX', 'ADYEN_API_URL_SANDBOX', 'ADYEN_MERCHANT_ACCOUNT_LIVE', 'ADYEN_USER_NAME_LIVE', 'ADYEN_PASSWORD_LIVE', 'ADYEN_API_URL_LIVE', 'ADYEN_CHARGE_AMOUNT'];
                                                                        $.each(boxesArr, function (key, value) {
                                                                            someKeys.push(value + "LIVE");
                                                                            someKeys.push(value + "SANDBOX");
                                                                        });
                                                                        paymentConfing();
                                                                    }
                                                                    function confirm_delete()
                                                                    {
                                                                        var confirm_ans = confirm("Are You sure You want to Delete Driver?");
                                                                        return confirm_ans;
                                                                        //document.getElementById(id).submit();
                                                                    }
                                                                    function changeCode(id)
                                                                    {
                                                                        var request = $.ajax({
                                                                            type: "POST",
                                                                            url: 'change_code.php',
                                                                            data: 'id=' + id,

                                                                            success: function (data)
                                                                            {
                                                                                document.getElementById("code").value = data;
                                                                                //window.location = 'profile.php';
                                                                            }
                                                                        });
                                                                    }
                                                                    $(function () {
                                                                        paymentConfing();
                                                                        var apppaymentmode = $('#APP_PAYMENT_MODE').val();
                                                                        var apppaymentmethod = $("#APP_PAYMENT_METHOD").val();
                                                                        $('#APP_PAYMENT_MODE').change(function () {
                                                                            if ($('#APP_PAYMENT_MODE').val() == '<?= $cardTxt; ?>' || $('#APP_PAYMENT_MODE').val() == 'Cash-<?= $cardTxt; ?>') {
                                                                                $('#APP_PAYMENT_METHOD,.APP_PAYMENT_METHOD').show();
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Stripe') {
                                                                                    $.each(stripearray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(stripearray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Braintree') {
                                                                                    $.each(braintreearray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(braintreearray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Paymaya') {
                                                                                    $.each(paymayarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(paymayarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Omise') {
                                                                                    $.each(omisarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(omisarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Adyen') {
                                                                                    $.each(adyenarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(adyenarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Xendit') {
                                                                                    $.each(xenditarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(xenditarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                $("#APP_PAYMENT_METHOD").on('focus', function () {
                                                                                    previous = this.value;
                                                                                    console.log(previous);
                                                                                }).change(function () {
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Stripe') {
                                                                                        $.each(stripearray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(stripearray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Braintree') {
                                                                                        $.each(braintreearray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(braintreearray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Paymaya') {
                                                                                        $.each(paymayarray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(paymayarray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Omise') {
                                                                                        $.each(omisarray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(omisarray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Adyen') {
                                                                                        $.each(adyenarray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(adyenarray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                    if ($("#APP_PAYMENT_METHOD").val() == 'Xendit') {
                                                                                        $.each(xenditarray, function (key, value) {
                                                                                            $('.' + value).show();
                                                                                        });
                                                                                    } else {
                                                                                        $.each(xenditarray, function (key, value) {
                                                                                            $('.' + value).hide();
                                                                                        });
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                $.each(someKeys, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                        });
                                                                        $("form").submit(function () {
                                                                            //Added By HJ On 11-06-2019 For Reset User Data When Change Payment Environment Mode Start
                                                                            var clearUserData =0;
                                                                            if(prevEnvMode != $('#SYSTEM_PAYMENT_ENVIRONMENT').val()){
                                                                                var clearUserData =1;
                                                                            }
                                                                            //Added By HJ On 11-06-2019 For Reset User Data When Change Payment Environment Mode End
                                                                            if ((previous != '' && $('#APP_PAYMENT_MODE').val() != 'Cash') || clearUserData ==1) {
                                                                                var status = confirm("Please note that changing payment gateway will reset all your <?php echo strtolower($langage_lbl_admin['LBL_RIDER']); ?>'s saved credit <?= $cardTxt; ?> details through last set payment gateway. <?php echo $langage_lbl_admin['LBL_RIDERS_ADMIN']; ?> will have to re-enter credit <?= $cardTxt; ?> details for new payment gateway once they make a first transaction.Click OK to continue?");
                                                                                if (status == false) {
                                                                                    return false;
                                                                                } else {
                                                                                    var request = $.ajax({
                                                                                        type: "POST",
                                                                                        url: "ajax_payment_method.php",
                                                                                        data: "paymentmethod=" + previous,
                                                                                        success: function (data) {
                                                                                            //alert(previous);
                                                                                            return false;
                                                                                        }
                                                                                    });
                                                                                }
                                                                            } else {
                                                                                //$("#APP_PAYMENT_METHOD").val(previous);
                                                                                return true;
                                                                            }
                                                                        });
                                                                    });
                                                                    function paymentConfing() {
                                                                        var apppaymentmode = $('#APP_PAYMENT_MODE').val();
                                                                        var apppaymentmethod = $("#APP_PAYMENT_METHOD").val();
                                                                        if (apppaymentmode == '<?= $cardTxt; ?>' || apppaymentmode == 'Cash-<?= $cardTxt; ?>') {
                                                                            $('#APP_PAYMENT_METHOD,.APP_PAYMENT_METHOD').show();
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Stripe') {
                                                                                $.each(stripearray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(stripearray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Braintree') {
                                                                                $.each(braintreearray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(braintreearray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Paymaya') {
                                                                                $.each(paymayarray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(paymayarray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Omise') {
                                                                                $.each(omisarray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(omisarray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Adyen') {
                                                                                $.each(adyenarray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(adyenarray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            if ($("#APP_PAYMENT_METHOD").val() == 'Xendit') {
                                                                                $.each(xenditarray, function (key, value) {
                                                                                    $('.' + value).show();
                                                                                });
                                                                            } else {
                                                                                $.each(xenditarray, function (key, value) {
                                                                                    $('.' + value).hide();
                                                                                });
                                                                            }
                                                                            $("#APP_PAYMENT_METHOD").on('focus', function () {
                                                                                previous = this.value;
                                                                                console.log(previous);
                                                                            }).change(function () {
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Stripe') {
                                                                                    $.each(stripearray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(stripearray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Braintree') {
                                                                                    $.each(braintreearray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(braintreearray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Paymaya') {
                                                                                    $.each(paymayarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(paymayarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Omise') {
                                                                                    $.each(omisarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(omisarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Adyen') {
                                                                                    $.each(adyenarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(adyenarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                                if ($("#APP_PAYMENT_METHOD").val() == 'Xendit') {
                                                                                    $.each(xenditarray, function (key, value) {
                                                                                        $('.' + value).show();
                                                                                    });
                                                                                } else {
                                                                                    $.each(xenditarray, function (key, value) {
                                                                                        $('.' + value).hide();
                                                                                    });
                                                                                }
                                                                            });
                                                                        } else {
                                                                            $.each(someKeys, function (key, value) {
                                                                                $('.' + value).hide();
                                                                            });
                                                                        }
                                                                    }
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
        <script type="text/javascript" src="js/moment.min.js"></script>

        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript">
            function nospaces(t) {
                if (t.value.match(/\s/g)) {
                    alert('Sorry, you are not allowed to enter any spaces');
                    t.value = t.value.replace(/\s/g, '');
                }
            }
            function showConfimbox(type) {
                if (type == "Pubnub") {
                    alert("This option will increase Pubnub.com usage and so increase overall billing. Are you sure you want to select it..?");
                }
            }
             $('input[type="time"]').datetimepicker({
        format: 'HH:mm',
        ignoreReadonly: true,
        useCurrent: false
    });


        </script>
    </body>
    <!-- END BODY-->
</html>
