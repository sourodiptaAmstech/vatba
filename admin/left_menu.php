<?php
$APP_DELIVERY_MODE = $generalobj->getConfigurations("configurations", "APP_DELIVERY_MODE");
$RIDE_LATER_BOOKING_ENABLED = $generalobj->getConfigurations("configurations", "RIDE_LATER_BOOKING_ENABLED");
$DRIVER_SUBSCRIPTION_ENABLE = $generalobj->getConfigurations("configurations", "DRIVER_SUBSCRIPTION_ENABLE");

$catdata = serviceCategories;
$allservice_cat_data = json_decode($catdata, true);
if ($APP_TYPE == 'UberX') {
    $menu = include 'left_menu_ufx_array.php';
} else if (ONLYDELIVERALL == "Yes") {
    $menu = include 'left_menu_deliverall_array.php';
} else {
    $menu = include 'left_menu_uberapp_array.php';
}
?>
<section class="sidebar">
    <!-- Sidebar -->
    <div id="sidebar" class="test" >
        <nav class="menu">
            <?php echo get_admin_nav($menu); ?>
        </nav>
    </div>
</section>