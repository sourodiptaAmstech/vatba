<?php
include_once('../common.php');
define("SERVICE_CATEGORIES", "service_categories");
define("CUISINE", "cuisine");
define("COMPANY", "company");
define("COMPANY_CUISINE", "company_cuisine");
define("FOOD_MENU", "food_menu");
define("MENU_ITEMS", "menu_items");
if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}

/* Use For Demo */
$storeIdArray = array('36');
/* Use For Demo */

////$generalobjAdmin->check_member_login();

if (!$userObj->hasPermission('view-store')) {
    $userObj->redirect();
}

$script = 'DeliverAllStore';
$eSystem = " AND  c.eSystem ='DeliverAll'";

//Start Sorting
$sortby = isset($_REQUEST['sortby']) ? $_REQUEST['sortby'] : 0;
$order = isset($_REQUEST['order']) ? $_REQUEST['order'] : '';
$ord = ' ORDER BY c.iCompanyId DESC';
if ($sortby == 1) {
    if ($order == 0)
        $ord = " ORDER BY c.vCompany ASC";
    else
        $ord = " ORDER BY c.vCompany DESC";
}

if ($sortby == 2) {
    if ($order == 0)
        $ord = " ORDER BY c.vEmail ASC";
    else
        $ord = " ORDER BY c.vEmail DESC";
}

if ($sortby == 3) {
    if ($order == 0)
        $ord = " ORDER BY `count` ASC";
    else
        $ord = " ORDER BY `count` DESC";
}

if ($sortby == 4) {
    if ($order == 0)
        $ord = " ORDER BY c.eStatus ASC";
    else
        $ord = " ORDER BY c.eStatus DESC";
}
//End Sorting

$cmp_ssql = "";
// if (SITE_TYPE == 'Demo') {
// $cmp_ssql = " And c.tRegistrationDate > '" . WEEK_DATE . "'";
// }
$dri_ssql = "";
if (SITE_TYPE == 'Demo') {
    $dri_ssql = " And c.tRegistrationDate > '" . WEEK_DATE . "'";
}
// Start Search Parameters
$option = isset($_REQUEST['option']) ? $_REQUEST['option'] : "";
$keyword = isset($_REQUEST['keyword']) ? stripslashes($_REQUEST['keyword']) : "";
$select_cat = isset($_REQUEST['selectcategory']) ? stripslashes($_REQUEST['selectcategory']) : "";
$searchDate = isset($_REQUEST['searchDate']) ? $_REQUEST['searchDate'] : "";
$eStatus = isset($_REQUEST['eStatus']) ? $_REQUEST['eStatus'] : "";

if (isset($_POST['copystore'])) {
    $companyId = $_POST['storeId'];
    $copyStores = $_POST['store_sel'];
    //echo "<pre>";
    //print_r($copyStores);die;
    if (count($copyStores) > 0 && $companyId > 0) {
        $getStoreFoodMenu = $obj->MySQLSelect("SELECT * FROM " . FOOD_MENU . " WHERE iCompanyId='" . $companyId . "' AND eStatus!='eStatus'");
        for ($m = 0; $m < count($getStoreFoodMenu); $m++) {
            $iFoodMenuId = $getStoreFoodMenu[$m]['iFoodMenuId'];
            $vMenu_EN = $getStoreFoodMenu[$m]['vMenu_EN'];
            $getMenuItems = $obj->MySQLSelect("SELECT * FROM " . MENU_ITEMS . " WHERE iFoodMenuId='" . $iFoodMenuId . "' AND eStatus!='eStatus'");
            for ($s = 0; $s < count($copyStores); $s++) {
                $storeId = $copyStores[$s];
                if ($companyId != $storeId) {
                    //Start For Copy Food Menu and Menu Item Data
                    $chekc_food = $obj->MySQLSelect("SELECT iFoodMenuId FROM " . FOOD_MENU . " WHERE iCompanyId='" . $storeId . "' AND vMenu_EN='" . $vMenu_EN . "'");
                    if (count($chekc_food) > 0) { // Update Exists Food menu Data
                        for ($f = 0; $f < count($chekc_food); $f++) {
                            $oldFoodId = $chekc_food[$f]['iFoodMenuId'];
                            $updateFields = "";
                            unset($getStoreFoodMenu[$m]['iFoodMenuId']);
                            foreach ($getStoreFoodMenu[$m] as $key1 => $val1) {
                                if ($key1 == "iCompanyId") {
                                    $val1 = $storeId;
                                }
                                $updateFields .= ",`$key1`='" . $val1 . "'";
                            }
                            if ($updateFields != "") {
                                $updateFields = trim($updateFields, ",");
                                $updateQuery = "UPDATE  `" . FOOD_MENU . "` SET $updateFields WHERE iFoodMenuId='" . $oldFoodId . "'";
                                $obj->sql_query($updateQuery);
                            }
                            for ($mi = 0; $mi < count($getMenuItems); $mi++) {
                                $menuItemName = $getMenuItems[$mi]['vItemType_EN'];
                                $chekc_item = $obj->MySQLSelect("SELECT iMenuItemId FROM " . MENU_ITEMS . " WHERE iFoodMenuId='" . $oldFoodId . "' AND vItemType_EN='" . $menuItemName . "'");
                                $updateFields1 = "";
                                unset($getMenuItems[$mi]['iMenuItemId']);
                                foreach ($getMenuItems[$mi] as $key2 => $val2) {
                                    if ($key2 == "iFoodMenuId") {
                                        $val2 = $oldFoodId;
                                    }
                                    $updateFields1 .= ",`$key2`='" . $val2 . "'";
                                }
                                if ($updateFields1 != "") {
                                    $updateFields1 = trim($updateFields1, ",");
                                    if (count($chekc_item) > 0) {
                                        for ($mr = 0; $mr < count($chekc_item); $mr++) {
                                            $food_ItemId = $chekc_item[$mr]['iMenuItemId'];
                                            $updateQuery1 = "UPDATE  `" . MENU_ITEMS . "` SET $updateFields1 WHERE iMenuItemId='" . $food_ItemId . "'";
                                            $obj->sql_query($updateQuery1);
                                        }
                                    } else {
                                        $updateQuery1 = "INSERT INTO  `" . MENU_ITEMS . "` SET $updateFields1";
                                        $obj->sql_query($updateQuery1);
                                        $food_ItemId = $obj->GetInsertId();
                                    }
                                }
                            }
                        }
                    } else { // Insert New Food menu Data
                        $updateFields = "";
                        unset($getStoreFoodMenu[$m]['iFoodMenuId']);
                        foreach ($getStoreFoodMenu[$m] as $key1 => $val1) {
                            if ($key1 == "iCompanyId") {
                                $val1 = $storeId;
                            }
                            $updateFields .= ",`$key1`='" . $val1 . "'";
                        }
                        if ($updateFields != "") {
                            $updateFields = trim($updateFields, ",");
                            $updateQuery = "INSERT INTO  `" . FOOD_MENU . "` SET $updateFields";
                            $obj->sql_query($updateQuery);
                            $company_foodId = $obj->GetInsertId();
                            if ($company_foodId > 0) {
                                for ($mi = 0; $mi < count($getMenuItems); $mi++) {
                                    $menuItemName = $getMenuItems[$mi]['vItemType_EN'];
                                    $updateFields1 = "";
                                    unset($getMenuItems[$mi]['iMenuItemId']);
                                    foreach ($getMenuItems[$mi] as $key2 => $val2) {
                                        if ($key2 == "iFoodMenuId") {
                                            $val2 = $company_foodId;
                                        }
                                        $updateFields1 .= ",`$key2`='" . $val2 . "'";
                                    }
                                    if ($updateFields1 != "") {
                                        $updateFields1 = trim($updateFields1, ",");
                                        $updateQuery1 = "INSERT INTO  `" . MENU_ITEMS . "` SET $updateFields1";
                                        $obj->sql_query($updateQuery1);
                                        $food_ItemId = $obj->GetInsertId();
                                    }
                                }
                            }
                        }
                    }
                    //End For Copy Food Menu and Menu Item Data
                    //Start For Copy Company Cuisine Data
                    $getCompanyCuisine = $obj->MySQLSelect("SELECT * FROM " . COMPANY_CUISINE . " WHERE iCompanyId='" . $companyId . "'");
                    $existCompanyCuisine = $obj->MySQLSelect("SELECT * FROM " . COMPANY_CUISINE . " WHERE iCompanyId='" . $storeId . "'");
                    $cCuisinePkArr = array();
                    if (count($existCompanyCuisine) > 0) {
                        for ($ec = 0; $ec < count($existCompanyCuisine); $ec++) {
                            $cCuisinePkArr[] = $existCompanyCuisine[$ec]['ccId'];
                        }
                    }
                    for ($c = 0; $c < count($getCompanyCuisine); $c++) {
                        $cCuisineId = $getCompanyCuisine[$c]['cuisineId'];
                        $cuisinePkId = $getCompanyCuisine[$c]['ccId'];
                        if (!in_array($cuisinePkId, $cCuisinePkArr)) {
                            //print_r($getCompanyCuisine[$c]);die;
                            $insertFields = "";
                            unset($getCompanyCuisine[$c]['ccId']);
                            foreach ($getCompanyCuisine[$c] as $key3 => $val3) {
                                if ($key3 == "iCompanyId") {
                                    $val3 = $storeId;
                                }
                                $insertFields .= ",`$key3`='" . $val3 . "'";
                            }
                            //print_r($checkCompanyCuisine);die;
                            if ($insertFields != "") {
                                $insertFields = trim($insertFields, ",");
                                $checkCompanyCuisine = $obj->MySQLSelect("SELECT ccId FROM " . COMPANY_CUISINE . " WHERE iCompanyId='" . $storeId . "' AND cuisineId='" . $cCuisineId . "' ORDER BY ccId ASC");
                                if (count($checkCompanyCuisine) > 0) {
                                    for ($cu = 0; $cu < count($checkCompanyCuisine); $cu++) {
                                        $copmCId = $checkCompanyCuisine[$cu]['ccId'];
                                        $wherecId = "ccId='" . $copmCId . "'";
                                        if ($cu == 0) {
                                            $insertQuery = "UPDATE `" . COMPANY_CUISINE . "` SET $insertFields WHERE $wherecId";
                                            $obj->sql_query($insertQuery);
                                        } else {
                                            $deletequery = "DELETE FROM `" . COMPANY_CUISINE . "` WHERE $wherecId";
                                            $obj->sql_query($deletequery);
                                        }
                                    }
                                } else {
                                    $insertQuery = "INSERT INTO  `" . COMPANY_CUISINE . "` SET $insertFields";
                                    $obj->sql_query($insertQuery);
                                    $company_cuisineId = $obj->GetInsertId();
                                }
                            }
                        }
                    }
                    //End For Copy Company Cuisine Data
                }
            }
        }
    }else{
        
    }
}

$ssql = '';
if ($keyword != '') {
    $keyword_new = $keyword;
    $chracters = array("(", "+", ")");
    $removespacekeyword = preg_replace('/\s+/', '', $keyword);
    $keyword_new = trim(str_replace($chracters, "", $removespacekeyword));
    if (is_numeric($keyword_new)) {
        $keyword_new = $keyword_new;
    } else {
        $keyword_new = $keyword;
    }
    if ($option != '') {
        $option_new = $option;
        if ($option == 'MobileNumber') {
            $option_new = "CONCAT(c.vCode,'',c.vPhone)";
        }
        if ($eStatus != '') {
            $ssql .= " AND " . stripslashes($option_new) . " LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "'";
        }if ($select_cat != "") {
            $ssql .= " AND " . stripslashes($option_new) . " LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "' ";
        }if ($select_cat != "" && $eStatus != '') {
            $ssql .= " AND " . stripslashes($option_new) . " LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "' AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "' ";
        } else {
            $ssql .= " AND " . stripslashes($option_new) . " LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%'";
        }
    } else {

        if ($eStatus == '' && $select_cat != "" && $keyword_new != "") {
            $ssql .= " AND (c.vCompany LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR c.vEmail LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR (concat(c.vCode,'',c.vPhone) LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%')) AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "'";
        } else if ($eStatus != '' && $select_cat != "") {
            $ssql .= " AND (c.vCompany LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR c.vEmail LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR (concat(c.vCode,'',c.vPhone) LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%')) AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "' AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "'";
        } else if ($eStatus != '') {
            $ssql .= " AND (c.vCompany LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR c.vEmail LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR (concat(c.vCode,'',c.vPhone) LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%')) AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "'";
        } else if ($select_cat != "") {
            $ssql .= " AND (c.vCompany LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR c.vEmail LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR (concat(c.vCode,'',c.vPhone) LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%')) AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "' AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "'";
        } else {
            $ssql .= " AND (c.vCompany LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR c.vEmail LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%' OR (concat(c.vCode,'',c.vPhone) LIKE '%" . $generalobjAdmin->clean($keyword_new) . "%'))";
        }
    }
} else if ($eStatus != '' && $select_cat != "" && $keyword == '') {
    $ssql .= " AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "' AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "'";
} else if ($eStatus != '' && $keyword == '' && $select_cat == "") {
    $ssql .= " AND c.eStatus = '" . $generalobjAdmin->clean($eStatus) . "'";
} else if ($eStatus == '' && $keyword == '' && $select_cat != "") {
    $ssql .= " AND sc.iServiceId = '" . $generalobjAdmin->clean($select_cat) . "'";
}

// End Search Parameters
//Pagination Start
$per_page = $DISPLAY_RECORD_NUMBER; // number of results to show per page
if (!empty($eStatus)) {
    $eStatus_sql = "";
} else {
    $eStatus_sql = " AND c.eStatus != 'Deleted'";
}

$sql = "SELECT COUNT(c.iCompanyId) AS Total FROM company AS c left join service_categories as sc on c.iServiceId = sc.iServiceId WHERE 1 = 1 and sc.eStatus='Active' $eSystem $eStatus_sql $ssql $cmp_ssql";
$totalData = $obj->MySQLSelect($sql);

$total_results = $totalData[0]['Total'];
$total_pages = ceil($total_results / $per_page); //total pages we going to have
$show_page = 1;

//-------------if page is setcheck------------------//
if (isset($_GET['page'])) {
    $show_page = $_GET['page'];             //it will telles the current page
    if ($show_page > 0 && $show_page <= $total_pages) {
        $start = ($show_page - 1) * $per_page;
        $end = $start + $per_page;
    } else {
        // error - show first set of results
        $start = 0;
        $end = $per_page;
    }
} else {
    // if page isn't set, show first set of results
    $start = 0;
    $end = $per_page;
}
// display pagination
$page = isset($_GET['page']) ? intval($_GET['page']) : 0;
$tpages = $total_pages;
if ($page <= 0)
    $page = 1;
//Pagination End
if (!empty($eStatus)) {
    $equery = "";
} else {
    $equery = " AND  c.eStatus != 'Deleted'";
}

//$sql = "SELECT c.iCompanyId, c.vCompany, c.vEmail, c.vCode,c.vPhone, c.eStatus,c.iServiceId, (SELECT count(iFoodMenuId) FROM food_menu WHERE iCompanyId = c.iCompanyId) as foodcatCount FROM company AS c WHERE 1 = 1 $eSystem $equery $ssql $cmp_ssql $ord LIMIT $start, $per_page";


$sql = "SELECT c.iCompanyId, c.vCompany, c.vEmail, c.vCode,c.vPhone, c.eStatus,c.iServiceId, sc.vServiceName_" . $default_lang . " as servicename ,(SELECT count(iFoodMenuId) FROM food_menu WHERE iCompanyId = c.iCompanyId AND eStatus != 'Deleted') as foodcatCount FROM company AS c  left join service_categories as sc on c.iServiceId = sc.iServiceId WHERE 1 = 1 and sc.eStatus='Active' $eSystem $equery $ssql $cmp_ssql $ord LIMIT $start, $per_page";

$data_drv = $obj->MySQLSelect($sql);
$endRecord = count($data_drv);

$getAllStore = $obj->MySQLSelect("SELECT c.iCompanyId, c.vCompany FROM company AS c  left join service_categories as sc on c.iServiceId = sc.iServiceId WHERE 1 = 1 and sc.eStatus='Active' $eSystem $equery");

$sql1 = "SELECT doc_masterid as total FROM `document_master` WHERE `doc_usertype` ='store' AND status = 'Active'";
$doc_count_query = $obj->MySQLSelect($sql1);
$doc_count = count($doc_count_query);


$var_filter = "";
foreach ($_REQUEST as $key => $val) {
    if ($key != "tpages" && $key != 'page')
        $var_filter .= "&$key=" . stripslashes($val);
}

$reload = $_SERVER['PHP_SELF'] . "?tpages=" . $tpages . $var_filter;

$catdata = serviceCategories;
$service_cat_data = json_decode($catdata, true);
?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD-->
    <head>
        <meta charset="UTF-8" />
        <title><?= $SITE_NAME ?> | <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <?php include_once('global_files.php'); ?>
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >
        <!-- Main LOading -->
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <?php include_once('header.php'); ?>
            <style>
                .multiselect {
                    width: 533px !important;
                }
            </style>
            <?php include_once('left_menu.php'); ?>
            <!--PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <div id="add-hide-show-div">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2><?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?></h2>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <?php include('valid_msg.php'); ?>
                    <form name="frmsearch" id="frmsearch" action="javascript:void(0);" method="post">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="admin-nir-table">
                            <tbody>
                                <tr>
                                    <td width="5%"><label for="textfield"><strong>Search:</strong></label></td>
                                    <td width="10%" class="padding-right10"><select name="option" id="option" class="form-control">
                                            <option value="">All</option>
                                            <option  value="c.vCompany" <?php
                                            if ($option == "c.vCompany") {
                                                echo "selected";
                                            }
                                            ?> >Name</option>
                                            <option value="c.vEmail" <?php
                                            if ($option == 'c.vEmail') {
                                                echo "selected";
                                            }
                                            ?> >E-mail</option>
                                            <option value="MobileNumber" <?php
                                            if ($option == 'MobileNumber') {
                                                echo "selected";
                                            }
                                            ?> >Mobile</option>
                                        </select>
                                    </td>
                                    <td width="145px" class="searchform"><input type="Text" id="keyword" name="keyword" value="<?php echo $keyword; ?>"  class="form-control" /></td>

                                    <td width="140px" class="estatus_options" id="eStatus_options" >
                                        <select name="eStatus" id="estatus_value" class="form-control">
                                            <option value="" >Select Status</option>
                                            <option value='Active' <?php
                                            if ($eStatus == 'Active') {
                                                echo "selected";
                                            }
                                            ?> >Active</option>
                                            <option value="Inactive" <?php
                                            if ($eStatus == 'Inactive') {
                                                echo "selected";
                                            }
                                            ?> >Inactive</option>
                                            <option value="Deleted" <?php
                                            if ($eStatus == 'Deleted') {
                                                echo "selected";
                                            }
                                            ?> >Delete</option>
                                        </select>
                                    </td>


                                    <? if (count($service_cat_data) > 1) { ?>
                                        <td width="145px" class="estatus_options" id="ecategory_options" >
                                            <select name="selectcategory" id="selectcategory" class="form-control">
                                                <option value="" >Select Category</option>
                                                <?php foreach ($service_cat_data as $servicedata) { ?>
                                                    <option value="<?= $servicedata['iServiceId'] ?>" <?php
                                                    if ($select_cat == $servicedata['iServiceId']) {
                                                        echo "selected";
                                                    }
                                                    ?> > <?= $servicedata['vServiceName']; ?></option>
                                                        <? } ?>
                                            </select>
                                        </td>
                                    <? } ?>

                                    <td>
                                        <input type="submit" value="Search" class="btnalt button11" id="Search" name="Search" title="Search" />
                                        <input type="button" value="Reset" class="btnalt button11" onClick="window.location.href = 'store.php'"/>
                                    </td>
                                    <?php if ($userObj->hasPermission('create-store')) { ?>
                                        <td width="15%"><a class="add-btn" href="store_action.php" style="text-align: center;">Add <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?></a></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>

                    </form>
                    <div class="table-list">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="admin-nir-export">
                                    <div class="changeStatus col-lg-12 option-box-left">
                                        <span class="col-lg-2 new-select001">
                                            <?php if ($userObj->hasPermission(['update-status-store', 'delete-store'])) { ?>
                                                <select name="changeStatus" id="changeStatus" class="form-control" onChange="status_check(this.value);">
                                                    <option value="" >Select Action</option>
                                                    <?php if ($userObj->hasPermission('update-status-store')) { ?>
                                                        <option value='Active' <?php
                                                        if ($option == 'Active') {
                                                            echo "selected";
                                                        }
                                                        ?> >Activate</option>
                                                        <option value="Inactive" <?php
                                                        if ($option == 'Inactive') {
                                                            echo "selected";
                                                        }
                                                        ?> >Deactivate</option>
                                                            <?php } ?>
                                                            <?php if ($eStatus != 'Deleted' && $userObj->hasPermission('delete-store')) { ?>
                                                        <option value="Deleted" <?php
                                                        if ($option == 'Delete') {
                                                            echo "selected";
                                                        }
                                                        ?> >Delete</option>
                                                            <?php } ?>
                                                </select>
                                            <?php } ?>
                                        </span>
                                    </div>
                                    <?php if (!empty($data_drv)) { ?>
                                        <div class="panel-heading">
                                            <form name="_export_form" id="_export_form" method="post" >
                                                <button type="button" onClick="showExportTypes('store')" >Export</button>
                                            </form>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="table-responsive">
                                    <form class="_list_form" id="_list_form" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th align="center" width="3%" style="text-align:center;"><input type="checkbox" id="setAllCheck" ></th>
                                                    <th width="20%"><a href="javascript:void(0);" onClick="Redirect(1,<?php
                                                        if ($sortby == '1') {
                                                            echo $order;
                                                        } else {
                                                            ?>0<?php } ?>)"><?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> Name <?php
                                                                           if ($sortby == 1) {
                                                                               if ($order == 0) {
                                                                                   ?><i class="fa fa-sort-amount-asc" aria-hidden="true"></i> <?php } else { ?><i class="fa fa-sort-amount-desc" aria-hidden="true"></i><?php
                                                                }
                                                            } else {
                                                                ?><i class="fa fa-sort" aria-hidden="true"></i> <?php } ?></a></th>                                                <th width="20%"><a href="javascript:void(0);" onClick="Redirect(2,<?php
                                                            if ($sortby == '2') {
                                                                echo $order;
                                                            } else {
                                                                ?>0<?php } ?>)">Email <?php
                                                                                                                                                                                                        if ($sortby == 2) {
                                                                                                                                                                                                            if ($order == 0) {
                                                                                                                                                                                                                ?><i class="fa fa-sort-amount-asc" aria-hidden="true"></i> <?php } else { ?><i class="fa fa-sort-amount-desc" aria-hidden="true"></i><?php
                                                                }
                                                            } else {
                                                                ?><i class="fa fa-sort" aria-hidden="true"></i> <?php } ?></a></th>
                                                            <? if (count($service_cat_data) > 1) { ?>
                                                        <th width="10%">Service Category</th>
                                                    <? } ?>
                                                    <th width="8%" style="text-align:center;">Item Categories</th>
                                                    <th width="15%">Mobile</th>
                                                    <th width="8%" class='align-center'>View/Edit Documents</th>                                                    <th width="6%" class='align-center' style="text-align:center;"><a href="javascript:void(0);" onClick="Redirect(4,<?php
                                                        if ($sortby == '4') {
                                                            echo $order;
                                                        } else {
                                                            ?>0<?php } ?>)">Status <?php
                                                                                                                                                                                                                                              if ($sortby == 4) {
                                                                                                                                                                                                                                                  if ($order == 0) {
                                                                                                                                                                                                                                                      ?><i class="fa fa-sort-amount-asc" aria-hidden="true"></i> <?php } else { ?><i class="fa fa-sort-amount-desc" aria-hidden="true"></i><?php
                                                                                                                                                                                                                                          }
                                                                                                                                                                                                                                      } else {
                                                                                                                                                                                                                                          ?><i class="fa fa-sort" aria-hidden="true"></i> <?php } ?></a></th>
                                                    <th width="6%" align="center" style="text-align:center;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($data_drv)) {
                                                    for ($i = 0; $i < count($data_drv); $i++) {
                                                        $default = '';
                                                        if ($data_drv[$i]['iCompanyId'] == 1) {
                                                            $default = 'disabled';
                                                        }
                                                        ?>
                                                        <tr class="gradeA">
                                                            <td align="center" style="text-align:center;"><input type="checkbox" id="checkbox" name="checkbox[]" <?php echo $default; ?> value="<?php echo $data_drv[$i]['iCompanyId']; ?>" data-count="<?= $data_drv[$i]['foodcatCount'] ?>"/>&nbsp;</td>
                                                            <td>
                                                                <a href="javascript:void(0);" onClick="show_company_details('<?= $data_drv[$i]['iCompanyId']; ?>')" style="text-decoration: underline;"><?= $generalobjAdmin->clearCmpName($data_drv[$i]['vCompany']); ?>
                                                                </a>

                                                            </td>
                                                            <td><?= $generalobjAdmin->clearEmail($data_drv[$i]['vEmail']); ?></td>
                                                            <? if (count($service_cat_data) > 1) { ?>
                                                                <td>
                                                                    <?php foreach ($service_cat_data as $servicedata) { ?>
                                                                        <?php if ($servicedata['iServiceId'] == $data_drv[$i]['iServiceId']) { ?><span><?php echo (isset($servicedata['vServiceName']) ? $servicedata['vServiceName'] : ''); ?></span><?php } ?>
                                                                    <?php } ?>
                                                                </td>
                                                            <? } ?>
                                                            <td style="text-align:center;">
                                                                <?php if ($data_drv[$i]['foodcatCount'] > 0 && $userObj->hasPermission('view-item-categories')) { ?>
                                                                    <a href="food_menu.php?iFoodMenuId=&option=c.vCompany&keyword=<?php echo $generalobjAdmin->clearCmpName($data_drv[$i]['vCompany']) ?>&eStatus=" target="_blank"><?= $data_drv[$i]['foodcatCount']; ?></a>
                                                                    <?php
                                                                } else {
                                                                    echo $data_drv[$i]['foodcatCount'];
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if (!empty($data_drv[$i]['vPhone'])) { ?>
                                                                    (+<?= $data_drv[$i]['vCode'] ?>) <?= $generalobjAdmin->clearPhone($data_drv[$i]['vPhone']); ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td align="center" >
                                                                <a href="store_document_action.php?id=<?= $data_drv[$i]['iCompanyId']; ?>&action=edit" target="_blank">
                                                                    <img src="img/edit-doc.png" alt="Edit Document" >
                                                                </a>
                                                            </td>
                                                            <td align="center" style="text-align:center;">
                                                                <?php
                                                                if ($data_drv[$i]['eStatus'] == 'Active') {
                                                                    $dis_img = "img/active-icon.png";
                                                                } else if ($data_drv[$i]['eStatus'] == 'Inactive') {
                                                                    $dis_img = "img/inactive-icon.png";
                                                                } else if ($data_drv[$i]['eStatus'] == 'Deleted') {
                                                                    $dis_img = "img/delete-icon.png";
                                                                }
                                                                ?>
                                                                <img src="<?= $dis_img; ?>" alt="image" data-toggle="tooltip" title="<?php echo $data_drv[$i]['eStatus']; ?>">
                                                            </td>
                                                            <td align="center" style="text-align:center;" class="action-btn001">


                                                                <? if ((in_array($data_drv[$i]['iCompanyId'], $storeIdArray)) && SITE_TYPE == 'Demo') { ?>

                                                                    <a href="store_action.php?id=<?= $data_drv[$i]['iCompanyId']; ?>" data-toggle="tooltip" title="Edit">
                                                                        <img src="img/edit-icon.png" alt="Edit">
                                                                    </a>

                                                                <?php } else { ?>

                                                                    <div class="share-button share-button4 openHoverAction-class" style="display:block;">
                                                                        <label class="entypo-export"><span><img src="images/settings-icon.png" alt=""></span></label>
                                                                        <div class="social show-moreOptions openPops_<?= $data_drv[$i]['iCompanyId']; ?>">
                                                                            <ul>
                                                                                <li class="entypo-twitter" data-network="twitter"><a href="store_action.php?id=<?= $data_drv[$i]['iCompanyId']; ?>" data-toggle="tooltip" title="Edit">
                                                                                        <img src="img/edit-icon.png" alt="Edit">
                                                                                    </a></li>
                                                                                <?php if ($userObj->hasPermission('update-status-store')) { ?>
                                                                                    <li class="entypo-facebook" data-network="facebook"><a href="javascript:void(0);" onClick="checkitemcount(<?= $data_drv[$i]['iCompanyId']; ?>, '<?= $data_drv[$i]['foodcatCount']; ?>', 'Inactive')"  data-toggle="tooltip" title="Activate">
                                                                                            <img src="img/active-icon.png" alt="<?php echo $data_drv[$i]['eStatus']; ?>" >
                                                                                        </a></li>
                                                                                    <li class="entypo-gplus" data-network="gplus"><a href="javascript:void(0);" onClick="changeStatus('<?php echo $data_drv[$i]['iCompanyId']; ?>', 'Active')" data-toggle="tooltip" title="Deactivate">
                                                                                            <img src="img/inactive-icon.png" alt="<?php echo $data_drv[$i]['eStatus']; ?>" >
                                                                                        </a></li>
                                                                                <?php } ?>

                                                                                <?php if ($eStatus != 'Deleted' && $userObj->hasPermission('delete-store')) { ?>

                                                                                    <?php if (!in_array($data_drv[$i]['iCompanyId'], $DEMO_NOT_DEL_COMPANY_ID)) { ?> 

                                                                                        <li class="entypo-gplus" data-network="gplus"><a href="javascript:void(0);" onClick="changeStatusDeletecd('<?php echo $data_drv[$i]['iCompanyId']; ?>')"  data-toggle="tooltip" title="Delete"> <img src="img/delete-icon.png" alt="Delete" > </a></li>

                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>  <!--<li class="entypo-gplus" data-network="gplus"><a href="javascript:void(0);" data-name="<?= $generalobjAdmin->clearCmpName($data_drv[$i]['vCompany']); ?>" data-id="<?php echo $data_drv[$i]['iCompanyId']; ?>" onClick="copyStoreData(this)"  data-toggle="tooltip" title="Copy"> <img src="img/right-green.png" alt="Copy" > </a></li>-->

                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr class="gradeA">
                                                        <td colspan="8"> No Records Found.</td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </form>
                                    <?php include('pagination_n.php'); ?>
                                </div>
                            </div> <!--TABLE-END-->
                        </div>
                    </div>
                    <div class="admin-notes">
                        <h4>Notes:</h4>
                        <ul>
                            <li><?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> module will list all <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> on this page.</li>
                            <li>Admin can Activate / Deactivate / Delete any <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?>. Default <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> cannot be Activated / Deactivated / Deleted.</li>
                            <li>Admin can export data in XLS or PDF format.</li>
                            <li>This module will list the <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> registered as a Food delivery, Grocery Delivery, Wine Delivery. <br/> ( * As per the package selection Paid services data will be shown here.)</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
        </div>
        <!--END MAIN WRAPPER -->
        <form name="pageForm" id="pageForm" action="action/store.php" method="post" >
            <input type="hidden" name="page" id="page" value="<?php echo $page; ?>">
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $tpages; ?>">
            <input type="hidden" name="iCompanyId" id="iMainId01" value="" >
            <input type="hidden" name="eStatus" id="eStatus" value="<?php echo $eStatus; ?>" >
            <input type="hidden" name="status" id="status01" value="" >
            <input type="hidden" name="statusVal" id="statusVal" value="" >
            <input type="hidden" name="option" value="<?php echo $option; ?>" >
            <input type="hidden" name="keyword" value="<?php echo $keyword; ?>" >
            <input type="hidden" name="sortby" id="sortby" value="<?php echo $sortby; ?>" >
            <input type="hidden" name="order" id="order" value="<?php echo $order; ?>" >
            <input type="hidden" name="selectcategory" id="selectcategory" value="<?php echo $select_cat; ?>" >
            <input type="hidden" name="method" id="method" value="" >
        </form>

        <div  class="modal fade" id="detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h4><i aria-hidden="true" class="fa fa-building-o" style="margin:2px 5px 0 2px;"></i>
                            <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> Details<button type="button" class="close" data-dismiss="modal">x</button>
                        </h4>
                    </div>
                    <div class="modal-body" style="max-height: 450px;overflow: auto;">
                        <div id="imageIcons" style="display:none">
                            <div align="center">                                                                       
                                <img src="default.gif"><br/>                                                            
                                <span>Retrieving details,please Wait...</span>                       
                            </div>    
                        </div>
                        <div id="comp_detail"></div>
                    </div>
                </div>
            </div>		
        </div>

        <div  class="modal fade" id="copy_store" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 id="storenametxt"></h4>
                    </div>
                    <div class="modal-body">
                        <form name="_company_form" id="_company_form" method="post" action="" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Select Store :</label>
                                </div>
                                <input type="hidden" id="storeid" name="storeId">
                                <div class="col-lg-12">
                                    <select class="form-control" multiple="multiple" name='store_sel[]' id="store_sel">
                                        <?php for ($s = 0; $s < count($getAllStore); $s++) { ?>
                                            <option value="<?= $getAllStore[$s]['iCompanyId']; ?>"><?= $getAllStore[$s]['vCompany']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><br><br><br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-default" name="copystore" id="copystore" value="Copy" >
                                    <a href="store.php" class="btn btn-default back_link">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>		
        </div>
        <?php include_once('footer.php'); ?>
        <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
        <link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
        <script>
                                                                                    function checkitemcount(id, countitem, status) {
                                                                                        if (countitem == 0) {
                                                                                            var retVal = confirm("This <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> has not added any items yet. Confirm to activate this <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?>?");
                                                                                            if (retVal == true) {
                                                                                                $("#pageForm").attr("action", "action/store.php");
                                                                                                changeStatus(id, status);
                                                                                                return true;
                                                                                            } else {
                                                                                                $("#pageForm").attr("action", "javascript:void(0);");
                                                                                                return false;
                                                                                            }


                                                                                        } else {
                                                                                            changeStatus(id, status);
                                                                                        }

                                                                                    }
                                                                                    $(document).ready(function () {
                                                                                        $('#store_sel').multiselect({
                                                                                            enableCaseInsensitiveFiltering: true,
                                                                                            includeSelectAllOption: true,
                                                                                            maxHeight: 400
                                                                                        });
                                                                                    });

                                                                                    /*    $(document).ready(function() {  
                                                                                     $('#eStatus_options').hide(); 
                                                                                     $('#option').each(function(){
                                                                                     if (this.value == 'c.eStatus') {
                                                                                     $('#eStatus_options').show(); 
                                                                                     $('.searchform').hide(); 
                                                                                     }
                                                                                     });
                                                                                     });
                                                                                     $(function() {
                                                                                     $('#option').change(function(){
                                                                                     if($('#option').val() == 'c.eStatus') {
                                                                                     $('#eStatus_options').show();
                                                                                     $("input[name=keyword]").val("");
                                                                                     $('.searchform').hide(); 
                                                                                     } else {
                                                                                     $('#eStatus_options').hide();
                                                                                     $("#estatus_value").val("");
                                                                                     $('.searchform').show();
                                                                                     } 
                                                                                     });
                                                                                     });*/
                                                                                    $("#setAllCheck").on('click', function () {
                                                                                        if ($(this).prop("checked")) {
                                                                                            jQuery("#_list_form input[type=checkbox]").each(function () {
                                                                                                if ($(this).attr('disabled') != 'disabled') {
                                                                                                    this.checked = 'true';
                                                                                                }
                                                                                            });
                                                                                        } else {
                                                                                            jQuery("#_list_form input[type=checkbox]").each(function () {
                                                                                                this.checked = '';
                                                                                            });
                                                                                        }
                                                                                    });

                                                                                    $("#Search").on('click', function () {
                                                                                        var action = $("#_list_form").attr('action');
                                                                                        var formValus = $("#frmsearch").serialize();
                                                                                        window.location.href = action + "?" + formValus;
                                                                                    });

                                                                                    $('.entypo-export').click(function (e) {
                                                                                        e.stopPropagation();
                                                                                        var $this = $(this).parent().find('div');
                                                                                        $(".openHoverAction-class div").not($this).removeClass('active');
                                                                                        $this.toggleClass('active');
                                                                                    });

                                                                                    $(document).on("click", function (e) {
                                                                                        if ($(e.target).is(".openHoverAction-class,.show-moreOptions,.entypo-export") === false) {
                                                                                            $(".show-moreOptions").removeClass("active");
                                                                                        }
                                                                                    });

                                                                                    function show_company_details(companyid) {
                                                                                        $("#comp_detail").html('');
                                                                                        $("#imageIcons").show();
                                                                                        $("#detail_modal").modal('show');

                                                                                        if (companyid != "") {
                                                                                            var request = $.ajax({
                                                                                                type: "POST",
                                                                                                url: "ajax_store_details.php",
                                                                                                data: "iCompanyId=" + companyid,
                                                                                                datatype: "html",
                                                                                                success: function (data) {
                                                                                                    $("#comp_detail").html(data);
                                                                                                    $("#imageIcons").hide();
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }

                                                                                    function status_check(status) {
                                                                                        if (status == "Active") {
                                                                                            var zero_values = "No";
                                                                                            $("input[type=checkbox]:checked").each(function () {
                                                                                                var cnt = $(this).attr('data-count');
                                                                                                if (cnt == 0) {
                                                                                                    zero_values = "Yes";
                                                                                                    return false;
                                                                                                }
                                                                                            });

                                                                                            if (zero_values == "No") {
                                                                                                ChangeStatusAll(status);
                                                                                                $('#new-msg-activeid').html("Are you sure to activate selected record(s)?");
                                                                                            } else {

                                                                                                ChangeStatusAll(status);

                                                                                                $('#new-msg-activeid').html('This <?php echo $langage_lbl_admin['LBL_RESTAURANT_TXT_ADMIN']; ?> has not added any items yet? Are you sure to activate selected record(s)?');
                                                                                            }
                                                                                        } else {
                                                                                            ChangeStatusAll(status);
                                                                                            $('#new-msg-activeid').html("Are you sure to activate selected record(s)?");
                                                                                        }
                                                                                    }
                                                                                    function copyStoreData(elem) {
                                                                                        var storeId = $(elem).attr("data-id");
                                                                                        var storeName = $(elem).attr("data-name");
                                                                                        $("#storenametxt").html('<i aria-hidden="true" class="fa fa-building-o" style="margin:2px 5px 0 2px;"></i>Store Data Copy : ' + storeName);
                                                                                        $("#storeid").val(storeId);
                                                                                        $('#store_sel option[value="' + storeName + '"]').remove();
                                                                                        $("#copy_store").modal('show');
                                                                                    }

        </script>
    </body>
    <!-- END BODY-->
</html>
