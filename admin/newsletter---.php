<?php
include_once('../common.php');
if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}
////$generalobjAdmin->check_member_login();
if (!$userObj->hasPermission('manage-newsletter')) {
    $userObj->redirect();
}
$script = 'Newsletter';
$type = (isset($_REQUEST['reviewtype']) && $_REQUEST['reviewtype'] != '') ? $_REQUEST['reviewtype'] : 'Driver';
$newslettertype = $type;
//Start Sorting
$sortby = isset($_REQUEST['sortby']) ? $_REQUEST['sortby'] : 0;
$order = isset($_REQUEST['order']) ? $_REQUEST['order'] : '';
$ord = ' ORDER BY iNewsLetterId DESC';
if ($sortby == 1) {
    if ($order == 0)
        $ord = " ORDER BY vName ASC";
    else
        $ord = " ORDER BY vName DESC";
}
if ($sortby == 2) {
    if ($order == 0)
        $ord = " ORDER BY vEmail ASC";
    else
        $ord = " ORDER BY vEmail DESC";
}
if ($sortby == 3) {
    if ($order == 0)
        $ord = " ORDER BY tDate ASC";
    else
        $ord = " ORDER BY tDate DESC";
}
// Start Search Parameters
$option = isset($_REQUEST['option']) ? stripslashes($_REQUEST['option']) : "";
$keyword = isset($_REQUEST['keyword']) ? stripslashes($_REQUEST['keyword']) : "";
$searchDate = isset($_REQUEST['searchDate']) ? $_REQUEST['searchDate'] : "";
// End Search Parameters
//Pagination Start
$per_page = $DISPLAY_RECORD_NUMBER; // number of results to show per page
$sql = "SELECT count(r.iNewsLetterId) as Total FROM newsletter WHERE 1=1";
$totalData = $obj->MySQLSelect($sql);
$total_pages = 0;
if (count($totalData) > 0) {
    $total_results = $totalData[0]['Total'];
}
$total_pages = ceil($total_results / $per_page); //total pages we going to have
$show_page = 1;
//-------------if page is setcheck------------------//
$start = 0;
$end = $per_page;
if (isset($_GET['page'])) {
    $show_page = $_GET['page'];             //it will telles the current page
    if ($show_page > 0 && $show_page <= $total_pages) {
        $start = ($show_page - 1) * $per_page;
        $end = $start + $per_page;
    }
}
// display pagination
$page = isset($_GET['page']) ? intval($_GET['page']) : 0;
$tpages = $total_pages;
if ($page <= 0)
    $page = 1;
//Pagination End
$chkusertype = "";
if ($type == "Driver") {
    $chkusertype = "Passenger";
} else {
    $chkusertype = "Driver";
}
$sql = "SELECT * FROM newsletter WHERE 1=1 $ord LIMIT $start, $per_page";
$data_drv = $obj->MySQLSelect($sql);
$endRecord = count($data_drv);
$var_filter = "";
foreach ($_REQUEST as $key => $val) {
    if ($key != "tpages" && $key != 'page')
        $var_filter .= "&$key=" . stripslashes($val);
}
$reload = $_SERVER['PHP_SELF'] . "?tpages=" . $tpages . $var_filter;
?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD-->
    <head>
        <meta charset="UTF-8" />
        <title><?= $SITE_NAME ?> | Admin</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <?php include_once('global_files.php'); ?>
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >
        <!-- Main LOading -->
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <?php include_once('header.php'); ?>
            <?php include_once('left_menu.php'); ?>

            <!--PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <div id="add-hide-show-div">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Newsletter</h2>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <?php include('valid_msg.php'); ?>
                    <div class="panel-heading">
                        <form name="frmsearch" id="frmsearch" action="javascript:void(0);" method="post">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="admin-nir-table">
                                <tbody>
                                    <tr>
                                        <td width="12%">
                                            <?php if (!empty($data_drv)) { ?>
                                                <button type="button" onClick="showExportTypes('newsletter')" class="panel-heading-av" >Export</button>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if (count($data_drv) > 0) { ?>
                                <div class="panel-heading ">
                                    <form name="_export_form" id="_export_form" method="post" >
                                    </form>
                                </div>
                            <?php } ?>
                        </form>
                        <div class="table-list">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div style="clear:both;"></div>
                                    <div class="table-responsive">
                                        <form class="_list_form" id="_list_form" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th width="15%"><?php echo $langage_lbl_admin['LBL_USER_NAME_HEADER_SLIDE_TXT']; ?></th>
                                                        <th width="15%"><?php echo $langage_lbl_admin['LBL_EMAIL_LBL_TXT']; ?></th>
                                                        <th width="15%"><?php echo $langage_lbl_admin['LBL_DATE_SIGNUP']; ?></th>
                                                        <th width="15%">IP</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (!empty($data_drv)) {
                                                        for ($i = 0; $i < count($data_drv); $i++) {
                                                            ?>
                                                            <tr class="gradeA">
                                                                <td width="10%"><?php echo $data_drv[$i]['vName']; ?></td>
                                                                <td> <?= $data_drv[$i]['vEmail'] ?> </td>
                                                                <td><?= $generalobjAdmin->DateTime($data_drv[$i]['tDate']); ?></td>
                                                                <td > <?= $data_drv[$i]['vIP'] ?></td>  
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr class="gradeA">
                                                            <td colspan="7"> No Records Found.</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                        <?php include('pagination_n.php'); ?>
                                    </div>
                                </div> <!--TABLE-END-->
                            </div>
                        </div>
                        <div class="admin-notes">
                            <h4>Notes:</h4>
                            <ul>
                                <li>
                                    Newsletter module will list all newsletter on this page.
                                </li>
                                <li>
                                    Administrator can not Delete any Newsletter.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--END PAGE CONTENT -->
            </div>
            <!--END MAIN WRAPPER -->
            <?php
            include_once('footer.php');
            ?>
    </body>
    <!-- END BODY-->
</html>