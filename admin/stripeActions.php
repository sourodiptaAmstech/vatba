<?php
include_once('../common.php');
if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}
////$generalobjAdmin->check_member_login();
require_once ('../assets/libraries/stripe/stripe_managment.php');

$retrun=array("error"=>true, "msg"=>"error while creating stripe account.");
$stripe_key="";
if(isset($_POST)){
    if(isset($_POST['driver_id'])){
        if((int)$_POST['driver_id']>0){
            // get  the key.....
            $sql_p = "select * from configurations_payment where vName='STRIPE_SECRET_KEY_SANDBOX'";
            $payment_value= $obj->MySQLSelect($sql_p);
            if(!empty($payment_value)){
                if(count($payment_value)>0){
                    $stripe_key=$payment_value[0]['vValue'];
                }
            }
            $StripeManagement= new StripeManagement();
            $sql = "SELECT rd.*, c.vCity as cityName,s.vState as stateName FROM register_driver rd 
                    left join city as c on c.iCityId=rd.vCity
                    left join state s on s.iStateId=c.iStateId
                    WHERE rd.iDriverId=".(int)$_POST['driver_id'];
                  //  echo 0; exit;
            $result=$obj->MySQLSelect($sql);
            if((int)$_REQUEST['check']===0){
                // check if any document is uploaded or not 
                $sql_doc="select * from document_list where doc_userid='".(int)$_POST['driver_id']."' and doc_masterid in (1,11,12)";
                $result_Doc=$obj->MySQLSelect($sql_doc);
                if(!empty($result_Doc)){
                    if(count($result_Doc)>0){
                        if(!empty($result) && $stripe_key!==""){
                            if(count($result)>0)
                            {
                                $documentArray_stripe=$StripeManagement->uploadFiles($result_Doc,$stripe_key);
                                
                                $stripe_data= $StripeManagement->createStripeConnectAccount($result,$stripe_key,$documentArray_stripe);
                               // print_r($stripe_data); exit;
                                // to do : link the user with stripe account.
                                if(array_key_exists("error", $stripe_data)){
                                    $retrun= array('msg' =>$stripe_data['error']['message'] ,'error'=>true, 'error_code'=>"RETRUN_BY_STRIPE");
                                }
                                else{
                                    $stripeBankID="";
                                    $stripeAccountID="";
                                    $stripeAccountStatus="pending";
                                    if(array_key_exists("external_accounts", $stripe_data)){
                                        if(array_key_exists("data", $stripe_data['external_accounts'])){
                                            foreach ( $stripe_data['external_accounts']['data'] as $key => $value) {
                                                $stripeBankID=$value['id'];
                                                $stripeAccountID=$value['account'];
                                            }
                                        }
                                    }
                                    //echo 10; exit;
                                    // check the status of the strip account 
                                    if(array_key_exists("individual", $stripe_data)){
                                        //if(array_key_exists("verification", $stripe_data['individual']){
                                        $stripeAccountStatus=$stripe_data['individual']['verification']['status'];
                                        //}
                                    }
                                    // check the stripe account id and bank id value is set or not before updating the driver registration table
                                    if($stripeBankID!=="" && $stripeAccountID !==""){
                                        $sqlUpdate="update register_driver set vStripeCusId='".$stripeAccountID."', stripeBankID='".$stripeBankID."',stripeConnectAccountStatus='".$stripeAccountStatus."'  where iDriverId =".(int)$_POST['driver_id'];
                                        $obj->sql_query($sqlUpdate);
                                        // update the document tabel with stripe document id
                                        foreach ($documentArray_stripe as $key) {
                                            if(array_key_exists("stripe_img_id", $key)){
                                                $sqlUpdate="update document_list set stripe_img_id='".$key['stripe_img_id']."' where doc_id =".(int)$key['doc_id'];
                                                $obj->sql_query($sqlUpdate);
                                            }
                                        }
                                        $retrun= array('msg' =>'Stripe account is created successfully.' ,'error'=>false,  'error_code'=>"RETRUN_FROM_CODE");
                                    }
                                    else{
                                        $retrun= array('msg' =>'Error in creating stripe account.' ,'error'=>true,  'error_code'=>"RETRUN_FROM_CODE");
                                    }
                                }
                            }
                            else{
                                $retrun= array('msg' =>'Error in creating stripe account.' ,'error'=>true,  'error_code'=>"RETRUN_FROM_CODE");
                            }
                        }
                    }
                    else{
                        $retrun= array('msg' =>'Documents is missing' ,'error'=>true,'error_code'=>"RETRUN_FROM_CODE");
                    }
                }
                else{
                    $retrun= array('msg' =>'Documents is missing' ,'error'=>true,'error_code'=>"RETRUN_FROM_CODE");
                }
            }
            else if((int)$_REQUEST['check']===1){
                $retrun=array('msg' =>"" ,'error'=>true,'error_code'=>"RETRUN_FROM_CODE");
                if($result[0]['vStripeCusId']!==""){
                     $stripe_data= $StripeManagement->checkstatus($result[0]['vStripeCusId'],$stripe_key);
                     if(array_key_exists("error", $stripe_data)){
                        $retrun= array('msg' =>"Invalid Account" ,'error'=>true, 'error_code'=>"RETRUN_BY_STRIPE");
                     }
                     else{
                        if(array_key_exists("individual", $stripe_data)){
                             if(array_key_exists("verification", $stripe_data['individual'])){
                                if(array_key_exists("status", $stripe_data['individual']['verification'])){
                                    $stripeAccountStatus= $stripe_data['individual']['verification']['status']; //exit;
                                     // update the driver status 
                                     $sqlUpdate="update register_driver set stripeConnectAccountStatus='".$stripeAccountStatus."'  where iDriverId =".(int)$_POST['driver_id'];
                                        $obj->sql_query($sqlUpdate);
                                        $retrun=array('msg' =>$stripeAccountStatus ,'error'=>false,'error_code'=>"RETRUN_FROM_CODE");
                                    }
                                }
                            }
                        }
                    }
                    //print_r($stripe_data); exit;
                }
            }
        }
    }
echo json_encode($retrun);

