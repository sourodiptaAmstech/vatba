<?php
/* Test Card Details
 * Card No 1: 4970 1000 0000 0055
 * Card No 2: 4970 1000 0000 0022
 * Card No 3: 4970 1000 0000 0014
 * MM/YY : 11/21
 * CVV : 123
 */
ini_set("display_errors", 1);
error_reporting(E_ALL);
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/keys.php';
require_once __DIR__ . '/helpers.php';
$client = new Lyra\Client();
$store = array("amount" => 250,"currency" => "EUR","formAction" => "REGISTER_PAY","customer" => array("email" => "sample@example.com","orderId" => uniqid("MyOrderId")));
$response = $client->post("V4/Charge/CreatePayment", $store);
if ($response['status'] != 'SUCCESS') {
    display_error($response);
    $error = $response['answer'];
    throw new Exception("error " . $error['errorCode'] . ": " . $error['errorMessage']);
}
/* everything is fine, I extract the formToken */
$formToken = $response["answer"]["formToken"];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Payzen Payment</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <script src="<?php echo $client->getClientEndpoint(); ?>/static/js/krypton-client/V4.0/stable/kr-payment-form.min.js" kr-public-key="<?php echo $client->getPublicKey(); ?>" kr-post-url-success="charge.php">
        </script>
        <link rel="stylesheet" media="screen" type="text/css" href="css/screen.css" />
        <link rel="stylesheet" href="<?php echo $client->getClientEndpoint(); ?>/static/js/krypton-client/V4.0/ext/classic-reset.css">
        <script src="<?php echo $client->getClientEndpoint(); ?>/static/js/krypton-client/V4.0/ext/classic.js"></script>
    </head>
    <body style="padding-top:20px">
        <div class="main-part"> 
            <div class="page-contant">  
                <div class="page-contant-inner"> 
                    <div class="kr-embedded" kr-form-token="<?php echo $formToken; ?>">
                        <label class="back-img" style="padding-top: 20px;"><img style="width: 300px;" src="img/card.png"></label>
                        <!-- payment form fields -->
                        <div class="kr-pan"></div>
                        <div class="kr-expiry"></div>
                        <div class="kr-security-code"></div>
                        <button class="kr-payment-button"></button>
                        <!-- error zone -->
                        <div class="kr-form-error"></div>
                    </div>  
                </div>

            </div>
        </div>
    </body>
</html>
<script>
            setTimeout(function () {
                $(".kr-section-row").hide();
            }, 1000);
            setTimeout(function () {
                $(".kr-section-row").hide();
            }, 1500);
            setTimeout(function () {
                $(".kr-section-row").hide();
            }, 2000);
</script>