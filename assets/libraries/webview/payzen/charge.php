<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);
$data = (array) json_decode($_REQUEST['kr-answer']);
$paymentMethodToken = "da9b15ad9691448d97024ab2093ddbb5";
if (isset($data['transactions'][0]->paymentMethodToken)) {
    $paymentMethodToken = $data['transactions'][0]->paymentMethodToken;
}
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/keys.php';
require_once __DIR__ . '/helpers.php';
$client = new Lyra\Client();
$store = array(
    "amount" => 550,
    "currency" => "EUR",
    "paymentMethodToken" => $paymentMethodToken
);
$response = $client->post("V4/Charge/CreatePayment", $store);
/* I check if there is some errors */
if ($response['status'] != 'SUCCESS') {
    /* an error occurs, I throw an exception */
    display_error($response);
    $error = $response['answer'];
    throw new Exception("error " . $error['errorCode'] . ": " . $error['errorMessage']);
} else {
    $transId =$cardNo= "";
    if (isset($response['answer']['transactions'][0]['shopId'])) {
        $transId = $response['answer']['transactions'][0]['shopId'];
        
    }
    if(isset($response['answer']['transactions'][0]['transactionDetails']['cardDetails']['pan'])){
        $cardNo = $response['answer']['transactions'][0]['transactionDetails']['cardDetails']['pan'];
    }
    echo "Card Successfully Charged<br>";
    echo "Transaction Id : ".$transId."<br>";
    echo "Card Number : ".$cardNo."<br>";die;
}
exit;
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/styles/dracula.min.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="skinned/assets/paid.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/highlight.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('pre code').each(function (i, block) {
                    hljs.highlightBlock(block);
                });
            });
        </script>
    </head>
    <body>
        <div class="container">  
            <h2>web-service request:</h2>
            <pre><code class="json"><?php print json_encode($store, JSON_PRETTY_PRINT) ?></code></pre>
    <h2>web-service answer:</h2>
    <pre><code class="json"><?php print json_encode($response, JSON_PRETTY_PRINT) ?></code></pre>
</div>
        <?php
        /* I check if it's really paid */
        if ($response['answer']['orderStatus'] != 'PAID') {
            $title = "Transaction not paid !";
        } else {
            $title = "Transaction paid !";
        }
        ?>
<h1><?php echo $title; ?></h1>
<h1><a href="/">Back to demo menu</a></h1>

</body></html>
