 <html>
<head>
	<title>Merchant Payment Here</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
	<style>
		label{
			float: left!important;
		}
	</style>
</head>
<body>
<?php

//ob_start();
//session_start();
date_default_timezone_set('Asia/Singapore');
$rspncmsg = "Your response will be here";

function sendRequest($gateway_url, $data_string){
	$content = json_encode($data_string);
	//echo $content;exit;
	$curl = curl_init($gateway_url);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
	array("Content-type: application/json"));
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
	$json_response = curl_exec($curl);
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ( $status != 200 ) {
		return 'error';
	}
	curl_close($curl);
	return $json_response;
}
 function doPayment(){

	$date = new DateTime();
	$security_code = "Qhm7dyHhjb0&";	
	$security_code_md5 = md5("&Yfbw&pjXFE&");	
	$merchant_id = "MCPSP84555";
	$ref = $date->getTimestamp();
	$cur = "MYR";
	$amt = "10";
	$fgkey = md5($security_code. "?mid=" . $merchant_id ."&ref=" . $ref ."&cur=" .$cur ."&amt=" .$amt);
	 
	$request = array(
				'mid' => $merchant_id,
				'txntype' => 'QUERY',
				'reference' => $ref,
				'cur' => $cur,
				'amt' => $amt,
				/*'cardno' => '5111111111111118',
				'expmonth' => '05',
				'expyear' => '21',
				
				'cardholder' => 'MCPayment',*/
				'cardtoken' => '01eaecea-4d29-4347-b510-29a5e6a37ee0',
				'cvv' => '100',
				'shop' => 'MCP Shop',
				'buyer' => 'MCP Buyer',
				'tel' => '',
				'email' => 'mcpayment@mcpayment.net',
				'product' => 'MCP Product',
				'lang' => 'en',
				'returnurl' => 'http://192.168.1.131/cubejekdev/assets/libraries/webview/mcp/return_gatewayhosted.php',
				'statusurl' => 'http://192.168.1.131/cubejekdev/assets/libraries/webview/mcp/return_gatewayhosted.php',
				'fgkey' => $fgkey
				);
	$gateway_url = "https://map.uat.mcpayment.net/api/PaymentAPI/Purchase";
	$result = sendRequest($gateway_url, $request);
	$posback = json_decode($result);
	
	$rescode = $posback->rescode;
	$resmsg = $posback->resmsg;
	$transid = $posback->transid;
	$response_fgkey = $posback->fgkey;

	$check_fgkey = md5($security_code_md5. "?mid=" . $merchant_id ."&ref=" . $ref ."&cur=" .$cur ."&amt=" .$amt . "&rescode=" . $rescode . "&transid=" . $transid);
	//$check_fgkey = md5($security_code_md5. "?mid=" . $merchant_id ."&ref=" . $ref ."&cur=" .$cur ."&amt=" .$amt);
	
	if( $rescode == '0000' && $check_fgkey == $response_fgkey) {
		$rspncmsg = "Transaction success";
	}else{
		$rspncmsg = '<table class="table table-hover">';
		$rspncmsg .= '<tr class="table-success"><th>Response Code : </th><td>'.$rescode.'</td></tr>';
		$rspncmsg .= '<tr class="table-danger"><th>Response Message : </th><td>'.$resmsg.'</td></tr>';
		$rspncmsg .= '<tr class="table-warning"><th>Original FgKey : </th><td>'.$check_fgkey.'</td></tr>';
		$rspncmsg .= '<tr class="table-warning"><th>Response Fgkey : </th><td>'.$response_fgkey.'</td></tr>';
		$rspncmsg .= '</table>';
		return $rspncmsg;
	}
 }

if (isset($_POST['submit_mcpayment'])) {
    $result = doPayment();
}

?>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="">MC Payment Gateway Token : [01eaecea-4d29-4347-b510-29a5e6a37ee0]</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>
<div class="container" style="margin-top: 20px;">
	<div class="col-md-12" align="center">
		<div class="" style="width: 50%">
		
			<code> <?=isset($result) ? $result : 'Your Reponse is here'?> </code>

			<form action="" method="POST">
			    <input class="btn btn-danger" type="submit"  name="submit_mcpayment" value="Pay via MCPayment Token" /><br>
			    <span><a href="index.php">Generate New card token</a></span>
			</form>
		</div>
	</div>
</div>
 </div>
</body>
</html>        