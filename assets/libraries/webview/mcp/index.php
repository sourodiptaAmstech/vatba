<html>
<head>
	<title>Payment Here</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
	<style>
		label{
			float: left!important;
		}
	</style>
</head>
<body>

 <?php
 	//ob_start();
 	//session_start();

 	date_default_timezone_set('Asia/Singapore');
 	$date = date('d-M-y')."<br>";
 	$time = date('h:i:s')."<br>";
	$date = new DateTime();

	
	$security_code = "&Yfbw&pjXFE&";//zIBDrv9Qx@WK
	$merchant_id = "MCPFP74586";//MCPSP84562
	$cur = 'MYR';
	$amt = '3.00';
	$ref = $date->getTimestamp();
	$fgkey = md5($security_code. "?mid=" . $merchant_id ."&ref=" . $ref ."&cur=" .$cur ."&amt=" .$amt);
	$request = array(
				'mid' => $merchant_id,
				'txntype' => 'QUERY',
				'ref' => $ref,
				'shop' => 'MCP Shop',
				'buyer' => 'MCP Buyer',
				'product' => 'MCP Product',
				'lang' => 'en',
				'tokenize' => 'Y',
				'returnurl' => 'http://192.168.1.131/cubejekdev/assets/libraries/webview/mcp/return_gatewayhosted.php',
				'statusurl' => 'http://192.168.1.131/cubejekdev/assets/libraries/webview/mcp/return_gatewayhosted.php',
				'fgkey' => $fgkey
		);
	$gateway_url = "https://map.uat.mcpayment.net/payment/dopayment";


 ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">MC Payment Gateway</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="container">
	<div class="col-md-12" align="center">
		<div class="" style="width: 50%">
			<form action="<?=$gateway_url?>" method="POST">
				<?php
					$randNumber = rand(11111,99999);
					foreach($request as $key=>$value){
						echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
					}
				?>
				<div class="form-group">
					<label>Your Mobile :</label>
			     	<input type="number" class="form-control" name="tel" required="" value="98565<?=$randNumber?>">
			    </div>
			    <div class="form-group">
					<label>Your Email :</label>
			     	<input type="email" class="form-control" name="email" required="" value="user<?=$randNumber?>@gmail.com">
			    </div>
			    <div class="form-group">
					<label>Your Currency :</label>
			     	<input type="text" class="form-control" name="cur" value="<?=$cur?>" readonly="">
			    </div>
				<div class="form-group">
					<label>Amount :</label>
			     	<input type="number" class="form-control" name="amt" value="<?=$amt?>" required="">
			    </div>
			    <!--
			    	[cardtoken] => 01eaecea-4d29-4347-b510-29a5e6a37ee0 #1
			    	[cardtoken] => 01eaecea-4d29-4347-b510-29a5e6a37ee0 #1
			    -->
			    <input type="submit" class="btn btn-danger" name="submitPayment" value="Make Payment And Generate Token"  >
			    <br>
			    <span><a href="merchanthosted.php">Payment With Saved card Token</a></span>
			</form>
		</div>
	</div>
</div>
 </div>
</body>
</html>