<?php 
class StripeManagement{
    private  $STRIPE_SERVICE_URL;//accounts';
    private $data;
    private $key; 
    private $headers;
    private $urlToCall;
    private $document_front;
    private $document_back;
    private $additional_document_front;

    private function setKey($key){
        $this->key=$key;
        return true;
    }

    private function  getKey($ContentType){
        //sk_test_6DJLS4SC0j23CNLv04AheTTJ005VOFqYvM      

       $this->headers=  array('Authorization: Bearer '.$this->key);
       if($ContentType=="multipart"){
           $this->headers[]='Content-Type: multipart/form-data';
       }
        return true;
    }

    private function stripeCURL($isMultipart,$Method){
      //  echo $this->headers; exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $this->STRIPE_SERVICE_URL.$this->urlToCall);
        if($Method==="POST")
        curl_setopt($ch, CURLOPT_POST, true);
    else
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");




        if($isMultipart==="multipart")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
        else
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
       return json_decode($output, true);
       // $acc_no = $output['id'];
       // $stripe_bank = '';
        //$id = '';
    }
    private function setDataForConnectAccountCreate(array $inputArray){
        $this->data= array(
            'type' => 'custom',
            'country' => 'NZ',
            'default_currency'=>'NZD',
            'email' => $inputArray[0]['vEmail'],
            'requested_capabilities' => array('card_payments', 'transfers'),
            'business_type' => 'individual',
            'tos_acceptance' => array(
                'date' => time(),
                'ip' => $_SERVER['REMOTE_ADDR'] // Assumes you're not using a proxy
            ),
            "business_profile"=> array(
                "mcc"=> "4121",
                "product_description"=> "This will be a ridesharing business based on the mobile app. Passengers can book the ride on the app. Once the driver accepts the ride and  completes the ride, he gets paid his share of the earnings while my platform takes its share of commission.",               
                "support_url"=> null,
                "url"=> "https://www.vatba.co.nz"
            ),
            'external_account'=>array(
                "object"=>"bank_account",
                "account_number"=>$inputArray[0]['vAccountNumber'],
                "country"=>"NZ",
                "currency"=>"NZD",
                "account_holder_name"=> $inputArray[0]['vName'].' '.$inputArray[0]['vLastName'], 
                "account_holder_type"=>"individual"
            ),
            'individual'=>array(
                "email"=>$inputArray[0]['vEmail'],
                "first_name"=> $inputArray[0]['vName'],
                "last_name"=>$inputArray[0]['vLastName'],
                "phone"=>"+".$inputArray[0]['vCode'].$inputArray[0]['vPhone'],
                "address"=>array("city"=>$inputArray[0]['cityName'],
                                "country"=>"NZ","line1"=>$inputArray[0]['vCaddress'], 
                                "line2"=>"","postal_code"=>$inputArray[0]['vZip'],
                                "state"=>$inputArray[0]['stateName']),
                "dob"=>array("day"=>date("d",strtotime($inputArray[0]['dBirthDate']) ),
                            "month"=>date("m",strtotime($inputArray[0]['dBirthDate']) ),
                            "year"=>date("Y",strtotime($inputArray[0]['dBirthDate']) )),
                "verification"=>array("document"=>array("front"=>$this->document_front,"back"=>$this->document_back),"additional_document"=>array("front"=>$this->additional_document_front)),

            ),
            
        );
    
    }
    private function setFiles($data)
    {
        $fileInfoArray=array();
        foreach($data as $key=>$val){
            $cFile ="";
            $file='../webimages/upload/documents/driver/' . $val['doc_userid'] . '/' . $val['doc_file'];
            if (file_exists($file)) {
                if (function_exists('curl_file_create')) { // php 5.5+
                    if(function_exists('mime_content_type')) {
                        $containtType=mime_content_type($file);
                    }
                    else{
                        
                        $containtType=   $this->mime_content_type_fake($file);//echo $containtType; exit;
                    }
                    
                    $basename=basename($file);
                    $cFile = curl_file_create($file,$containtType,$basename);
                } else { //
                    $cFile = '@' . realpath('../webimages/upload/documents/driver/' . $val['doc_userid'] . '/' . $val['doc_file']);
                }
            }
            $this->data=array('purpose' => 'identity_document', 'file' => $cFile);
            $returnData= $this->stripeCURL("multipart","POST");
            if(!array_key_exists("error",$returnData)){
               $val['stripe_img_id']=$returnData['id'];
            }
            $fileInfoArray[]=$val;
        }
       // print_r($fileInfoArray); exit;
        return $fileInfoArray; 
    }


    private function setFilesForAccountCreate(array $doc_data){
        foreach($doc_data as $key){
            if((int)$key['doc_masterid']===1){
                $this->document_front=$key['stripe_img_id'];
            }
            if((int)$key['doc_masterid']===11){
                $this->document_back=$key['stripe_img_id'];
            }
            if((int)$key['doc_masterid']===12){
                $this->additional_document_front=$key['stripe_img_id'];
            }
        }
        return true;
    }





    
    public function createStripeConnectAccount(array $userData,$stripKey,array $doc_data){
       $this->setFilesForAccountCreate($doc_data);
     //  exit;
        $this->setDataForConnectAccountCreate($userData);
        $this->setKey($stripKey);
        $this->getKey("");
        $this->urlToCall='accounts';
        $this->STRIPE_SERVICE_URL = 'https://api.stripe.com/v1/';//accounts';
        return $this->stripeCURL("","POST");
    }



    public function uploadFiles(array $docData, $stripKey){
        $this->STRIPE_SERVICE_URL = 'https://files.stripe.com/v1/'; // for file upload.
        $this->setKey($stripKey);
        $this->getKey("multipart");
        $this->urlToCall='files';    
       return  $this->setFiles($docData);
       
    }

    public function checkstatus( $stripe_account_id,  $stripeKey){
        $this->STRIPE_SERVICE_URL='https://api.stripe.com/v1/';
        $this->urlToCall='accounts/'.$stripe_account_id;
        $this->setKey($stripeKey);
        $this->getKey("");
         return $this->stripeCURL("","GET");



    }
  private  function mime_content_type_fake($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }








}