/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.protectedSource.push(/<i[^>]*><\/i>/g);
	
	config.allowedContent = true;
	config.extraPlugins = 'filebrowser';

	

	
	
};
//$('.ckeditor').each(function(e){
//CKEDITOR.replace( this.id, {
//	//				extraPlugins: 'image2,uploadimage',
//
//					toolbar: [
//						{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
//						{ name: 'styles', items: [ 'Styles', 'Format' ] },
//						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
//						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
//						{ name: 'links', items: [ 'Link', 'Unlink' ] },
//						{ name: 'insert', items: [ 'Image', 'Table' ] },
//						{ name: 'tools', items: [ 'Maximize' ] },
//						{ name: 'editing', items: [ 'Scayt' ] }
//					],
//
//					// Configure your file manager integration. This example uses CKFinder 3 for PHP.
//					filebrowserBrowseUrl: '../assets/plugins/ckfinder/ckfinder.html',
//					filebrowserImageBrowseUrl: '../assets/plugins/ckfinder/ckfinder.html?type=Images',
//					filebrowserUploadUrl: '../assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//					filebrowserImageUploadUrl: '../assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//
//					// Upload dropped or pasted images to the CKFinder connector (note that the response type is set to JSON).
//					uploadUrl: '../assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
//
//					// Reduce the list of block elements listed in the Format drop-down to the most commonly used.
//					format_tags: 'p;h1;h2;h3;pre',
//					// Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
//					removeDialogTabs: 'image:advanced;link:advanced',
//
//					height: 450
//				});    
//
//});